/*******************************************************************************
  SGP41 I2C Driver

  Created by:
    Miroslav Soukup

  File Name:
    sgp41.h

  Summary:
    SGP41 I2C Driver Header File
 
  Version:
    1.0

  Description:
    This file provides basic functions for sgp41 sensor.

*******************************************************************************/

#ifndef SGP41_H // Protection against multiple inclusion
#define SGP41_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define SGP41_I2C_CLIENT_ADDRESS            UINT16_C(0x59)


#define SGP41_CMD_EXECUTE_CONDITIONING_MSB  UINT8_C(0x26)
#define SGP41_CMD_EXECUTE_CONDITIONING_LSB  UINT8_C(0x12)
    
#define SGP41_CMD_MEASURE_RAW_SIGNALS_MSB   UINT8_C(0x26)
#define SGP41_CMD_MEASURE_RAW_SIGNALS_LSB   UINT8_C(0x19)
    
#define SGP41_CMD_EXECUTE_SELF_TEST_MSB     UINT8_C(0x28)
#define SGP41_CMD_EXECUTE_SELF_TEST_LSB     UINT8_C(0x0E)
    
#define SGP41_CMD_TURN_HEATER_OFF_MSB       UINT8_C(0x36)
#define SGP41_CMD_TURN_HEATER_OFF_LSB       UINT8_C(0x15)

#define SGP41_CMD_GET_SERIAL_NUMBER_MSB     UINT8_C(0x36)
#define SGP41_CMD_GET_SERIAL_NUMBER_LSB     UINT8_C(0x82)

#define SGP41_CMD_SOFT_RESET_MSB            UINT8_C(0x00)
#define SGP41_CMD_SOFT_RESET_LSB            UINT8_C(0x06)



// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct sgp41_descriptor sgp41_t;
typedef struct sgp41_data_descriptor sgp41_data_t;

typedef uint8_t (*sgp41_i2c_funcptr_t)(uint16_t client_address, uint8_t *data, uint8_t size);
typedef void (*sgp41_delay_us_funcptr_t)(uint32_t us);

struct sgp41_data_descriptor{
    uint16_t sraw_voc;
    uint16_t sraw_nox;
    uint8_t serial_number[6];
};

struct sgp41_descriptor{
    uint16_t client_address;
    sgp41_i2c_funcptr_t i2c_write;
    sgp41_i2c_funcptr_t i2c_read;
    sgp41_delay_us_funcptr_t delay_us;
    sgp41_data_t data;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************


uint8_t sgp41_init (sgp41_t *me, uint16_t client_address);

uint8_t sgp41_i2c_write_register (sgp41_t *me, sgp41_i2c_funcptr_t i2c_write_funcptr);

uint8_t sgp41_i2c_read_register (sgp41_t *me, sgp41_i2c_funcptr_t i2c_read_funcptr);

uint8_t sgp41_delay_us_register (sgp41_t *me, sgp41_delay_us_funcptr_t delay_us_funcptr);

uint8_t sgp41_execute_conditioning (sgp41_t *me);

uint8_t sgp41_get_measurement_data (sgp41_t *me, float humidity, float temperature);

uint8_t sgp41_get_measurement_data_not_compensated (sgp41_t *me);

uint8_t sgp41_turn_heater_off (sgp41_t *me);

uint8_t sgp41_get_serial_number (sgp41_t *me);
        

#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of SGP41_H
