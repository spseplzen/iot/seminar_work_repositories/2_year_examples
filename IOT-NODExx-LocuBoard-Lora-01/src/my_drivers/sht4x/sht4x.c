/*******************************************************************************
  SHT4X I2C Driver

  Created by:
    Miroslav Soukup

  File Name:
    sht4x.c

  Summary:
    SHT4X I2C Driver Source File
 
  Version:
    1.0

  Description:
    This file provides basic functions for sht4x sensor.

*******************************************************************************/



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "sht4x.h"



// *****************************************************************************
// Section: Functions
// *****************************************************************************

uint8_t sht4x_init (sht4x_t *me, uint16_t client_address){
    me->client_address = client_address;
    
    if(!sht4x_get_serial_number(me)) return 0;
    
    if(!sht4x_get_measurement_data(me, SHT4X_MEASURE_TYPE_LOW_PRECISION)) return 0;
    
    return 1;
}

uint8_t sht4x_i2c_write_register (sht4x_t *me, sht4x_i2c_funcptr_t i2c_write_funcptr){
    if(i2c_write_funcptr == NULL) return 0;
    me->i2c_write = i2c_write_funcptr;
    return 1;
}


uint8_t sht4x_i2c_read_register (sht4x_t *me, sht4x_i2c_funcptr_t i2c_read_funcptr){
    if(i2c_read_funcptr == NULL) return 0;
    me->i2c_read = i2c_read_funcptr;
    return 1;
}

uint8_t sht4x_delay_us_register (sht4x_t *me, sht4x_delay_us_funcptr_t delay_us_funcptr){
    if(delay_us_funcptr == NULL) return 0;
    me->delay_us = delay_us_funcptr;
    return 1;
}

uint8_t sht4x_get_measurement_data (sht4x_t *me, SHT4X_MEASURE_TYPE_e measure_type){
    uint8_t data[6];
    
    if(measure_type == SHT4X_MEASURE_TYPE_MEDIUM_PRECISION) data[0] = SHT4X_CMD_MEASURE_MPM;
    else if(measure_type == SHT4X_MEASURE_TYPE_HIGH_PRECISION) data[0] = SHT4X_CMD_MEASURE_HPM;
    else if(measure_type == SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER200_1S) data[0] = SHT4X_CMD_MEASURE_HEATER200_1S;
    else if(measure_type == SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER200_0_1S) data[0] = SHT4X_CMD_MEASURE_HEATER200_0_1S;
    else if(measure_type == SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER110_1S) data[0] = SHT4X_CMD_MEASURE_HEATER110_1S;
    else if(measure_type == SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER110_0_1S) data[0] = SHT4X_CMD_MEASURE_HEATER110_0_1S;
    else if(measure_type == SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER20_1S) data[0] = SHT4X_CMD_MEASURE_HEATER20_1S;
    else if(measure_type == SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER20_0_1S) data[0] = SHT4X_CMD_MEASURE_HEATER20_0_1S;
    else data[0] = SHT4X_CMD_MEASURE_LPM;
    if(!me->i2c_write(me->client_address, data, 1)) return 0;
    if(measure_type == SHT4X_MEASURE_TYPE_LOW_PRECISION) me->delay_us(2500);
    else me->delay_us(10000);
    if(!me->i2c_read(me->client_address, data, 6)) return 0;
    
    me->data.temperature = -45 + ((175.0 * (((uint16_t) data[0] << 8) | data[1])) / 65535.0);
    me->data.humidity = -6 + ((125.0 * (((uint16_t) data[3] << 8) | data[4])) / 65535.0);
    
    if(me->data.humidity > 100) me->data.humidity = 100;
    if(me->data.humidity < 0) me->data.humidity = 0;
    
    return 1;
}

uint8_t sht4x_get_serial_number (sht4x_t *me){
    uint8_t data[6];
    
    data[0] = SHT4X_CMD_SERIAL_NUMBER;
    if(!me->i2c_write(me->client_address, data, 1)) return 0;
    me->delay_us(1000);
    if(!me->i2c_read(me->client_address, data, 6)) return 0;
    
    me->data.serial_number = (data[0] << 24) | (data[1] << 16) | (data[3] << 8) | data[4];
    
    return 1;
}
