/*******************************************************************************
  SHT4X I2C Driver

  Created by:
    Miroslav Soukup

  File Name:
    sht4x.h

  Summary:
    SHT4X I2C Driver Header File
 
  Version:
    1.0

  Description:
    This file provides basic functions for sht4x sensor.

*******************************************************************************/

#ifndef SHT4X_H // Protection against multiple inclusion
#define SHT4X_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define SHT4X_I2C_CLIENT_ADDRESS            UINT16_C(0x44)

    
#define SHT4X_CMD_MEASURE_HPM               UINT8_C(0xFD)
#define SHT4X_CMD_MEASURE_MPM               UINT8_C(0xF6)
#define SHT4X_CMD_MEASURE_LPM               UINT8_C(0xE0)

#define SHT4X_CMD_SERIAL_NUMBER             UINT8_C(0x89)
    
#define SHT4X_CMD_SOFT_RESET                UINT8_C(0x94)

#define SHT4X_CMD_MEASURE_HEATER200_1S      UINT8_C(0x39)
#define SHT4X_CMD_MEASURE_HEATER200_0_1S    UINT8_C(0x32)
#define SHT4X_CMD_MEASURE_HEATER110_1S      UINT8_C(0x2F)
#define SHT4X_CMD_MEASURE_HEATER110_0_1S    UINT8_C(0x24)
#define SHT4X_CMD_MEASURE_HEATER20_1S       UINT8_C(0x1E)
#define SHT4X_CMD_MEASURE_HEATER20_0_1S     UINT8_C(0x15)



// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct sht4x_descriptor sht4x_t;
typedef struct sht4x_data_descriptor sht4x_data_t;

typedef uint8_t (*sht4x_i2c_funcptr_t)(uint16_t client_address, uint8_t *data, uint8_t size);
typedef void (*sht4x_delay_us_funcptr_t)(uint32_t us);

typedef enum{
    SHT4X_MEASURE_TYPE_LOW_PRECISION                 = 0,
    SHT4X_MEASURE_TYPE_MEDIUM_PRECISION              = 1,
    SHT4X_MEASURE_TYPE_HIGH_PRECISION                = 2,
    SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER200_1S   = 3,
    SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER200_0_1S = 4,
    SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER110_1S   = 5,
    SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER110_0_1S = 6,
    SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER20_1S    = 7,
    SHT4X_MEASURE_TYPE_HIGH_PRECISION_HEATER20_0_1S  = 8
} SHT4X_MEASURE_TYPE_e;

struct sht4x_data_descriptor{
    float temperature;
    float humidity;
    uint32_t serial_number;
};

struct sht4x_descriptor{
    uint16_t client_address;
    sht4x_i2c_funcptr_t i2c_write;
    sht4x_i2c_funcptr_t i2c_read;
    sht4x_delay_us_funcptr_t delay_us;
    sht4x_data_t data;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************


uint8_t sht4x_init (sht4x_t *me, uint16_t client_address);

uint8_t sht4x_i2c_write_register (sht4x_t *me, sht4x_i2c_funcptr_t i2c_write_funcptr);

uint8_t sht4x_i2c_read_register (sht4x_t *me, sht4x_i2c_funcptr_t i2c_read_funcptr);

uint8_t sht4x_delay_us_register (sht4x_t *me, sht4x_delay_us_funcptr_t delay_us_funcptr);

uint8_t sht4x_get_measurement_data (sht4x_t *me, SHT4X_MEASURE_TYPE_e measure_type);

uint8_t sht4x_get_serial_number (sht4x_t *me);
        

#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of SHT4X_H
