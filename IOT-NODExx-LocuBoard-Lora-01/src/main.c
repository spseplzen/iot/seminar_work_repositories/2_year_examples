#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"
#include <stdio.h>

#define RING_BUFFER_SIZE 256
#include "my_drivers/ring_buffer/ring_buffer.h"

#include "my_drivers/sht4x/sht4x.h"
#include "my_drivers/sgp41/sgp41.h"
#include "my_drivers/sensirion_gas_index_algorithm/sensirion_gas_index_algorithm.h"



#define NODE_HOSTNAME "NODExx"                                  // zmen jmeno nodu

#define NODE_LORA_DEVEUI  "xxxxxxxxxxxxxxxx"                    // zmen Lora deveui
#define NODE_LORA_NWKSKEY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"    // zmen Lora nwkskey
#define NODE_LORA_APPSKEY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"    // zmen Lora appskey
#define NODE_LORA_DEVADDR "xxxxxxxx"                            // zmen Lora devaddr



#define CMWX1ZZABZ_COMMAND_MODUL_RESET      "AT+FACNEW"
#define CMWX1ZZABZ_COMMAND_MODUL_DFORMAT    "AT+DFORMAT"
#define CMWX1ZZABZ_COMMAND_MODUL_MODE       "AT+MODE"
#define CMWX1ZZABZ_COMMAND_LORA_DEVEUI      "AT+DEVEUI"
#define CMWX1ZZABZ_COMMAND_LORA_NWKSKEY     "AT+NWKSKEY"
#define CMWX1ZZABZ_COMMAND_LORA_APPSKEY     "AT+APPSKEY"
#define CMWX1ZZABZ_COMMAND_LORA_DEVADDR     "AT+DEVADDR"



#define CMWX1ZZABZ_DEBUG_PRINT_CHAR(A) (((A) == '\r') ? printf("[CR]") : (((A) == '\n') ? printf("[LF]") : printf("[%c]", (A))));

#define CMWX1ZZABZ_DEBUG_PRINT_ERR(A)                                                                                                                                              \
                            if((A) != CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_NONE) printf("-> ERR - ");                                                                     \
                            if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_COMMAND_UNKNOWN) printf("UKNOWN COMMAND TYPE");                                                \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_PARAMETER_NUMBER_INVALID) printf("PARAMETR NUMBER INVALID");                              \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_PARAMETER_CONTENT_INVALID) printf("PARAMETR CONTENT INVALID");                            \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FACNEW_FAILED) printf("FIRMWARE UPGRADE FAILED");                                         \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_NOT_YET_CONNECTED) printf("LORA NOT YET CONNECTED");                                      \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_ALREADY_CONNECTED) printf("LORA ALREADY CONNECTED");                                      \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_TRANSMISSION_BUSY) printf("TRANSMISSION IS BUSY");                                        \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FIRMWARE_IS_UP_TO_DATE) printf("FIRMWARE IS ALREADY UP TO DATE");                         \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FIRMWARE_INFORMATION_NOT_SET) printf("FIRMWARE INFORMATION NOT SET");                     \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FIRMWARE_FLASH_ERROR) printf("FLASH WRITE/READ ERROR");                                   \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FIRMWARE_UPDATE_FAILED) printf("FIRMWARE UPDATE FAILED");                                 \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_PAYLOAD_IS_TO_BIG) printf("DATA PAYLOAD IS TO BIG");                                      \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_COMMAND_SUPPORTED_ON_ABP) printf("COMMAND IS SUPPORTED ONLY ON ABP");                     \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_COMMAND_SUPPORTED_ON_OTAA) printf("COMMAND IS SUPPORTED ONLY ON OTAA");                   \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_BAND_IS_NOT_SUPPORTED) printf("BAND SETTING IS NOT SUPPORTED");                           \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_POWER_OVER_MAXIMUM_RANGE) printf("POWER VALUE EXCEEDS MAXIMUM");                          \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_COMMAND_UNUSABLE_UNDER_BAND) printf("COMMAND IS UNUSABLE UNDER BAND");                    \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_TX_MAXIMUM_DATA_COUNT) printf("TX IS NOT ALLOW DUE TO DUTY CYCLE LIMITS");                \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_TX_NO_CHANNEL_AVAILABLE) printf("TX NO CHANNEL AVAILABLE DUE TO LBT OR ERROR PARAMS");    \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_TX_MULTIPLE_LINKCHECK_IN_QUEUE) printf("MULTIPLE LinkCheckReq COMMAND IN QUEUE");         \
                            if((A) != CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_NONE) printf("\r\n");

#define DEBUG_PRINT_EVENT(A)                                                                                                                                                                    \
                            if((A) != CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_NONE) printf("-> EVENT - ");                                                                                         \
                            if((A) == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_REBOOTS_SUCESSFULLY) printf("MODULE REBOOTS SUCESSFULLY");                                                    \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_RESTORES_TO_FACTORY_NEW_SUCESSFULLY) printf("MODULE RESTORES TO FACTORY NEW SUCESSFULLY");               \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_ENTERS_BOOTLOADER_MODE) printf("MODULE ENTERS BOOTLOADER MODE");                                         \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LORA_CANT_JOIN) printf("MODULE DOESNT JOIN TO LORA NETWORK");                                                   \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LORA_JOIN_SUCESSFULLY) printf("MODULE JOIN LORA NETWORK SUCESSFULLY");                                          \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LINK_IS_LOST) printf("THE LINK BETWEEN MODEM AND GATEWAY IS LOST");                                             \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LINK_IS_CONNECTED) printf("THE LINK BETWEEN MODEM AND GATEWAY IS CONNECTED");                                   \
                            else if((A) == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LINK_NOT_RECEIVE_ACK) printf("MODULE DOESNT RECEIVE ACK FOR CONFIRMED UPLINK MESSAGE AND WILL RETRANSMISSION"); \
                            if((A) != CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_NONE) printf("\r\n");


typedef enum{
    CMWX1ZZABZ_PARSER_STATE_INIT,
            
    CMWX1ZZABZ_PARSER_STATE_PLUS,
    
    CMWX1ZZABZ_PARSER_STATE_PLUS_OK,
    CMWX1ZZABZ_PARSER_STATE_PLUS_OK_SET,
    
    CMWX1ZZABZ_PARSER_STATE_PLUS_ER_EV,
    
    CMWX1ZZABZ_PARSER_STATE_PLUS_ERR,
    CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_EQUALS_SIGN,
    CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_MINUS_SIGN,
    CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_NUM,
    CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_SET,
    
    CMWX1ZZABZ_PARSER_STATE_PLUS_EVE,
    CMWX1ZZABZ_PARSER_STATE_PLUS_EVEN,
    CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT,
    CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_EQUALS_SIGN,
    CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_NUM1,
    CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_COMMA_SIGN,
    CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_NUM2,
    CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_SET,
    
    CMWX1ZZABZ_PARSER_STATE_PLUS_RE,
    CMWX1ZZABZ_PARSER_STATE_PLUS_REC,
    CMWX1ZZABZ_PARSER_STATE_PLUS_RECV,
    CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_EQUALS_SIGN,
    CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_PORT_NUMBER,
    CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_COMMA_SIGN,
    CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_DATA_LENGTH,
    CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_DATA_READ,
    CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_SET,
    
    CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CR,
    CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLF,
    CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLFCR,
    CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLFCRLF,
            
    CMWX1ZZABZ_PARSER_STATE_COMMAND_ACCEPT,
   
} CMWX1ZZABZ_PARSER_STATE_e;

typedef enum{
    CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE,
    CMWX1ZZABZ_PARSER_COMMAND_TYPE_OK,
    CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR,
    CMWX1ZZABZ_PARSER_COMMAND_TYPE_EVENT,
    CMWX1ZZABZ_PARSER_COMMAND_TYPE_RECV
} CMWX1ZZABZ_PARSER_COMMAND_TYPE_e;

typedef enum{
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_NONE,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_COMMAND_UNKNOWN,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_PARAMETER_NUMBER_INVALID,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_PARAMETER_CONTENT_INVALID,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FACNEW_FAILED,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_NOT_YET_CONNECTED,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_ALREADY_CONNECTED,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_TRANSMISSION_BUSY,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FIRMWARE_IS_UP_TO_DATE,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FIRMWARE_INFORMATION_NOT_SET,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FIRMWARE_FLASH_ERROR,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_FIRMWARE_UPDATE_FAILED,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_PAYLOAD_IS_TO_BIG,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_COMMAND_SUPPORTED_ON_ABP,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_COMMAND_SUPPORTED_ON_OTAA,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_BAND_IS_NOT_SUPPORTED,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_POWER_OVER_MAXIMUM_RANGE,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_COMMAND_UNUSABLE_UNDER_BAND,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_TX_MAXIMUM_DATA_COUNT,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_TX_NO_CHANNEL_AVAILABLE,
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_TX_MULTIPLE_LINKCHECK_IN_QUEUE
} CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_e;

typedef enum{
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_NONE,
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_REBOOTS_SUCESSFULLY,
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_RESTORES_TO_FACTORY_NEW_SUCESSFULLY,
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_ENTERS_BOOTLOADER_MODE,
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LORA_CANT_JOIN,
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LORA_JOIN_SUCESSFULLY,
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LINK_IS_LOST,
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LINK_IS_CONNECTED,
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LINK_NOT_RECEIVE_ACK
} CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_e;

typedef enum{
    CMWX1ZZABZ_INIT_STATE_INIT,
    
    CMWX1ZZABZ_INIT_STATE_RESET,
    CMWX1ZZABZ_INIT_STATE_RESETING1,
    CMWX1ZZABZ_INIT_STATE_RESETING2,
    CMWX1ZZABZ_INIT_STATE_RESETING3,
    
    CMWX1ZZABZ_INIT_STATE_DFORMAT_SET,
    CMWX1ZZABZ_INIT_STATE_DFORMAT_SETTING,
    
    CMWX1ZZABZ_INIT_STATE_MODE_SET,
    CMWX1ZZABZ_INIT_STATE_MODE_SETTING,
    
    CMWX1ZZABZ_INIT_STATE_ABP_DEVEUI_SET,
    CMWX1ZZABZ_INIT_STATE_ABP_DEVEUI_SETTING,
    
    CMWX1ZZABZ_INIT_STATE_ABP_NWKSKEY_SET,
    CMWX1ZZABZ_INIT_STATE_ABP_NWKSKEY_SETTING,
    
    CMWX1ZZABZ_INIT_STATE_ABP_APPSKEY_SET,
    CMWX1ZZABZ_INIT_STATE_ABP_APPSKEY_SETTING,
    
    CMWX1ZZABZ_INIT_STATE_ABP_DEVADDR_SET,
    CMWX1ZZABZ_INIT_STATE_ABP_DEVADDR_SETTING,
    
    CMWX1ZZABZ_INIT_STATE_ERROR,
    CMWX1ZZABZ_INIT_STATE_DONE
} CMWX1ZZABZ_INIT_STATE_e;

typedef enum{
    CMWX1ZZABZ_PARSER_DEBUG_LEVEL_NONE     = 0,
    CMWX1ZZABZ_PARSER_DEBUG_LEVEL_BASIC    = 1,
    CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM   = 2,
    CMWX1ZZABZ_PARSER_DEBUG_LEVEL_EXTENDED = 3,
    CMWX1ZZABZ_PARSER_DEBUG_LEVEL_ALL      = 4
} CMWX1ZZABZ_PARSER_DEBUG_LEVEL_e;


typedef struct cmwx1zzabz_data_descriptor_t{
    uint8_t buffer[256];
    uint16_t buffer_length;
    uint8_t port;
    CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_e err_type;
    CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_e event_type;
} cmwx1zzabz_data_t;


void CMWX1ZZABZ_PARSER_Task(ring_buffer_t *me, CMWX1ZZABZ_PARSER_COMMAND_TYPE_e *pcta, cmwx1zzabz_data_t *data, const CMWX1ZZABZ_PARSER_DEBUG_LEVEL_e dl);
CMWX1ZZABZ_INIT_STATE_e CMWX1ZZABZ_INIT_Task(CMWX1ZZABZ_PARSER_COMMAND_TYPE_e *pcta, cmwx1zzabz_data_t *data, const CMWX1ZZABZ_PARSER_DEBUG_LEVEL_e dl);



volatile uint8_t m2_char = 0;
void m2_usart_rx_callback(uintptr_t context){
    ring_buffer_write((ring_buffer_t *) context, m2_char);
    SERCOM0_USART_Read((void *) &m2_char, 1);
}


void timer0_callback(TC_TIMER_STATUS status, uintptr_t context){
    *((volatile uint8_t *) context) = 1;
}

void timer2_callback(TC_TIMER_STATUS status, uintptr_t context){
    *((volatile uint8_t *) context) = 1;
}


uint8_t i2c_read(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM5_I2C_Read(client_address, data, size);
    while(SERCOM5_I2C_IsBusy());
    return (SERCOM5_I2C_ErrorGet() == SERCOM_I2C_ERROR_NONE) ? 1 : 0;
}

uint8_t i2c_write(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM5_I2C_Write(client_address, data, size);
    while(SERCOM5_I2C_IsBusy());
    return (SERCOM5_I2C_ErrorGet() == SERCOM_I2C_ERROR_NONE) ? 1 : 0;
}

void delay_us(uint32_t us){
    SYSTICK_TimerStart();
    SYSTICK_DelayUs(us);
    SYSTICK_TimerStop();
}


int main ( void ){
    SYS_Initialize(NULL);
    
    printf("CMWX1ZZABZ " NODE_HOSTNAME " initialization...");
    
    
    POWER_M2_ENABLE_Set();
    printf("POWER M2 ENABLED...");
    
    
    ring_buffer_t m2_rb;
    ring_buffer_init(&m2_rb, RING_BUFFER_SIZE);
    SERCOM0_USART_ReadCallbackRegister(m2_usart_rx_callback, (uintptr_t) &m2_rb);
    SERCOM0_USART_Read((void *) &m2_char, 1);
    printf("M2 USART COMMUNICATION INIT...");

    
    volatile uint8_t timer0_flag = 0;
    TC0_TimerCallbackRegister(timer0_callback, (uintptr_t) &timer0_flag);
    TC0_Timer32bitPeriodSet(1966079U * 4);
    TC0_TimerStart();
    printf("TIMER0 FOR PERIODIC LORA DATA SEND INIT...");
    
    
    volatile uint8_t timer2_flag = 1;
    TC2_TimerCallbackRegister(timer2_callback, (uintptr_t) &timer2_flag);
    TC2_TimerStart();
    printf("TIMER2 FOR PERIODIC DATA MEASURE INIT...");
    
    
    sht4x_t sht4x;
    sht4x_i2c_read_register(&sht4x, i2c_read);
    sht4x_i2c_write_register(&sht4x, i2c_write);
    sht4x_delay_us_register(&sht4x, delay_us);
    
    if(sht4x_init(&sht4x, SHT4X_I2C_CLIENT_ADDRESS)){
        printf("SHT4x INIT OK...");
    }
    else{
        printf("SHT4x INIT FAILED...");
    }
    
    
    sgp41_t sgp41;
    sgp41_i2c_read_register(&sgp41, i2c_read);
    sgp41_i2c_write_register(&sgp41, i2c_write);
    sgp41_delay_us_register(&sgp41, delay_us);
    
    if(sgp41_init(&sgp41, SGP41_I2C_CLIENT_ADDRESS)){
        printf("SGP41 INIT OK...");
    }
    else{
        printf("SGP41 INIT FAILED...");
    }
    
    
    GasIndexAlgorithmParams voc_params;
    GasIndexAlgorithm_init(&voc_params, GasIndexAlgorithm_ALGORITHM_TYPE_VOC);
    
    GasIndexAlgorithmParams nox_params;
    GasIndexAlgorithm_init(&nox_params, GasIndexAlgorithm_ALGORITHM_TYPE_NOX);
    
    
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(3000);
    SYSTICK_TimerStop();
    
    printf("done.\r\n");
    
    
    while (true){
        
        SYS_Tasks();
        
        /* ********************************************* CMWX1ZZABZ PARSER ********************************************* */
        static cmwx1zzabz_data_t parser_data;
        static CMWX1ZZABZ_PARSER_DEBUG_LEVEL_e parser_debug_level = CMWX1ZZABZ_PARSER_DEBUG_LEVEL_ALL;
        static CMWX1ZZABZ_PARSER_COMMAND_TYPE_e parser_command_type_accept = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
        CMWX1ZZABZ_PARSER_Task(&m2_rb, &parser_command_type_accept, &parser_data, parser_debug_level);
        /* ************************************************************************************************************* */
        
        
        /* ********************************************** CMWX1ZZABZ INIT ********************************************** */
        CMWX1ZZABZ_INIT_STATE_e init_state = CMWX1ZZABZ_INIT_Task(&parser_command_type_accept, &parser_data, parser_debug_level);
        /* ************************************************************************************************************* */
        
        
        /* ********************************************** APPLICATION CODE ********************************************* */
        static uint8_t app_state = 0;

        switch(app_state){
            case 0:
                if(init_state == CMWX1ZZABZ_INIT_STATE_DONE) app_state = 1;
            break;

            case 1:{
                static int32_t voc_index_value = 0;
                static int32_t nox_index_value = 0;

                if(timer2_flag){
                    timer2_flag = 0;
                    
                    sht4x_get_measurement_data(&sht4x, SHT4X_MEASURE_TYPE_HIGH_PRECISION);
                    
                    sgp41_execute_conditioning(&sgp41);
                    sgp41_get_measurement_data(&sgp41, sht4x.data.humidity, sht4x.data.temperature);
                    sgp41_turn_heater_off(&sgp41);
                    GasIndexAlgorithm_process(&voc_params, sgp41.data.sraw_voc, &voc_index_value);
                    GasIndexAlgorithm_process(&nox_params, sgp41.data.sraw_nox, &nox_index_value);
                }
                
                
                if(timer0_flag){
                    if(!SERCOM0_USART_WriteIsBusy()){
                        timer0_flag = 0;
                        
                        uint8_t _buff[64];
                        
                        uint8_t temp_int = ((uint8_t) sht4x.data.temperature) + 127;
                        uint8_t temp_dec = ((sht4x.data.temperature + 127) * 16) - (temp_int * 16);
                        
                        uint8_t hum_int = (uint8_t) sht4x.data.humidity;
                        uint8_t hum_dec = (sht4x.data.humidity * 16) - (hum_int * 16);
                        
                        sprintf((char *) _buff, "AT+UTX 8\r%02X%02X%02X%02X%04X%04X\r", temp_int, temp_dec, hum_int, hum_dec, (uint16_t) voc_index_value, (uint16_t) nox_index_value);
                        printf("%s\r\n", _buff);
                        SERCOM0_USART_Write((void *) _buff, strlen((char *) _buff));
                    }
                }
                
                
                if(parser_command_type_accept == CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR){
                    parser_command_type_accept = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                    CMWX1ZZABZ_DEBUG_PRINT_ERR(parser_data.err_type);
                }
                
                
                if(parser_command_type_accept == CMWX1ZZABZ_PARSER_COMMAND_TYPE_EVENT){
                    parser_command_type_accept = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                    DEBUG_PRINT_EVENT(parser_data.event_type);
                }
                
                
                if(parser_command_type_accept == CMWX1ZZABZ_PARSER_COMMAND_TYPE_RECV){
                    parser_command_type_accept = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                    printf("-> RECV[%u] - \"", parser_data.port);
                    for(uint16_t i = 0; i < parser_data.buffer_length; ++i) printf("%c", parser_data.buffer[i]);
                    printf("\"\r\n");
                    
                    if(strncmp((char *) parser_data.buffer, "LED", 3) == 0){
                        if(parser_data.buffer[4] == '='){
                            if(parser_data.buffer[3] == '1'){
                                (parser_data.buffer[5] == '1') ? LED1_Set() : LED1_Clear();
                            }
                            else if(parser_data.buffer[3] == '2'){
                                (parser_data.buffer[5] == '1') ? LED2_Set() : LED2_Clear();
                            }
                            else if(parser_data.buffer[3] == '3'){
                                (parser_data.buffer[5] == '1') ? LED3_Set() : LED3_Clear();
                            }
                            else if(parser_data.buffer[3] == '4'){
                                (parser_data.buffer[5] == '1') ? LED4_Set() : LED4_Clear();
                            }
                        }    
                    }
                }
                
                
                if(!timer0_flag && !timer2_flag){
                    PM_StandbyModeEnter(); // enter sleep mode
                }
                
            }break;
        }
        /* ************************************************************************************************************* */

    }

    return (EXIT_FAILURE);
}


void CMWX1ZZABZ_PARSER_Task(ring_buffer_t *me, CMWX1ZZABZ_PARSER_COMMAND_TYPE_e *pcta, cmwx1zzabz_data_t *data, const CMWX1ZZABZ_PARSER_DEBUG_LEVEL_e dl)
{
    //static uint8_t read_char = 0;
    static CMWX1ZZABZ_PARSER_STATE_e parser_state = 0;
    static CMWX1ZZABZ_PARSER_COMMAND_TYPE_e parser_command_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
    static CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_e parser_command_err_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_NONE;
    static uint8_t parser_command_event_num1_parsing = 3;
    static uint8_t parser_command_event_num2_parsing = 3;
    static CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_e parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_NONE;
    static uint8_t parser_command_recv_port_number_parsing = 0;
    static uint16_t parser_command_recv_data_length_parsing = 0;
    static uint16_t parser_command_recv_help_count = 0;
    static uint8_t parser_command_recv_data_parsing[256] = {};

    switch(parser_state){

        case CMWX1ZZABZ_PARSER_STATE_INIT:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == '+') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS;
                else CMWX1ZZABZ_DEBUG_PRINT_CHAR(read_char);
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'O') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_OK;
                else if(read_char == 'E') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_ER_EV;
                else if(read_char == 'R') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_RE;
                else{
                    CMWX1ZZABZ_DEBUG_PRINT_CHAR(read_char);
                    parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
                }
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_OK:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'K') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_OK_SET;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_OK_SET:
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("<OK detected>\r\n");
            parser_command_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_TYPE_OK;
            parser_state = CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CR;
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_ER_EV:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'R') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_ERR;
                else if(read_char == 'V') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_EVE;
                else{
                    CMWX1ZZABZ_DEBUG_PRINT_CHAR(read_char);
                    parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
                }
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_ERR:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'R') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_EQUALS_SIGN;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_EQUALS_SIGN:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == '=') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_MINUS_SIGN;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_MINUS_SIGN:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == '-') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_NUM;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_NUM:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char >= '0' && read_char <= '9'){
                    if(parser_command_err_type_parsing != CMWX1ZZABZ_PARSER_COMMAND_ERR_TYPE_NONE){
                        parser_command_err_type_parsing = parser_command_err_type_parsing * 10;
                        parser_command_err_type_parsing = parser_command_err_type_parsing + (read_char - '0');
                    }
                    else parser_command_err_type_parsing = read_char - '0';
                }
                else if(read_char == '\r') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_SET;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_ERR_SET:
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("<ERR detected>\r\n");
            parser_command_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR;
            parser_state = CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLF;
        break;


        case CMWX1ZZABZ_PARSER_STATE_PLUS_EVE:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'E') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_EVEN;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_EVEN:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'N') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'T') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_EQUALS_SIGN;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_EQUALS_SIGN:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == '=') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_NUM1;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_NUM1:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char >= '0' && read_char <= '2'){
                    parser_command_event_num1_parsing = read_char - '0';
                    parser_state =  CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_COMMA_SIGN;
                }
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_COMMA_SIGN:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == ',') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_NUM2;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_NUM2:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char >= '0' && read_char <= '2'){
                    parser_command_event_num2_parsing = read_char - '0';
                    parser_state =  CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_SET;
                }
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_EVENT_SET:
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("<EVENT detected>\r\n");
            if(parser_command_event_num1_parsing == 0){
                if(parser_command_event_num2_parsing == 0) parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_REBOOTS_SUCESSFULLY;
                else if(parser_command_event_num2_parsing == 1) parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_RESTORES_TO_FACTORY_NEW_SUCESSFULLY;
                else if(parser_command_event_num2_parsing == 2) parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_ENTERS_BOOTLOADER_MODE;
                else parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_NONE;
            }
            else if(parser_command_event_num1_parsing == 1){
                if(parser_command_event_num2_parsing == 0) parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LORA_CANT_JOIN;
                else if(parser_command_event_num2_parsing == 1) parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LORA_JOIN_SUCESSFULLY;
                else parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_NONE;
            }
            else if(parser_command_event_num1_parsing == 2){
                if(parser_command_event_num2_parsing == 0) parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LINK_IS_LOST;
                else if(parser_command_event_num2_parsing == 1) parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LINK_IS_CONNECTED;
                else if(parser_command_event_num2_parsing == 2) parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_LINK_NOT_RECEIVE_ACK;
                else parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_NONE;
            }
            else parser_command_event_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_NONE;
            parser_command_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_TYPE_EVENT;
            parser_state = CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CR;
        break;


        case CMWX1ZZABZ_PARSER_STATE_PLUS_RE:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'E') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_REC;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_REC:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'C') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_RECV;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_RECV:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == 'V') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_EQUALS_SIGN;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_EQUALS_SIGN:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == '=') parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_PORT_NUMBER;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_PORT_NUMBER:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char >= '0' && read_char <= '2'){
                    parser_command_recv_port_number_parsing = read_char - '0';
                    parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_COMMA_SIGN;
                }
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_COMMA_SIGN:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == ','){
                    parser_command_recv_data_length_parsing = 0;
                    parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_DATA_LENGTH;
                }
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_DATA_LENGTH:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char >= '0' && read_char <= '9'){
                    if(parser_command_recv_data_length_parsing != 0){
                        parser_command_recv_data_length_parsing = parser_command_recv_data_length_parsing * 10;
                        parser_command_recv_data_length_parsing = parser_command_recv_data_length_parsing + (read_char - '0');
                    }
                    else parser_command_recv_data_length_parsing = read_char - '0';
                }
                else if(read_char == '\r'){

                    parser_command_recv_help_count = parser_command_recv_data_length_parsing * 2;
                    //printf("%u, %u\r\n", parser_command_recv_data_length_parsing, parser_command_recv_help_count);
                    parser_command_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_TYPE_RECV;
                    parser_state = CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLF;
                }
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_DATA_READ:{
            if(parser_command_recv_help_count == 0) parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_SET;
            else{
                if(!ring_buffer_is_empty(me)){
                    uint8_t read_char;
                    ring_buffer_read(me, &read_char);
                    uint16_t _index = (parser_command_recv_data_length_parsing * 2) - parser_command_recv_help_count;
                    uint8_t _recv_data = (read_char >= '0' && read_char <= '9') ? (read_char - '0') : (read_char - 'A' + 10);
                    parser_command_recv_data_parsing[_index / 2] |= _recv_data << (4 * (!(_index % 2)));
                    --parser_command_recv_help_count;
                }
            }
        }break;

        case CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_SET:
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("<RECV detected>\r\n");
            parser_state = CMWX1ZZABZ_PARSER_STATE_COMMAND_ACCEPT;
        break;


        case CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CR:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == '\r') parser_state = CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLF;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLF:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == '\n') parser_state = CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLFCR;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLFCR:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == '\r') parser_state = CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLFCRLF;
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_COMMAND_END_CRLFCRLF:
            if(!ring_buffer_is_empty(me)){
                uint8_t read_char;
                ring_buffer_read(me, &read_char);
                if(read_char == '\n'){
                    if(parser_command_type_parsing == CMWX1ZZABZ_PARSER_COMMAND_TYPE_RECV){
                        for(uint16_t i = 0; i < 256; ++i) parser_command_recv_data_parsing[i] = 0;
                        parser_state = CMWX1ZZABZ_PARSER_STATE_PLUS_RECV_DATA_READ;
                    }
                    else parser_state = CMWX1ZZABZ_PARSER_STATE_COMMAND_ACCEPT;
                }
                else parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
            }
        break;

        case CMWX1ZZABZ_PARSER_STATE_COMMAND_ACCEPT:
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_EXTENDED) printf("<command accepted>\r\n");
            
            *pcta = parser_command_type_parsing;
            
            if(parser_command_type_parsing == CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR) data->err_type = parser_command_err_type_parsing;
            else if(parser_command_type_parsing == CMWX1ZZABZ_PARSER_COMMAND_TYPE_EVENT) data->event_type = parser_command_event_type_parsing;

            if(parser_command_type_parsing == CMWX1ZZABZ_PARSER_COMMAND_TYPE_RECV){
                data->port = parser_command_recv_port_number_parsing;
                data->buffer_length = parser_command_recv_data_length_parsing;
                for(uint16_t i = 0; i < 256; ++i) data->buffer[i] = parser_command_recv_data_parsing[i];
            }
            
            parser_command_type_parsing = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
            
            parser_state = CMWX1ZZABZ_PARSER_STATE_INIT;
        break;

    }
}


CMWX1ZZABZ_INIT_STATE_e CMWX1ZZABZ_INIT_Task(CMWX1ZZABZ_PARSER_COMMAND_TYPE_e *pcta, cmwx1zzabz_data_t *data, const CMWX1ZZABZ_PARSER_DEBUG_LEVEL_e dl)
{
    static CMWX1ZZABZ_INIT_STATE_e state = CMWX1ZZABZ_INIT_STATE_INIT;

    switch(state){
        case CMWX1ZZABZ_INIT_STATE_INIT:
            state = CMWX1ZZABZ_INIT_STATE_RESET;
        break;

        case CMWX1ZZABZ_INIT_STATE_RESET:{
            uint8_t _buff[] = CMWX1ZZABZ_COMMAND_MODUL_RESET "\r";
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("%s\r\n", (char *) _buff);
            SERCOM0_USART_Write((void *) _buff, sizeof(_buff) - 1);
            while(SERCOM0_USART_WriteIsBusy());
            state = CMWX1ZZABZ_INIT_STATE_RESETING1;
        }break;

        case CMWX1ZZABZ_INIT_STATE_RESETING1:
            if(*pcta != CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE){
                if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_OK){
                    *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                    state = CMWX1ZZABZ_INIT_STATE_RESETING2;
                }
                else if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR){
                    *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                    CMWX1ZZABZ_DEBUG_PRINT_ERR(data->err_type);
                    if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_BASIC) printf("<init not completed>\r\n");
                    state = CMWX1ZZABZ_INIT_STATE_ERROR;
                }
            }
        break;

        case CMWX1ZZABZ_INIT_STATE_RESETING2:
            if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_EVENT){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                if(data->event_type == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_RESTORES_TO_FACTORY_NEW_SUCESSFULLY){
                    state = CMWX1ZZABZ_INIT_STATE_RESETING3;
                }
                else{
                    DEBUG_PRINT_EVENT(data->event_type);
                    if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_BASIC) printf("<init not completed>\r\n");
                    state = CMWX1ZZABZ_INIT_STATE_ERROR;
                }
            }
        break;

        case CMWX1ZZABZ_INIT_STATE_RESETING3:
            if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_EVENT){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                if(data->event_type == CMWX1ZZABZ_PARSER_COMMAND_EVENT_TYPE_MODULE_REBOOTS_SUCESSFULLY){
                    state = CMWX1ZZABZ_INIT_STATE_DFORMAT_SET;
                }
                else{
                    DEBUG_PRINT_EVENT(data->event_type);
                    if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_BASIC) printf("<init not completed>\r\n");
                    state = CMWX1ZZABZ_INIT_STATE_ERROR;
                }
            }
        break;

        case CMWX1ZZABZ_INIT_STATE_DFORMAT_SET:{
            uint8_t _buff[] = CMWX1ZZABZ_COMMAND_MODUL_DFORMAT "=1" "\r";
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("%s\r\n", (char *) _buff);
            SERCOM0_USART_Write((void *) _buff, sizeof(_buff) - 1);
            while(SERCOM0_USART_WriteIsBusy());
            state = CMWX1ZZABZ_INIT_STATE_DFORMAT_SETTING;
        }break;

        case CMWX1ZZABZ_INIT_STATE_DFORMAT_SETTING:
            if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_OK){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_MODE_SET;
            }
            else if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ERROR;
            }
        break;

        case CMWX1ZZABZ_INIT_STATE_MODE_SET:{
            uint8_t _buff[] = CMWX1ZZABZ_COMMAND_MODUL_MODE "=0" "\r";
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("%s\r\n", (char *) _buff);
            SERCOM0_USART_Write((void *) _buff, sizeof(_buff) - 1);
            while(SERCOM0_USART_WriteIsBusy());
            state = CMWX1ZZABZ_INIT_STATE_MODE_SETTING;
        }break;

        case CMWX1ZZABZ_INIT_STATE_MODE_SETTING:
            if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_OK){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ABP_DEVEUI_SET;
            }
            else if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ERROR;
            }
        break;

        case CMWX1ZZABZ_INIT_STATE_ABP_DEVEUI_SET:{
            uint8_t _buff[] = CMWX1ZZABZ_COMMAND_LORA_DEVEUI "=" NODE_LORA_DEVEUI "\r";
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("%s\r\n", (char *) _buff);
            SERCOM0_USART_Write((void *) _buff, sizeof(_buff) - 1);
            while(SERCOM0_USART_WriteIsBusy());
            state = CMWX1ZZABZ_INIT_STATE_ABP_DEVEUI_SETTING;
        }break;

        case CMWX1ZZABZ_INIT_STATE_ABP_DEVEUI_SETTING:
            if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_OK){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ABP_NWKSKEY_SET;
            }
            else if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ERROR;
            }
        break;

        case CMWX1ZZABZ_INIT_STATE_ABP_NWKSKEY_SET:{
            uint8_t _buff[] = CMWX1ZZABZ_COMMAND_LORA_NWKSKEY "=" NODE_LORA_NWKSKEY "\r";
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("%s\r\n", (char *) _buff);
            SERCOM0_USART_Write((void *) _buff, sizeof(_buff) - 1);
            while(SERCOM0_USART_WriteIsBusy());
            state = CMWX1ZZABZ_INIT_STATE_ABP_NWKSKEY_SETTING;
        }break;

        case CMWX1ZZABZ_INIT_STATE_ABP_NWKSKEY_SETTING:
            if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_OK){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ABP_APPSKEY_SET;
            }
            else if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ERROR;
            }
        break;

        case CMWX1ZZABZ_INIT_STATE_ABP_APPSKEY_SET:{
            uint8_t _buff[] = CMWX1ZZABZ_COMMAND_LORA_APPSKEY "=" NODE_LORA_APPSKEY "\r";
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("%s\r\n", (char *) _buff);
            SERCOM0_USART_Write((void *) _buff, sizeof(_buff) - 1);
            while(SERCOM0_USART_WriteIsBusy());
            state = CMWX1ZZABZ_INIT_STATE_ABP_APPSKEY_SETTING;
        }break;

        case CMWX1ZZABZ_INIT_STATE_ABP_APPSKEY_SETTING:
            if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_OK){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ABP_DEVADDR_SET;
            }
            else if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ERROR;
            }
        break;

        case CMWX1ZZABZ_INIT_STATE_ABP_DEVADDR_SET:{
            uint8_t _buff[] = CMWX1ZZABZ_COMMAND_LORA_DEVADDR "=" NODE_LORA_DEVADDR "\r";
            if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_INFORM) printf("%s\r\n", (char *) _buff);
            SERCOM0_USART_Write((void *) _buff, sizeof(_buff) - 1);
            while(SERCOM0_USART_WriteIsBusy());
            state = CMWX1ZZABZ_INIT_STATE_ABP_DEVADDR_SETTING;
        }break;

        case CMWX1ZZABZ_INIT_STATE_ABP_DEVADDR_SETTING:
            if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_OK){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                if(dl >= CMWX1ZZABZ_PARSER_DEBUG_LEVEL_BASIC) printf("<init completed>\r\n");
                state = CMWX1ZZABZ_INIT_STATE_DONE;
            }
            else if(*pcta == CMWX1ZZABZ_PARSER_COMMAND_TYPE_ERR){
                *pcta = CMWX1ZZABZ_PARSER_COMMAND_TYPE_NONE;
                state = CMWX1ZZABZ_INIT_STATE_ERROR;
            }
        break;

        case CMWX1ZZABZ_INIT_STATE_ERROR:

        break;

        case CMWX1ZZABZ_INIT_STATE_DONE:

        break;
    }

    return state;
}
