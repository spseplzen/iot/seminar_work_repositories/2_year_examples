/*******************************************************************************
  ESP32C6 Driver UART

  Created by:
    Miroslav Soukup

  File Name:
    esp32c6_driver_uart.h

  Summary:
    ESP32C6 Driver UART Header File

  Version:
    2.3

  Description:
    This file provides basic functions for esp32c6 communication module.

*******************************************************************************/

#ifndef ESP32C6_DRIVER_UART_H // Protection against multiple inclusion
#define ESP32C6_DRIVER_UART_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdbool.h>
#include <string.h>



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define ESP32C6_MINI_COMMAND_ECHO_DISABLE "ATE0"
#define ESP32C6_MINI_COMMAND_ECHO_ENABLE "ATE1"

#define ESP32C6_MINI_COMMAND_RESTART "AT+RST"

#define ESP32C6_MINI_COMMAND_RESET "AT+RESTORE"

#define ESP32C6_MINI_COMMAND_WIFI_INIT "AT+CWINIT="
#define ESP32C6_MINI_COMMAND_WIFI_DEINIT "AT+CWINIT="
#define ESP32C6_MINI_COMMAND_WIFI_MODE_NULL "AT+CWMODE=0"
#define ESP32C6_MINI_COMMAND_WIFI_MODE_STATION "AT+CWMODE=1"
#define ESP32C6_MINI_COMMAND_WIFI_STATION_CONNECT "AT+CWJAP="
#define ESP32C6_MINI_COMMAND_WIFI_STATION_HOSTNAME "AT+CWHOSTNAME="

#define ESP32C6_MINI_COMMAND_MQTT_CONFIG "AT+MQTTUSERCFG="
#define ESP32C6_MINI_COMMAND_MQTT_CONNECT "AT+MQTTCONN="
#define ESP32C6_MINI_COMMAND_MQTT_SUBSCRIBE "AT+MQTTSUB=0,"
#define ESP32C6_MINI_COMMAND_MQTT_PUBLISH "AT+MQTTPUB=0,"

#define ESP32C6_MINI_COMMAND_HTTP_PUT "AT+HTTPCPUT"
#define ESP32C6_MINI_COMMAND_HTTP_POST "AT+HTTPCPOST"
#define ESP32C6_MINI_COMMAND_HTTP_HEADER "AT+HTTPCHEAD"

// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct esp32c6_descriptor esp32c6_t;
typedef struct esp32c6_data_descriptor esp32c6_data_t;
typedef struct esp32c6_parser_descriptor esp32c6_parser_t;
typedef struct esp32c6_parser_command_descriptor esp32c6_parser_command_t;
typedef struct esp32c6_config_descriptor esp32c6_config_t;
typedef struct esp32c6_mqtt_config_topic_descriptor esp32c6_mqtt_config_topic_t;
typedef struct esp32c6_tasks_state_descriptor esp32c6_tasks_state_t;
typedef struct esp32c6_tasks_descriptor esp32c6_tasks_t;

typedef bool (*esp32c6_rb_read_char_funcptr_t)(uintptr_t rbme, char *c);
typedef bool (*esp32c6_rb_is_empty_funcptr_t)(uintptr_t rbme);
typedef bool (*esp32c6_uart_write_funcptr_t)(char *buffer, size_t size);
typedef bool (*esp32c6_uart_write_is_busy_funcptr_t)(void);

typedef void (*esp32c6_mqtt_callback_funcptr_t)(esp32c6_t *me);


typedef enum{
    ESP32C6_PARSER_COMMAND_TYPE_NONE,
    ESP32C6_PARSER_COMMAND_TYPE_OK,
    ESP32C6_PARSER_COMMAND_TYPE_ERROR,
    ESP32C6_PARSER_COMMAND_TYPE_READY,
    ESP32C6_PARSER_COMMAND_TYPE_MQTT_SUBRECV
} ESP32C6_PARSER_COMMAND_TYPE_e;

typedef enum{
    ESP32C6_DEBUG_LEVEL_NONE     = 0,
    ESP32C6_DEBUG_LEVEL_BASIC    = 1,
    ESP32C6_DEBUG_LEVEL_INFORM   = 2,
    ESP32C6_DEBUG_LEVEL_EXTENDED = 3,
    ESP32C6_DEBUG_LEVEL_ALL      = 4
} ESP32C6_DEBUG_LEVEL_e;

typedef enum{
    ESP32C6_PARSER_STATE_INIT                                   = 0,
    
    
    ESP32C6_PARSER_STATE_PLUS                                   = 1,
    
    ESP32C6_PARSER_STATE_PLUS_MQTT                              = 2,
    
    ESP32C6_PARSER_STATE_PLUS_MQTT_X                            = 3,
            
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV                       = 4,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_DELIM                 = 5,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_LINKID                = 6,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_COMMA1                = 7,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_QUOTATION_MARK1       = 8,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_TOPIC_READ            = 9,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_TOPIC_READING         = 10,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_COMMA2                = 11,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_SIZE_READ             = 12,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_SIZE_READING          = 13,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_DATA_READ             = 14,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_DATA_READING          = 15,
    ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_CR_DONE               = 16,
    
    
    ESP32C6_PARSER_STATE_CHECK_CRLF                             = 17,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_INIT                      = 18,
    
    
    ESP32C6_PARSER_STATE_COMMAND_CRLF_OK                        = 19,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_OK_SET                    = 20,
    
    
    ESP32C6_PARSER_STATE_COMMAND_CRLF_ER                        = 21,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_ERR                       = 22,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_ERRO                      = 23,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_ERROR                     = 24,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_ERROR_SET                 = 25,
    
    
    ESP32C6_PARSER_STATE_COMMAND_CRLF_re                        = 26,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_rea                       = 27,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_read                      = 28,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_ready                     = 29,
    ESP32C6_PARSER_STATE_COMMAND_CRLF_ready_SET                 = 30,
    
    
    ESP32C6_PARSER_STATE_COMMAND_END_CR                         = 31,
    ESP32C6_PARSER_STATE_COMMAND_END_CRLF                       = 32,
    
    
    ESP32C6_PARSER_STATE_COMMAND_ACCEPT                         = 33,
} ESP32C6_PARSER_STATE_e;

typedef enum{
    ESP32C6_INIT_STATE_INIT,
    ESP32C6_INIT_STATE_RESET,
    ESP32C6_INIT_STATE_RESETING1,
    ESP32C6_INIT_STATE_RESETING2,
    ESP32C6_INIT_STATE_ECHO_DISABLE,
    ESP32C6_INIT_STATE_ECHO_DISABLING,
    ESP32C6_INIT_STATE_DONE
} ESP32C6_INIT_STATE_e;

typedef enum{
    ESP32C6_INIT_WIFI_STATE_INIT,
    ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZE,
    ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZATION,
    ESP32C6_INIT_WIFI_STATE_MODE_STATION_SET,
    ESP32C6_INIT_WIFI_STATE_MODE_STATION_SETTING,
    ESP32C6_INIT_WIFI_STATE_HOSTNAME_SET,
    ESP32C6_INIT_WIFI_STATE_HOSTNAME_SETTING,
    ESP32C6_INIT_WIFI_STATE_CONNECT,
    ESP32C6_INIT_WIFI_STATE_CONNECTING,
    ESP32C6_INIT_WIFI_STATE_DISCONECTED,
    ESP32C6_INIT_WIFI_STATE_DONE
} ESP32C6_INIT_WIFI_STATE_e;

typedef enum{
    ESP32C6_INIT_MQTT_STATE_INIT,
    ESP32C6_INIT_MQTT_STATE_CONFIG,
    ESP32C6_INIT_MQTT_STATE_CONFIGURING,
    ESP32C6_INIT_MQTT_STATE_CONNECT,
    ESP32C6_INIT_MQTT_STATE_CONNECTING,
    ESP32C6_INIT_MQTT_STATE_DISCONNECTED,
    ESP32C6_INIT_MQTT_STATE_SUBSCRIBE,
    ESP32C6_INIT_MQTT_STATE_SUBSCRIBING,
    ESP32C6_INIT_MQTT_STATE_SUBSCRIBED,
    ESP32C6_INIT_MQTT_STATE_DONE
} ESP32C6_INIT_MQTT_STATE_e;

typedef enum{
    ESP32C6_INIT_HTTP_STATE_INIT,
    ESP32C6_INIT_HTTP_STATE_CONFIG,
    ESP32C6_INIT_HTTP_STATE_CONFIGURING,
    ESP32C6_INIT_HTTP_STATE_DONE
} ESP32C6_INIT_HTTP_STATE_e;

typedef struct esp32c6_data_descriptor{
    uint8_t buffer[64];
    uint8_t mqtt_topic[128];
    uint8_t mqtt_link_id;
} esp32c6_data_t;

struct esp32c6_mqtt_config_topic_descriptor{
    char *topic;
    esp32c6_mqtt_callback_funcptr_t callback_funcptr;
};

struct esp32c6_config_descriptor{
    char hostname[16];
    
    char wifi_ssid[16];
    char wifi_password[32];
    
    bool mqtt_reconnect_enable;
    char mqtt_broker_ip[16];
    char mqtt_broker_port[8];
    char mqtt_username[32];
    char mqtt_password[32];
    
    bool http_header_enable;
    char *http_header;
    
    esp32c6_mqtt_config_topic_t *mqtt_topics;
    uint8_t mqtt_topics_count;
};

struct esp32c6_tasks_state_descriptor{
    ESP32C6_INIT_STATE_e modul;
    ESP32C6_INIT_WIFI_STATE_e wifi;
    ESP32C6_INIT_MQTT_STATE_e mqtt;
    ESP32C6_INIT_HTTP_STATE_e http;
};

struct esp32c6_parser_command_descriptor{
    ESP32C6_PARSER_COMMAND_TYPE_e type;
};

struct esp32c6_parser_descriptor{
    esp32c6_parser_command_t command;
};

struct esp32c6_tasks_descriptor{
    esp32c6_tasks_state_t state;
    esp32c6_parser_t parser;
};

struct esp32c6_descriptor{
    ESP32C6_DEBUG_LEVEL_e debug_level;
    
    esp32c6_data_t data;
    
    esp32c6_config_t config;
    
    esp32c6_tasks_t tasks;
        
    esp32c6_rb_read_char_funcptr_t rb_read_char_funcptr;
    esp32c6_rb_is_empty_funcptr_t rb_is_empty_funcptr;
    uintptr_t rb_read_char_funcptr_rbme;
    uintptr_t rb_is_empty_funcptr_rbme;
    
    esp32c6_uart_write_funcptr_t write_funcptr;
    esp32c6_uart_write_is_busy_funcptr_t write_is_busy_funcptr;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

bool esp32c6_rb_read_char_register (esp32c6_t *me, esp32c6_rb_read_char_funcptr_t funcptr, uintptr_t rbme);
bool esp32c6_rb_is_empty_register (esp32c6_t *me, esp32c6_rb_is_empty_funcptr_t funcptr, uintptr_t rbme);

bool esp32c6_uart_write_register (esp32c6_t *me, esp32c6_uart_write_funcptr_t funcptr);
bool esp32c6_uart_write_is_busy_register (esp32c6_t *me, esp32c6_uart_write_is_busy_funcptr_t funcptr);

void ESP32C6_PARSER_Task(esp32c6_t *me);
ESP32C6_INIT_STATE_e ESP32C6_Task(esp32c6_t *me);
ESP32C6_INIT_WIFI_STATE_e ESP32C6_WIFI_Task(esp32c6_t *me);
ESP32C6_INIT_MQTT_STATE_e ESP32C6_MQTT_Task(esp32c6_t *me);
ESP32C6_INIT_HTTP_STATE_e ESP32C6_HTTP_Task(esp32c6_t *me);

void ESP32C6_MQTT_SUBSCRIBED_TOPICS_EXECUTE_CALLBACK_Task(esp32c6_t *me);

void esp32c6_configure_init(esp32c6_t *me, char *hostname, ESP32C6_DEBUG_LEVEL_e dl);
void esp32c6_configure_wifi_secure(esp32c6_t *me, char *ssid, char *pswd);
void esp32c6_configure_mqtt_secure(esp32c6_t *me, char *server_ip, char *server_port, char *client_username, char *client_password);
void esp32c6_configure_mqtt_reconnecting(esp32c6_t *me, bool enable);
void esp32c6_configure_mqtt_register_subscribe_topics(esp32c6_t *me, esp32c6_mqtt_config_topic_t *mqtt_topics_array, const uint8_t count);
void esp32c6_configure_http_header(esp32c6_t *me, char *http_header);
void esp32c6_configure_http_no_header(esp32c6_t *me);

ESP32C6_INIT_STATE_e esp32c6_get_init_module_state(esp32c6_t *me);
ESP32C6_INIT_WIFI_STATE_e esp32c6_get_init_wifi_state(esp32c6_t *me);
ESP32C6_INIT_MQTT_STATE_e esp32c6_get_init_mqtt_state(esp32c6_t *me);
ESP32C6_INIT_HTTP_STATE_e esp32c6_get_init_http_state(esp32c6_t *me);

uint16_t esp32c6_get_mqtt_create_message_uint(char *_buff, char *topic, unsigned int value);
uint16_t esp32c6_get_mqtt_create_message_int(char *_buff, char *topic, signed int value);
uint16_t esp32c6_get_mqtt_create_message_float(char *_buff, char *topic, float value, unsigned int decimal_places);



#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End
