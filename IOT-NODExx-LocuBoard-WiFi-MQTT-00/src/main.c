#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"
#include <stdio.h>

#include "my_drivers/ring_buffer/ring_buffer.h"
#include "my_drivers/esp32c6_driver_uart/esp32c6_driver_uart.h"


#define NODE_WIFI_SSID "ssid"       // zmen jmeno WiFi
#define NODE_WIFI_PASSWORD "heslo"  // zmen heslo k WiFi

#define NODE_HOSTNAME               "NODExx"        // jmeno nodu
#define NODE_MQTT_SERVER_IP         "172.16.2.1"    // IP adresu MQTT brokeru
#define NODE_MQTT_SERVER_PORT       "1883"          // PORT pro MQTT broker
#define NODE_MQTT_CLIENT_USERNAME   ""              // uzivatelske jmeno k MQTT brokeru
#define NODE_MQTT_CLIENT_PASSWORD   ""              // heslo k MQTT brokeru


#define NODE_MQTT_BTN_TOPIC_BEGINNING   "home/" NODE_HOSTNAME "/control/btn"



volatile uint8_t module_uart_receive_char;
void module_uart_rx_callback(uintptr_t context){
    ring_buffer_write((ring_buffer_t *) context, module_uart_receive_char);
    SERCOM2_USART_Read((void *) &module_uart_receive_char, 1);
}

bool module_uart_tx_write(char *buffer, size_t size){
    return SERCOM2_USART_Write((void *) buffer, size);
}

bool module_uart_tx_is_busy(void){
    return SERCOM2_USART_WriteIsBusy();
}

bool rb_read_char(uintptr_t rbme, char *c)
{
    return ring_buffer_read((ring_buffer_t *) rbme, (uint8_t *) c);
}

bool rb_is_empty(uintptr_t rbme)
{
    return ring_buffer_is_empty((ring_buffer_t *) rbme);
}




volatile uint8_t timer0_flag = 0;
void timer0_callback(TC_TIMER_STATUS status, uintptr_t context){
    timer0_flag = 1;
}


volatile uint8_t btn1_flag = 0;
void btn_callback(uintptr_t context){
    btn1_flag = 1;
}



void led4_callback(esp32c6_t *me)
{
    bool value = me->data.buffer[0] - '0';
    printf("-> LED4 = %1u\r\n", value);
    if(value) LED4_Set();
    else LED4_Clear();
}


esp32c6_mqtt_config_topic_t mqtt_topics[] =     {
                                                    {"home/" NODE_HOSTNAME "/led4",     led4_callback}            // led4
                                                };

int main(void)
{
    SYS_Initialize(NULL);
    
    
    
    ring_buffer_t rb;
    uint8_t rb_buffer[256];
    ring_buffer_init(&rb, rb_buffer, 256);
    
    SERCOM2_USART_ReadCallbackRegister(module_uart_rx_callback, (uintptr_t) &rb);
    SERCOM2_USART_Read((void *) &module_uart_receive_char, 1);
    
    
    
    esp32c6_t esp32c6 = {};
    
    esp32c6_rb_read_char_register(&esp32c6, rb_read_char, (uintptr_t) &rb); // ring buffer read
    esp32c6_rb_is_empty_register(&esp32c6, rb_is_empty, (uintptr_t) &rb);   // ring buffer is empty
    
    esp32c6_uart_write_register(&esp32c6, module_uart_tx_write);            // uart write
    esp32c6_uart_write_is_busy_register(&esp32c6, module_uart_tx_is_busy);  // uart is busy
    
    esp32c6_configure_init(&esp32c6, NODE_HOSTNAME, ESP32C6_DEBUG_LEVEL_BASIC);  // module config init
    
    esp32c6_configure_wifi_secure(&esp32c6, NODE_WIFI_SSID, NODE_WIFI_PASSWORD); // wifi config

    esp32c6_configure_mqtt_secure(&esp32c6, NODE_MQTT_SERVER_IP, NODE_MQTT_SERVER_PORT, NODE_MQTT_CLIENT_USERNAME, NODE_MQTT_CLIENT_PASSWORD); // mqtt config
    
    esp32c6_configure_mqtt_reconnecting(&esp32c6, true); // mqtt init reconnecting enabled
    
    esp32c6_configure_mqtt_register_subscribe_topics(&esp32c6, mqtt_topics, sizeof(mqtt_topics)/sizeof(*mqtt_topics)); // mqtt subscribe topics config
    
    
    
    EIC_CallbackRegister(EIC_PIN_6, btn_callback, (uintptr_t) NULL); // btn1
    
    
    
    TC0_TimerCallbackRegister(timer0_callback, (uintptr_t) NULL);
    TC0_TimerStart();
    
    
    
    printf("LocuBoard" " " NODE_HOSTNAME " WiFi MQTT example (00) initialization...");
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(3000);
    SYSTICK_TimerStop();
    printf("done.\r\n");
    
    
    
    while(true)
    {
        SYS_Tasks();
        
        
        ESP32C6_PARSER_Task(&esp32c6); // read and parse received messages

        ESP32C6_Task(&esp32c6); // module initialization

        ESP32C6_WIFI_Task(&esp32c6); // initialize WiFi connection

        ESP32C6_MQTT_Task(&esp32c6); // initialize MQTT

        ESP32C6_MQTT_SUBSCRIBED_TOPICS_EXECUTE_CALLBACK_Task(&esp32c6); // read mqtt topic and call callback function
        
        
        if(esp32c6_get_init_mqtt_state(&esp32c6) == ESP32C6_INIT_MQTT_STATE_DONE)
        {
            if(timer0_flag  && !module_uart_tx_is_busy()) // casove omezeni + omezeni odesilani (uart)
            {
                timer0_flag = 0;
                
                if(btn1_flag)
                {
                    btn1_flag = 0;
                    
                    static uint8_t btn_state = 0;
                    btn_state = !btn_state;
                    
                    char _buff[128];
                    uint16_t len = esp32c6_get_mqtt_create_message_uint(_buff, NODE_MQTT_BTN_TOPIC_BEGINNING "1", btn_state);
                    module_uart_tx_write(_buff, len);
                    
                }
            }
        }
        
        
    }

    return EXIT_FAILURE;
}
