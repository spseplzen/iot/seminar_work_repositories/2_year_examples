# Repozitář ukázkové ročníkové práce druhého ročníku

Tato stránka je pouze úvodní stránka pro repozitář ukázkové ročníkové práce z druhého ročníku.

## Zkopírování repozitáře

Můžete použít aplikace pro správu GIT př.: Sourcetree, GitAhead, GitKraken,...
Anebo můžete použít příkazovou řádku a použít příkaz níže:

```bash
git clone git@gitlab.com:spseplzen/iot/seminar_work_repositories/2_year_examples.git
```