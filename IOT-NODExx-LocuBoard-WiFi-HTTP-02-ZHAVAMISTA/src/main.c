#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"
#include <string.h>
#include <stdio.h>

#define RING_BUFFER_SIZE 256
#include "my_drivers/ring_buffer/ring_buffer.h"


#define NODE_WIFI_SSID      "ssid"  // zmen jmeno WiFi
#define NODE_WIFI_PASSWORD  "heslo" // zmen heslo k WiFi

#define NODE_HOSTNAME   "NODE19"                                    // zmen jmeno nodu
#define NODE_SERVER_API_TOKEN "token"                               // zmen API token
#define NODE_SERVER_API_URL   "https://www.zhavamista.cz/api/1.0"   // URL adresa HTTP serveru



#define ESP32C6_MINI_COMMAND_ECHO_DISABLE "ATE0"
#define ESP32C6_MINI_COMMAND_ECHO_ENABLE "ATE1"

#define ESP32C6_MINI_COMMAND_RESTART "AT+RST"

#define ESP32C6_MINI_COMMAND_RESET "AT+RESTORE"

#define ESP32C6_MINI_COMMAND_WIFI_INIT "AT+CWINIT=1"
#define ESP32C6_MINI_COMMAND_WIFI_DEINIT "AT+CWINIT=0"
#define ESP32C6_MINI_COMMAND_WIFI_MODE_NULL "AT+CWMODE=0"
#define ESP32C6_MINI_COMMAND_WIFI_MODE_STATION "AT+CWMODE=1"
#define ESP32C6_MINI_COMMAND_WIFI_STATION_CONNECT "AT+CWJAP="
#define ESP32C6_MINI_COMMAND_WIFI_STATION_HOSTNAME "AT+CWHOSTNAME="

#define ESP32C6_MINI_COMMAND_MQTT_CONFIG "AT+MQTTUSERCFG="
#define ESP32C6_MINI_COMMAND_MQTT_CONNECT "AT+MQTTCONN="
#define ESP32C6_MINI_COMMAND_MQTT_SUBSCRIBE "AT+MQTTSUB=0,"
#define ESP32C6_MINI_COMMAND_MQTT_PUBLISH "AT+MQTTPUB=0,"

#define ESP32C6_MINI_COMMAND_HTTP_PUT "AT+HTTPCPUT"



typedef enum{
    PARSER_COMMAND_TYPE_NONE,
    PARSER_COMMAND_TYPE_OK,
    PARSER_COMMAND_TYPE_ERROR,
    PARSER_COMMAND_TYPE_READY,
    PARSER_COMMAND_TYPE_MQTTSUBRECV
} PARSER_COMMAND_TYPE_e;

typedef enum{
    ESP32C6_PARSER_DEBUG_LEVEL_NONE     = 0,
    ESP32C6_PARSER_DEBUG_LEVEL_BASIC    = 1,
    ESP32C6_PARSER_DEBUG_LEVEL_INFORM   = 2,
    ESP32C6_PARSER_DEBUG_LEVEL_EXTENDED = 3,
    ESP32C6_PARSER_DEBUG_LEVEL_ALL      = 4
} ESP32C6_PARSER_DEBUG_LEVEL_e;

typedef enum{
    PARSER_STATE_INIT,
            
    PARSER_STATE_PLUS,
    PARSER_STATE_PLUS_MQ,
    PARSER_STATE_PLUS_MQT,
    PARSER_STATE_PLUS_MQTT,
    PARSER_STATE_PLUS_MQTTS,
    PARSER_STATE_PLUS_MQTTSU,
    PARSER_STATE_PLUS_MQTTSUB,
    PARSER_STATE_PLUS_MQTTSUBR,
    PARSER_STATE_PLUS_MQTTSUBRE,
    PARSER_STATE_PLUS_MQTTSUBREC,
    PARSER_STATE_PLUS_MQTTSUBRECV,
    PARSER_STATE_PLUS_MQTTSUBRECV_DELIM,
    PARSER_STATE_PLUS_MQTTSUBRECV_LINKID,
    PARSER_STATE_PLUS_MQTTSUBRECV_COMMA1,
    PARSER_STATE_PLUS_MQTTSUBRECV_QUOTATION_MARK1,
    PARSER_STATE_PLUS_MQTTSUBRECV_TOPIC_READ,
    PARSER_STATE_PLUS_MQTTSUBRECV_TOPIC_READING,
    PARSER_STATE_PLUS_MQTTSUBRECV_COMMA2,
    PARSER_STATE_PLUS_MQTTSUBRECV_SIZE_READ,
    PARSER_STATE_PLUS_MQTTSUBRECV_SIZE_READING,
    PARSER_STATE_PLUS_MQTTSUBRECV_DATA_READ,
    PARSER_STATE_PLUS_MQTTSUBRECV_DATA_READING,
    PARSER_STATE_PLUS_MQTTSUBRECV_CR_DONE,
    
    PARSER_STATE_CHECK_CRLF,
    PARSER_STATE_COMMAND_CRLF_INIT,
    
    PARSER_STATE_COMMAND_CRLF_OK,
    PARSER_STATE_COMMAND_CRLF_OK_SET,
    
    PARSER_STATE_COMMAND_CRLF_ER,
    PARSER_STATE_COMMAND_CRLF_ERR,
    PARSER_STATE_COMMAND_CRLF_ERRO,
    PARSER_STATE_COMMAND_CRLF_ERROR,
    PARSER_STATE_COMMAND_CRLF_ERROR_SET,
    
    PARSER_STATE_COMMAND_CRLF_re,
    PARSER_STATE_COMMAND_CRLF_rea,
    PARSER_STATE_COMMAND_CRLF_read,
    PARSER_STATE_COMMAND_CRLF_ready,
    PARSER_STATE_COMMAND_CRLF_ready_SET,
            
    PARSER_STATE_COMMAND_END_CR,
    PARSER_STATE_COMMAND_END_CRLF,
    
    PARSER_STATE_COMMAND_ACCEPT,
} PARSER_STATE_e;

typedef enum{
    ESP32C6_INIT_STATE_INIT,
    ESP32C6_INIT_STATE_RESET,
    ESP32C6_INIT_STATE_RESETING1,
    ESP32C6_INIT_STATE_RESETING2,
    ESP32C6_INIT_STATE_ECHO_DISABLE,
    ESP32C6_INIT_STATE_ECHO_DISABLING,
    ESP32C6_INIT_STATE_DONE
} ESP32C6_INIT_STATE_e;

typedef enum{
    ESP32C6_INIT_WIFI_STATE_INIT,
    ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZE,
    ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZATION,
    ESP32C6_INIT_WIFI_STATE_MODE_STATION_SET,
    ESP32C6_INIT_WIFI_STATE_MODE_STATION_SETTING,
    ESP32C6_INIT_WIFI_STATE_HOSTNAME_SET,
    ESP32C6_INIT_WIFI_STATE_HOSTNAME_SETTING,
    ESP32C6_INIT_WIFI_STATE_CONNECT,
    ESP32C6_INIT_WIFI_STATE_CONNECTING,
    ESP32C6_INIT_WIFI_STATE_DISCONECTED,
    ESP32C6_INIT_WIFI_STATE_DONE
} ESP32C6_INIT_WIFI_STATE_e;

typedef struct esp32c6_data_mqtt_descriptor_t{
    uint8_t mqtt_topic[128];
    uint8_t mqtt_link_id;
} esp32c6_data_mqtt_t;

typedef struct esp32c6_data_descriptor_t{
    uint8_t buffer[64];
    esp32c6_data_mqtt_t mqtt;
} esp32c6_data_t;



void ESP32C6_PARSER_Task(ring_buffer_t *me, PARSER_COMMAND_TYPE_e *pcta, esp32c6_data_t *data, ESP32C6_PARSER_DEBUG_LEVEL_e dl);
ESP32C6_INIT_STATE_e ESP32C6_INIT_Task(PARSER_COMMAND_TYPE_e *pcta, ESP32C6_PARSER_DEBUG_LEVEL_e dl);
ESP32C6_INIT_WIFI_STATE_e ESP32C6_INIT_WIFI_Task(PARSER_COMMAND_TYPE_e *pcta, ESP32C6_INIT_STATE_e state_init, ESP32C6_PARSER_DEBUG_LEVEL_e dl);


int8_t month_to_number(const char *month){
    if (strncmp(month, "Jan", 3) == 0) return 0;
    if (strncmp(month, "Feb", 3) == 0) return 1;
    if (strncmp(month, "Mar", 3) == 0) return 2;
    if (strncmp(month, "Apr", 3) == 0) return 3;
    if (strncmp(month, "May", 3) == 0) return 4;
    if (strncmp(month, "Jun", 3) == 0) return 5;
    if (strncmp(month, "Jul", 3) == 0) return 6;
    if (strncmp(month, "Aug", 3) == 0) return 7;
    if (strncmp(month, "Sep", 3) == 0) return 8;
    if (strncmp(month, "Oct", 3) == 0) return 9;
    if (strncmp(month, "Nov", 3) == 0) return 10;
    if (strncmp(month, "Dec", 3) == 0) return 11;
    return -1;
}

uint8_t get_compile_time(struct tm *time) {
    char month_str[4];
    int day, month, year, hour, minute, second;

    sscanf(__DATE__, "%3s %2d %4d", month_str, &day, &year);
    
    month = month_to_number(month_str);
    if(month == -1) return 0;
    
    sscanf(__TIME__, "%2d:%2d:%2d", &hour, &minute, &second);

    time->tm_mday = day;
    time->tm_mon = month;
    time->tm_year = year - 1900;
    time->tm_hour = hour;
    time->tm_min = minute;
    time->tm_sec = second;
    time->tm_isdst = -1;
    
    return 1;
}


typedef struct bg51_data_descriptor bg51_data_t;

struct bg51_data_descriptor{
    volatile uint8_t ready;
    volatile uint32_t precounter;
    volatile uint32_t counter;
};


void rtc_alarm0( RTC_CLOCK_INT_MASK intCause, uintptr_t context ){
    if ((intCause & RTC_CLOCK_INT_MASK_ALARM0) == RTC_CLOCK_INT_MASK_ALARM0){
        ((bg51_data_t *) context)->ready = 1;
        ((bg51_data_t *) context)->counter = ((bg51_data_t *) context)->precounter;
        ((bg51_data_t *) context)->precounter = 0;
    }
}

void bg51_callback(uintptr_t context){
    ((bg51_data_t *) context)->precounter = ((bg51_data_t *) context)->precounter + 1;
}


volatile uint8_t click_receive_char;
void click_usart_rx_callback(uintptr_t context){
    ring_buffer_write((ring_buffer_t *) context, click_receive_char);
    SERCOM2_USART_Read((void *) &click_receive_char, 1);
}


int main(void)
{
    SYS_Initialize(NULL);
    
    printf("ESP32C6-MINI " NODE_HOSTNAME " " "HTTP" " initialization...");
    
    struct tm time_startup;
    get_compile_time(&time_startup);
    
    RTC_RTCCTimeSet(&time_startup);
    RTC_RTCCAlarmSet(&time_startup, RTC_ALARM_MASK_MMSS);
    RTC_RTCCClockSyncEnable();
    
    
    POWER_5V_ENABLE_Set();
    
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(3000);
    SYSTICK_TimerStop();
    
    
    ring_buffer_t rbc;
    ring_buffer_init(&rbc, RING_BUFFER_SIZE);
    SERCOM2_USART_ReadCallbackRegister(click_usart_rx_callback, (uintptr_t) &rbc);
    SERCOM2_USART_Read((void *) &click_receive_char, 1);
    
    
    bg51_data_t bg51_data = {};
    
    EIC_CallbackRegister(EIC_PIN_10, bg51_callback, (uintptr_t) &bg51_data);
    
    RTC_RTCCCallbackRegister(rtc_alarm0, (uintptr_t) &bg51_data);
    

    printf("done.\r\n");
    
    while(true)
    {
        SYS_Tasks();
        
        static PARSER_COMMAND_TYPE_e parser_command_type_accepted = PARSER_COMMAND_TYPE_NONE;
        static esp32c6_data_t data;
        static ESP32C6_PARSER_DEBUG_LEVEL_e debug_level = ESP32C6_PARSER_DEBUG_LEVEL_BASIC;
        ESP32C6_PARSER_Task(&rbc, &parser_command_type_accepted, &data, debug_level);
        
        
        ESP32C6_INIT_STATE_e esp32c6_state_init = ESP32C6_INIT_Task(&parser_command_type_accepted, debug_level);
        
        ESP32C6_INIT_WIFI_STATE_e esp32c6_state_wifi_init = ESP32C6_INIT_WIFI_Task(&parser_command_type_accepted, esp32c6_state_init, debug_level);
        
        if(esp32c6_state_wifi_init == ESP32C6_INIT_WIFI_STATE_DONE){
            if(bg51_data.ready){
                bg51_data.ready = 0;
                
                uint8_t _buff[128];
                sprintf((char *) _buff, ESP32C6_MINI_COMMAND_HTTP_PUT "\r\n");
                if(debug_level >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("%s", _buff);
                SERCOM2_USART_Write((void *) _buff, strlen((char *) _buff));
                while(SERCOM2_USART_WriteIsBusy());
                
                printf("CPH: %lu, Radiation: %.06f uSv/h\r\n", bg51_data.counter, bg51_data.counter / (5.0 * 60.0));

            }
        }
        
    }

    return EXIT_FAILURE;
}


void ESP32C6_PARSER_Task(ring_buffer_t *me, PARSER_COMMAND_TYPE_e *pcta, esp32c6_data_t *data, ESP32C6_PARSER_DEBUG_LEVEL_e dl){
    static PARSER_STATE_e parser_state = 0;
    static PARSER_COMMAND_TYPE_e parser_command_type_parsing = PARSER_COMMAND_TYPE_NONE;
    uint8_t rbc_char = '\0';
    
    static uint8_t topic_increment = 0;
    static uint8_t data_increment  = 0;
    

    switch(parser_state){
        case PARSER_STATE_INIT:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == '\r') parser_state = PARSER_STATE_CHECK_CRLF;
                else if(rbc_char == '+') parser_state = PARSER_STATE_PLUS;
//                else if(rbc_char == 'W');
            }
        break;
        
        case PARSER_STATE_PLUS:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'M') parser_state = PARSER_STATE_PLUS_MQ;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQ:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'Q') parser_state = PARSER_STATE_PLUS_MQT;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQT:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'T') parser_state = PARSER_STATE_PLUS_MQTT;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTT:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'T') parser_state = PARSER_STATE_PLUS_MQTTS;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTS:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'S') parser_state = PARSER_STATE_PLUS_MQTTSU;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSU:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'U') parser_state = PARSER_STATE_PLUS_MQTTSUB;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUB:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'B') parser_state = PARSER_STATE_PLUS_MQTTSUBR;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBR:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'R') parser_state = PARSER_STATE_PLUS_MQTTSUBRE;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRE:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'E') parser_state = PARSER_STATE_PLUS_MQTTSUBREC;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBREC:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'C') parser_state = PARSER_STATE_PLUS_MQTTSUBRECV;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'V') parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_DELIM;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_DELIM:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == ':') parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_LINKID;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_LINKID:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char >= '0' && rbc_char <= '9'){
                    data->mqtt.mqtt_link_id = rbc_char - '0';
                    parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_COMMA1;
                }
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_COMMA1:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == ',') parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_QUOTATION_MARK1;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_QUOTATION_MARK1:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == '\"') parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_TOPIC_READ;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_TOPIC_READ:
            topic_increment = 0;
            parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_TOPIC_READING;
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_TOPIC_READING:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == '\"'){
                    data->mqtt.mqtt_topic[topic_increment] = '\0';
                    parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_COMMA2;
                }
                else if(rbc_char == '\r') parser_state = PARSER_STATE_CHECK_CRLF;
                else data->mqtt.mqtt_topic[topic_increment++] = rbc_char;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_COMMA2:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == ',') parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_SIZE_READ;
                else parser_state = PARSER_STATE_INIT;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_SIZE_READ:
            // size_increment = 0;
            parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_SIZE_READING;
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_SIZE_READING:
           if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == ',') parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_DATA_READ;
                else if(rbc_char == '\r') parser_state = PARSER_STATE_CHECK_CRLF;
                // else size
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_DATA_READ:
            data_increment = 0;
            parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_DATA_READING;
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_DATA_READING:
           if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == '\r') parser_state = PARSER_STATE_PLUS_MQTTSUBRECV_CR_DONE;
                else data->buffer[data_increment++] = rbc_char;
            }
        break;
        
        case PARSER_STATE_PLUS_MQTTSUBRECV_CR_DONE:
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("<mqttsubrecv detected>\r\n");
            parser_command_type_parsing = PARSER_COMMAND_TYPE_MQTTSUBRECV;
            parser_state = PARSER_STATE_COMMAND_END_CRLF;
        break;

        case PARSER_STATE_CHECK_CRLF:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == '\n') parser_state = PARSER_STATE_COMMAND_CRLF_INIT;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_INIT:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == '\r') parser_state = PARSER_STATE_CHECK_CRLF;
                else if(rbc_char == 'O') parser_state = PARSER_STATE_COMMAND_CRLF_OK;
                else if(rbc_char == 'E') parser_state = PARSER_STATE_COMMAND_CRLF_ER;
                else if(rbc_char == 'r') parser_state = PARSER_STATE_COMMAND_CRLF_re;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_OK:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'K') parser_state = PARSER_STATE_COMMAND_CRLF_OK_SET;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_OK_SET:
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("<OK detected>\r\n");
            parser_command_type_parsing = PARSER_COMMAND_TYPE_OK;
            parser_state = PARSER_STATE_COMMAND_END_CR;
        break;

        case PARSER_STATE_COMMAND_CRLF_ER:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'R') parser_state = PARSER_STATE_COMMAND_CRLF_ERR;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_ERR:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'R') parser_state = PARSER_STATE_COMMAND_CRLF_ERRO;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_ERRO:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'O') parser_state = PARSER_STATE_COMMAND_CRLF_ERROR;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_ERROR:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'R') parser_state = PARSER_STATE_COMMAND_CRLF_ERROR_SET;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_ERROR_SET:
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("<ERROR detected>\r\n");
            parser_command_type_parsing = PARSER_COMMAND_TYPE_ERROR;
            parser_state = PARSER_STATE_COMMAND_END_CR;
        break;


        case PARSER_STATE_COMMAND_CRLF_re:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'e') parser_state = PARSER_STATE_COMMAND_CRLF_rea;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_rea:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'a') parser_state = PARSER_STATE_COMMAND_CRLF_read;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_read:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'd') parser_state = PARSER_STATE_COMMAND_CRLF_ready;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_ready:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == 'y') parser_state = PARSER_STATE_COMMAND_CRLF_ready_SET;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_CRLF_ready_SET:
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("<ready detected>\r\n");
            parser_command_type_parsing = PARSER_COMMAND_TYPE_READY;
            parser_state = PARSER_STATE_COMMAND_END_CR;
        break;            

        case PARSER_STATE_COMMAND_END_CR:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == '\r') parser_state = PARSER_STATE_COMMAND_END_CRLF;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_END_CRLF:
            if(!ring_buffer_is_empty(me)){
                ring_buffer_read(me, &rbc_char);
                if(rbc_char == '\n') parser_state = PARSER_STATE_COMMAND_ACCEPT;
                else parser_state = PARSER_STATE_INIT;
            }
        break;

        case PARSER_STATE_COMMAND_ACCEPT: // command accepted
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_EXTENDED) printf("<command accepted>\r\n");
            *pcta = parser_command_type_parsing;
            parser_state = PARSER_STATE_INIT;
        break;
    }
    
    if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_ALL && rbc_char != '\0') printf("%c", rbc_char);
}


ESP32C6_INIT_STATE_e ESP32C6_INIT_Task(PARSER_COMMAND_TYPE_e *pcta, ESP32C6_PARSER_DEBUG_LEVEL_e dl){
    static ESP32C6_INIT_STATE_e state = ESP32C6_INIT_STATE_INIT;
        
    switch(state){
        case ESP32C6_INIT_STATE_INIT:
            state = ESP32C6_INIT_STATE_RESET;
        break;

        case ESP32C6_INIT_STATE_RESET:{
            uint8_t _buff[128];
            sprintf((char *) _buff, ESP32C6_MINI_COMMAND_RESET "\r\n");
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            SERCOM2_USART_Write((void *) _buff, strlen((char *) _buff));
            while(SERCOM2_USART_WriteIsBusy());
            state = ESP32C6_INIT_STATE_RESETING1;
        }break;

        case ESP32C6_INIT_STATE_RESETING1:
            if(*pcta == PARSER_COMMAND_TYPE_OK){
                *pcta = PARSER_COMMAND_TYPE_NONE;
                state = ESP32C6_INIT_STATE_RESETING2;
            }
        break;

        case ESP32C6_INIT_STATE_RESETING2:
            if(*pcta == PARSER_COMMAND_TYPE_READY){
                *pcta = PARSER_COMMAND_TYPE_NONE;
                state = ESP32C6_INIT_STATE_ECHO_DISABLE;
            }
        break;

        case ESP32C6_INIT_STATE_ECHO_DISABLE:{
            uint8_t _buff[128];
            sprintf((char *) _buff, ESP32C6_MINI_COMMAND_ECHO_DISABLE "\r\n");
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            SERCOM2_USART_Write((void *) _buff, strlen((char *) _buff));
            while(SERCOM2_USART_WriteIsBusy());
            state = ESP32C6_INIT_STATE_ECHO_DISABLING;
        }break;

        case ESP32C6_INIT_STATE_ECHO_DISABLING:
            if(*pcta == PARSER_COMMAND_TYPE_OK){
                *pcta = PARSER_COMMAND_TYPE_NONE;
                if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_BASIC) printf("<init completed>\r\n");
                state = ESP32C6_INIT_STATE_DONE;
            }
        break;

        case ESP32C6_INIT_STATE_DONE:
            
        break;
    }
    
    return state;
}


ESP32C6_INIT_WIFI_STATE_e ESP32C6_INIT_WIFI_Task(PARSER_COMMAND_TYPE_e *pcta, ESP32C6_INIT_STATE_e state_init, ESP32C6_PARSER_DEBUG_LEVEL_e dl){
    static ESP32C6_INIT_WIFI_STATE_e state = ESP32C6_INIT_WIFI_STATE_INIT;
        
    switch(state){
        case ESP32C6_INIT_WIFI_STATE_INIT:
            if(state_init == ESP32C6_INIT_STATE_DONE) state = ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZE;
        break;

        case ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZE:{
            uint8_t _buff[128];
            sprintf((char *) _buff, ESP32C6_MINI_COMMAND_WIFI_INIT "\r\n");
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            SERCOM2_USART_Write((void *) _buff, strlen((char *) _buff));
            while(SERCOM2_USART_WriteIsBusy());
            state = ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZATION;
        }break;
        
        case ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZATION:
            if(*pcta == PARSER_COMMAND_TYPE_OK){
                *pcta = PARSER_COMMAND_TYPE_NONE;
                state = ESP32C6_INIT_WIFI_STATE_MODE_STATION_SET;
            }
        break;
        
        case ESP32C6_INIT_WIFI_STATE_MODE_STATION_SET:{
            uint8_t _buff[128];
            sprintf((char *) _buff, ESP32C6_MINI_COMMAND_WIFI_MODE_STATION "\r\n");
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            SERCOM2_USART_Write((void *) _buff, strlen((char *) _buff));
            while(SERCOM2_USART_WriteIsBusy());
            state = ESP32C6_INIT_WIFI_STATE_MODE_STATION_SETTING;
        }break;
        
        case ESP32C6_INIT_WIFI_STATE_MODE_STATION_SETTING:
            if(*pcta == PARSER_COMMAND_TYPE_OK){
                *pcta = PARSER_COMMAND_TYPE_NONE;
                state = ESP32C6_INIT_WIFI_STATE_HOSTNAME_SET;
            }
        break;
        
        case ESP32C6_INIT_WIFI_STATE_HOSTNAME_SET:{
            uint8_t _buff[128];
            sprintf((char *) _buff, ESP32C6_MINI_COMMAND_WIFI_STATION_HOSTNAME "\"" NODE_HOSTNAME "\"" "\r\n");
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            SERCOM2_USART_Write((void *) _buff, strlen((char *) _buff));
            while(SERCOM2_USART_WriteIsBusy());
            state = ESP32C6_INIT_WIFI_STATE_HOSTNAME_SETTING;
        }break;
        
        case ESP32C6_INIT_WIFI_STATE_HOSTNAME_SETTING:
            if(*pcta == PARSER_COMMAND_TYPE_OK){
                *pcta = PARSER_COMMAND_TYPE_NONE;
                state = ESP32C6_INIT_WIFI_STATE_CONNECT;
            }
        break;
        
        case ESP32C6_INIT_WIFI_STATE_CONNECT:{
            uint8_t _buff[128];
            sprintf((char *) _buff, ESP32C6_MINI_COMMAND_WIFI_STATION_CONNECT "\"" NODE_WIFI_SSID "\",\"" NODE_WIFI_PASSWORD "\"" "\r\n");
            if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            SERCOM2_USART_Write((void *) _buff, strlen((char *) _buff));
            while(SERCOM2_USART_WriteIsBusy());
            state = ESP32C6_INIT_WIFI_STATE_CONNECTING;
        }break;
        
        case ESP32C6_INIT_WIFI_STATE_CONNECTING:
            if(*pcta == PARSER_COMMAND_TYPE_OK){
                *pcta = PARSER_COMMAND_TYPE_NONE;
                if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_BASIC) printf("<init wifi completed>\r\n");
                state = ESP32C6_INIT_WIFI_STATE_DONE;
            }
            else if(*pcta == PARSER_COMMAND_TYPE_ERROR){
                *pcta = PARSER_COMMAND_TYPE_NONE;
                if(dl >= ESP32C6_PARSER_DEBUG_LEVEL_BASIC) printf("<init wifi error>\r\n");
                state = ESP32C6_INIT_WIFI_STATE_DISCONECTED;
            }
        break;
        
        case ESP32C6_INIT_WIFI_STATE_DISCONECTED:
            
        break;
        
        case ESP32C6_INIT_WIFI_STATE_DONE:
            
        break;
    }
    
    return state;
}
