# The following variables contains the files used by the different stages of the build process.
set(PIC32_METEOSTATION_EXAMPLE_default_default_XC32_FILE_TYPE_assemble)
set(PIC32_METEOSTATION_EXAMPLE_default_default_XC32_FILE_TYPE_assembleWithPreprocess)
set_source_files_properties(${PIC32_METEOSTATION_EXAMPLE_default_default_XC32_FILE_TYPE_assembleWithPreprocess} PROPERTIES LANGUAGE C)
set(PIC32_METEOSTATION_EXAMPLE_default_default_XC32_FILE_TYPE_compile
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/exceptions.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/initialization.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/interrupts.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/libc_syscalls.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/clock/plib_clock.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/eic/plib_eic.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/evsys/plib_evsys.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/nvic/plib_nvic.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/nvmctrl/plib_nvmctrl.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/port/plib_port.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/rtc/plib_rtc_timer.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/sercom/i2c_master/plib_sercom0_i2c_master.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/sercom/spi_master/plib_sercom2_spi_master.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/sercom/usart/plib_sercom1_usart.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/sercom/usart/plib_sercom4_usart.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/systick/plib_systick.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/tc/plib_tc0.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/peripheral/tc/plib_tc1.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/startup_xc32.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/stdio/xc32_monitor.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/touch/touch.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/touch/touch_example.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/main.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/my_drivers/as3935/as3935.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/my_drivers/bmp581/bmp581.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/my_drivers/esp32c6_driver_uart/esp32c6_driver_uart.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/my_drivers/scd41/scd41.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/my_drivers/sensirion_gas_index_algorithm/sensirion_gas_index_algorithm.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/my_drivers/sgp41/sgp41.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/my_drivers/sht4x/sht4x.c"
    "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/my_drivers/ssd1306/ssd1306.c")
set(PIC32_METEOSTATION_EXAMPLE_default_default_XC32_FILE_TYPE_compile_cpp)
set(PIC32_METEOSTATION_EXAMPLE_default_default_XC32_FILE_TYPE_link)

# The linker script used for the build.
set(PIC32_METEOSTATION_EXAMPLE_default_LINKER_SCRIPT "${CMAKE_CURRENT_SOURCE_DIR}/../../../src/config/default/PIC32CM5164JH01048.ld")

# The (internal) path to the resulting build image.
set(PIC32_METEOSTATION_EXAMPLE_default_internal_image_name "${CMAKE_CURRENT_SOURCE_DIR}/../../../_build/PIC32_METEOSTATION_EXAMPLE/default/default.elf")

# The name of the resulting image, including namespace for configuration.
set(PIC32_METEOSTATION_EXAMPLE_default_image_name "PIC32_METEOSTATION_EXAMPLE_default_default.elf")

# The name of the image, excluding the namespace for configuration.
set(PIC32_METEOSTATION_EXAMPLE_default_original_image_name "default.elf")

# The output directory of the final image.
set(PIC32_METEOSTATION_EXAMPLE_default_output_dir "${CMAKE_CURRENT_SOURCE_DIR}/../../../out/PIC32_METEOSTATION_EXAMPLE")
