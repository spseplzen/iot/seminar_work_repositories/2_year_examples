#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"

#include "my_drivers/ssd1306/ssd1306.h"
#include "my_drivers/ssd1306/ssd1306_fonts.h"

#include "my_drivers/bmp581/bmp581.h"

#include "my_drivers/as3935/as3935.h"

#include "my_drivers/sht4x/sht4x.h"
#include "my_drivers/sgp41/sgp41.h"
#include "my_drivers/sensirion_gas_index_algorithm/sensirion_gas_index_algorithm.h"

#include "my_drivers/scd41/scd41.h"

#include "my_drivers/ring_buffer/ring_buffer.h"
#include "my_drivers/esp32c6_driver_uart/esp32c6_driver_uart.h"



// Interrupt callback - receiving char from module
volatile uint8_t module_uart_receive_char;
void module_uart_rx_callback(uintptr_t context)
{
    ring_buffer_write((ring_buffer_t *) context, module_uart_receive_char);
    SERCOM4_USART_Read((void *) &module_uart_receive_char, 1);
}

// write function -> write chars to module
bool module_uart_tx_write(char *buffer, size_t size)
{
    return SERCOM4_USART_Write((void *) buffer, size);
}

// write is busy function -> get status of communication
bool module_uart_tx_is_busy(void)
{
    return SERCOM4_USART_WriteIsBusy();
}

void debug_print(char *str)
{
    printf("%s", str);
}


// ring buffer function read and write
bool rb_read_char(uintptr_t rbme, char *c)
{
    return ring_buffer_read((ring_buffer_t *) rbme, (uint8_t *) c);
}

bool rb_is_empty(uintptr_t rbme)
{
    return ring_buffer_is_empty((ring_buffer_t *) rbme);
}


void delay_ms(uint32_t ms)
{
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(ms);
    SYSTICK_TimerStop();
}

void delay_us(uint32_t us)
{
    SYSTICK_TimerStart();
    SYSTICK_DelayUs(us);
    SYSTICK_TimerStop();
}

uint8_t i2c_write(uint16_t client_address, uint8_t *data, uint8_t size)
{
    SERCOM0_I2C_Write(client_address, data, size);
    while(SERCOM0_I2C_IsBusy());
    return !(SERCOM0_I2C_ErrorGet());
}

uint8_t i2c_read(uint16_t client_address, uint8_t *data, uint8_t size)
{
    SERCOM0_I2C_Read(client_address, data, size);
    while(SERCOM0_I2C_IsBusy());
    return !(SERCOM0_I2C_ErrorGet());
}

uint8_t spi_write(uint8_t *data, uint8_t size)
{
    SERCOM2_SPI_Write(data, size);
    while(SERCOM2_SPI_IsBusy());
    return 1;
}

uint8_t spi_read(uint8_t *data, uint8_t size)
{
    SERCOM2_SPI_Read(data, size);
    while(SERCOM2_SPI_IsBusy());
    return 1;
}

void spi_cs_set(void){
    SPI_CS_Set();
}

void spi_cs_clear(void){
    SPI_CS_Clear();
}


volatile uint8_t ubtn_flag = 0;
void ubtn_callback(uintptr_t context)
{
    ubtn_flag = 1;
}

volatile uint8_t tc0_flag = 0;
void tc0_ovf_callback(TC_TIMER_STATUS status, uintptr_t context)
{
    if(status & TC_TIMER_STATUS_OVERFLOW)
        tc0_flag = 1;
}

volatile uint8_t tc1_flag = 0;
void tc1_ovf_callback(TC_TIMER_STATUS status, uintptr_t context)
{
    if(status & TC_TIMER_STATUS_OVERFLOW)
        tc1_flag = 1;
}



void mqtt_callback_uled(esp32c6_t *me)
{
    if(me->data.buffer[0] == '1')
    {
        ULED_Set();
        debug_print("--> ULED ON\r\n");
    }
    else if(me->data.buffer[0] == '0')
    {
        ULED_Clear();
        debug_print("--> ULED OFF\r\n");
    }
    else if(me->data.buffer[0] == '2')
    {
        ULED_Toggle();
        debug_print("--> ULED TOGGLE\r\n");
    }
}


esp32c6_mqtt_config_topic_t mqtt_topics[] = {
                                                {"home/meteo/uled", mqtt_callback_uled} // uled
                                            };



int main ( void )
{
    SYS_Initialize ( NULL );


    // Initial delay
    delay_ms(3000);
    
    
    // init ring buffer
    ring_buffer_t rb;
    uint8_t rb_buffer[256];
    ring_buffer_init(&rb, rb_buffer, 256);
    
    SERCOM4_USART_ReadCallbackRegister(module_uart_rx_callback, (uintptr_t) &rb);
    SERCOM4_USART_Read((void *) &module_uart_receive_char, 1);
    
    
    // ESP32 init
    esp32c6_t esp32c6 = {};

    esp32c6_rb_read_char_register(&esp32c6, rb_read_char, (uintptr_t) &rb); // ring buffer read
    esp32c6_rb_is_empty_register(&esp32c6, rb_is_empty, (uintptr_t) &rb);   // ring buffer is empty
    esp32c6_uart_write_register(&esp32c6, module_uart_tx_write);            // uart write
    esp32c6_uart_write_is_busy_register(&esp32c6, module_uart_tx_is_busy);  // uart is busy
    esp32c6_debug_print_register(&esp32c6, debug_print);  // debug print

    esp32c6_configure_init(&esp32c6, "meteo");  // module config init
    esp32c6_configure_debug(&esp32c6, ESP32C6_DEBUG_LEVEL_NONE);
    esp32c6_configure_wifi_secure(&esp32c6, "SSID", "PSWD"); // wifi config
    //esp32c6_configure_mqtt_unsecure(&esp32c6, "MQTT_SERVER_IP", "1883"); // mqtt config
    esp32c6_configure_mqtt_secure(&esp32c6, "MQTT_SERVER_IP", "8883", "user", "pswd"); // mqtt config
    esp32c6_configure_mqtt_reconnecting(&esp32c6, true); // mqtt init reconnecting enabled
    esp32c6_configure_mqtt_register_subscribe_topics(&esp32c6, mqtt_topics, sizeof(mqtt_topics)/sizeof(*mqtt_topics)); // mqtt subscribe topics config


    // OLED init
    ssd1306_t oled;
    ssd1306_font_t oled_font = {(void *) ssd1306_font5x7, 5, 7};
    
    ssd1306_i2c_write_register(&oled, i2c_write);

    debug_print("SSD1306 INIT...");
    if(ssd1306_init(&oled, SSD1306_I2C_ADDRESS, &oled_font)) debug_print("OK.");
    else                                                     debug_print("FAILED.");
    debug_print("\r\n");


    // BMP581 init
    bmp581_t bmp581;
    
    bmp581_i2c_read_register(&bmp581, i2c_read);
    bmp581_i2c_write_register(&bmp581, i2c_write);
    bmp581_delay_us_register(&bmp581, delay_us);

    bmp581_config_t bmp581_config;
    bmp581_get_default_config(&bmp581_config);

    debug_print("BMP581 INIT...");
    if(bmp581_init(&bmp581, BMP581_I2C_CLIENT_ADDRESS_SDO0, &bmp581_config)) debug_print("OK.");
    else                                                                     debug_print("FAILED.");
    debug_print("\r\n");


    // AS3935 init
    as3935_t thunder;

    as3935_spi_write_register(&thunder, spi_write);
    as3935_spi_read_register(&thunder, spi_read);
    as3935_spi_cs_set_register(&thunder, spi_cs_set);
    as3935_spi_cs_clear_register(&thunder, spi_cs_clear);

    as3935_config_t as3935_config;
    as3935_get_default_config(&as3935_config);

    debug_print("AS3935 INIT...");
    if(as3935_init(&thunder, &as3935_config)) debug_print("OK.");
    else                                      debug_print("FAILED.");
    debug_print("\r\n");


    // SHT4X init
    sht4x_t sht4x;
    
    sht4x_i2c_read_register(&sht4x, i2c_read);
    sht4x_i2c_write_register(&sht4x, i2c_write);
    sht4x_delay_us_register(&sht4x, delay_us);

    debug_print("SHT4x INIT...");
    if(sht4x_init(&sht4x, SHT4X_I2C_CLIENT_ADDRESS)) debug_print("OK.");
    else                                             debug_print("FAILED.");
    debug_print("\r\n");


    // SGP41 init
    sgp41_t sgp41;
    
    sgp41_i2c_read_register(&sgp41, i2c_read);
    sgp41_i2c_write_register(&sgp41, i2c_write);
    sgp41_delay_us_register(&sgp41, delay_us);

    debug_print("SGP41 INIT...");
    if(sgp41_init(&sgp41, SGP41_I2C_CLIENT_ADDRESS)) debug_print(" OK.");
    else                                             debug_print("FAILED.");
    debug_print("\r\n");

    GasIndexAlgorithmParams voc_params;
    GasIndexAlgorithmParams nox_params;
    GasIndexAlgorithm_init(&voc_params, GasIndexAlgorithm_ALGORITHM_TYPE_VOC);
    GasIndexAlgorithm_init(&nox_params, GasIndexAlgorithm_ALGORITHM_TYPE_NOX);


    // SCD41 init
    scd41_t scd41;
    
    scd41_i2c_read_register(&scd41, i2c_read);
    scd41_i2c_write_register(&scd41, i2c_write);
    scd41_delay_ms_register(&scd41, delay_ms);

    scd41_config_t scd41_config;
    scd41_get_default_config(&scd41_config);

    debug_print("SCD41 INIT...");
    if(scd41_init(&scd41, SCD41_I2C_CLIENT_ADDRESS, &scd41_config)) debug_print("OK.");
    else                                                            debug_print("FAILED.");
    debug_print("\r\n");
    
    
    // EIC init
    EIC_CallbackRegister(EIC_PIN_15, ubtn_callback, (uintptr_t) NULL);
    
    
    // Timer init
    TC0_TimerCallbackRegister(tc0_ovf_callback, (uintptr_t) NULL);
    TC1_TimerCallbackRegister(tc1_ovf_callback, (uintptr_t) NULL);
    

    while ( true )
    {
        SYS_Tasks ( );
        
        ESP32C6_PARSER_Task(&esp32c6); // read and parse received messages

        ESP32C6_INIT_Task(&esp32c6); // initialization

        ESP32C6_INIT_WIFI_Task(&esp32c6); // initialize WiFi connection

        ESP32C6_INIT_MQTT_Task(&esp32c6); // initialize MQTT

        ESP32C6_MQTT_SUBSCRIBED_TOPICS_EXECUTE_CALLBACK_Task(&esp32c6); // read mqtt topic and call callback function


        if(esp32c6_get_init_mqtt_state(&esp32c6) == ESP32C6_INIT_MQTT_STATE_DONE)
        {
            static int32_t voc_index_value = 0;
            static int32_t nox_index_value = 0;
            
            
            static uint8_t app_task_state = 0;
            
            // APP Task
            switch(app_task_state)
            {
                // APP init
                case 0:
                    debug_print("APP started.\r\n");
                    TC0_TimerStart();
                    TC1_TimerStart();
                    app_task_state = 1;
                break;
                
                // APP run
                case 1:{
                    
                    if(tc0_flag) // periodic 1s
                    {
                        tc0_flag = 0;
                        
                        debug_print("-> Periodic 1s measure.\r\n");

                        // measure temperature & humidity
                        sht4x_get_measurement_data(&sht4x, SHT4X_MEASURE_TYPE_HIGH_PRECISION);

                        // measure VOC & NOX
                        sgp41_execute_conditioning(&sgp41);
                        sgp41_get_measurement_data(&sgp41, sht4x.data.humidity, sht4x.data.temperature);
                        sgp41_turn_heater_off(&sgp41);
                        GasIndexAlgorithm_process(&voc_params, sgp41.data.sraw_voc, &voc_index_value);
                        GasIndexAlgorithm_process(&nox_params, sgp41.data.sraw_nox, &nox_index_value);

                    }
                    
                    
                    static uint8_t app_data_task_state = 0;
                    static char buff[128];
                    static uint16_t len;
                    
                    // APP DATA Task
                    switch(app_data_task_state)
                    {
                        // APP DATA init
                        case 0:
                            if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> Periodic 5s measure.\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        // APP DATA measure
                        case 1:
                            ssd1306_clear_display(&oled);
                            ssd1306_write_string_position(&oled, 0, 0, "teplota:");
                            ssd1306_write_string_position(&oled, 0, 13, "vlhkost:");
                            ssd1306_write_string_position(&oled, 0, 26, "tlak:");
                            ssd1306_write_string_position(&oled, 0, 39, "VOC:");
                            ssd1306_write_string_position(&oled, 0, 52, "blesky:");
                            
                            
                            sprintf(buff, "%+5.02f C", sht4x.data.temperature);
                            ssd1306_write_string_position(&oled, 50, 0, buff);
                            
                            
                            sprintf(buff, "%5.02f %%", sht4x.data.humidity);
                            ssd1306_write_string_position(&oled, 50, 13, buff);
                            
                            
                            if(scd41_get_device_status(&scd41) && scd41.status.data_ready == SCD41_STATUS_DATA_READY)
                            {
                                scd41_get_data(&scd41);
                            }
                            
                            sprintf(buff, "%ld/%ld/%u", voc_index_value, nox_index_value, scd41.data.co2);
                            ssd1306_write_string_position(&oled, 30, 39, buff);
                            
                            
                            as3935_get_data(&thunder);
                            
                            sprintf(buff, "%u/%lu/%u", thunder.data.minimum_number_of_lightning, thunder.data.lightning_energy / 1000, thunder.data.distance);
                            ssd1306_write_string_position(&oled, 45, 52, buff);
                            
                            
                            if(bmp581_get_device_status(&bmp581) && bmp581.status.int_asserted)
                            {
                                bmp581_get_data(&bmp581);
                                bmp581_convert_data_to_absolute_pressure(&bmp581);
                            }
                            
                            sprintf(buff, "%6.02f/%-6.02f", bmp581.data.pressure / 100.0, bmp581.data.absolute_pressure / 100.0);
                            ssd1306_write_string_position(&oled, 32, 26, buff);
                            
                            
                            app_data_task_state = 2;
                        break;
                        
                        
                        // APP DATA temperature send begin
                        case 2:
                            esp32c6_clear_parser_command_type(&esp32c6);
                            
                            len = esp32c6_get_mqtt_create_message_float(buff, "home/meteo/teplota", sht4x.data.temperature, 2, 0, 0);
                            module_uart_tx_write(buff, len);
                            
                            app_data_task_state = 3;
                        break;
                        
                        // APP DATA temperature send wait
                        case 3:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 4;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        
                        // APP DATA humidity send begin
                        case 4:
                            esp32c6_clear_parser_command_type(&esp32c6);
                            
                            len = esp32c6_get_mqtt_create_message_float(buff, "home/meteo/vlhkost", sht4x.data.humidity, 2, 0, 0);
                            module_uart_tx_write(buff, len);
                            
                            app_data_task_state = 5;
                        break;
                        
                        // APP DATA humidity send wait
                        case 5:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 6;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        
                        // APP DATA pressure send begin
                        case 6:
                            esp32c6_clear_parser_command_type(&esp32c6);
                            
                            len = esp32c6_get_mqtt_create_message_float(buff, "home/meteo/tlak", bmp581.data.pressure / 100.0, 2, 0, 0);
                            module_uart_tx_write(buff, len);
                            
                            app_data_task_state = 7;
                        break;
                        
                        // APP DATA pressure send wait
                        case 7:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 8;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        
                        // APP DATA voc send begin
                        case 8:
                            esp32c6_clear_parser_command_type(&esp32c6);
                            
                            len = esp32c6_get_mqtt_create_message_int(buff, "home/meteo/voc", (int) voc_index_value, 0, 0);
                            module_uart_tx_write(buff, len);
                            
                            app_data_task_state = 9;
                        break;
                        
                        // APP DATA voc send wait
                        case 9:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 10;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        // APP DATA nox send begin
                        case 10:
                            esp32c6_clear_parser_command_type(&esp32c6);
                            
                            len = esp32c6_get_mqtt_create_message_int(buff, "home/meteo/nox", (int) nox_index_value, 0, 0);
                            module_uart_tx_write(buff, len);
                            
                            app_data_task_state = 11;
                        break;
                        
                        // APP DATA nox send wait
                        case 11:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 12;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        
                        // APP DATA co2 send begin
                        case 12:
                            esp32c6_clear_parser_command_type(&esp32c6);
                            
                            len = esp32c6_get_mqtt_create_message_uint(buff, "home/meteo/co2", (unsigned int) scd41.data.co2, 0, 0);
                            module_uart_tx_write(buff, len);
                            
                            app_data_task_state = 13;
                        break;
                        
                        // APP DATA co2 send wait
                        case 13:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 14;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        
                        // APP DATA lightning count send begin
                        case 14:
                            esp32c6_clear_parser_command_type(&esp32c6);
                            
                            len = esp32c6_get_mqtt_create_message_uint(buff, "home/meteo/blesky/pocet", (unsigned int) thunder.data.minimum_number_of_lightning, 0, 0);
                            module_uart_tx_write(buff, len);
                            
                            app_data_task_state = 15;
                        break;
                        
                        // APP DATA lightning count send wait
                        case 15:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 16;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        
                        // APP DATA lightning distance send begin
                        case 16:
                            esp32c6_clear_parser_command_type(&esp32c6);
                            
                            len = esp32c6_get_mqtt_create_message_uint(buff, "home/meteo/blesky/vzdalenost", (unsigned int) thunder.data.distance, 0, 0);
                            module_uart_tx_write(buff, len);
                            
                            app_data_task_state = 17;
                        break;
                        
                        // APP DATA lightning distance send wait
                        case 17:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 18;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        
                        // APP DATA lightning energy send begin
                        case 18:
                            esp32c6_clear_parser_command_type(&esp32c6);
                            
                            len = esp32c6_get_mqtt_create_message_uint(buff, "home/meteo/blesky/energie", (unsigned int) thunder.data.lightning_energy, 0, 0);
                            module_uart_tx_write(buff, len);
                            
                            app_data_task_state = 19;
                        break;
                        
                        // APP DATA lightning energy send wait
                        case 19:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 20;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        
                        // APP DATA ubtn send begin
                        case 20:
                            if(ubtn_flag)
                            {
                                ubtn_flag = 0;
                                
                                esp32c6_clear_parser_command_type(&esp32c6);
                                
                                static uint8_t ubtn_state = 1;
                                len = esp32c6_get_mqtt_create_message_uint(buff, "home/meteo/ubtn", (unsigned int) ubtn_state, 0, 0);
                                ubtn_state = !ubtn_state;
                                module_uart_tx_write(buff, len);

                                app_data_task_state = 21;
                            }
                            else app_data_task_state = 22;
                        break;
                        
                        // APP DATA ubtn send wait
                        case 21:
                            if(esp32c6_get_parser_command_type(&esp32c6) == ESP32C6_PARSER_COMMAND_TYPE_OK)
                                app_data_task_state = 22;
                            else if(tc1_flag)
                            {
                                tc1_flag = 0;
                                debug_print("-> ERR\r\n");
                                app_data_task_state = 1;
                            }
                        break;
                        
                        
                        // APP DATA update display
                        case 22:
                            ssd1306_update_display(&oled);
                            app_data_task_state = 0;
                        break;
                    }

                }break;
            }

        }
        
    }

    return ( EXIT_FAILURE );
}
