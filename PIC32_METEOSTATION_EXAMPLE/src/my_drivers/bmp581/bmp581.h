/*
 * File:   bmp581.h
 * Author: Miroslav Soukup
 * Description: Header file of bmp581 pressure sensor driver.
 * 
 */



#ifndef BMP581_H // Protection against multiple inclusion
#define BMP581_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define BMP581_I2C_CLIENT_ADDRESS_SDO0              UINT8_C(0x46)
#define BMP581_I2C_CLIENT_ADDRESS_SDO1              UINT8_C(0x47)
    
#define BMP581_DEVICE_ID_PRIMARY                    UINT8_C(0x50)
#define BMP581_DEVICE_ID_SECONDARY                  UINT8_C(0x51)

#define BMP581_REG_CHIP_ID                          UINT8_C(0x01)
#define BMP581_REG_REV_ID                           UINT8_C(0x02)
#define BMP581_REG_CHIP_STATUS                      UINT8_C(0x11)
#define BMP581_REG_DRIVE_CONFIG                     UINT8_C(0x13)
#define BMP581_REG_INT_CONFIG                       UINT8_C(0x14)
#define BMP581_REG_INT_SOURCE                       UINT8_C(0x15)
#define BMP581_REG_FIFO_CONFIG                      UINT8_C(0x16)
#define BMP581_REG_FIFO_COUNT                       UINT8_C(0x17)
#define BMP581_REG_FIFO_SEL                         UINT8_C(0x18)
#define BMP581_REG_TEMP_DATA_XLSB                   UINT8_C(0x1D)
#define BMP581_REG_TEMP_DATA_LSB                    UINT8_C(0x1E)
#define BMP581_REG_TEMP_DATA_MSB                    UINT8_C(0x1F)
#define BMP581_REG_PRESS_DATA_XLSB                  UINT8_C(0x20)
#define BMP581_REG_PRESS_DATA_LSB                   UINT8_C(0x21)
#define BMP581_REG_PRESS_DATA_MSB                   UINT8_C(0x22)
#define BMP581_REG_INT_STATUS                       UINT8_C(0x27)
#define BMP581_REG_STATUS                           UINT8_C(0x28)
#define BMP581_REG_FIFO_DATA                        UINT8_C(0x29)
#define BMP581_REG_NVM_ADDR                         UINT8_C(0x2B)
#define BMP581_REG_NVM_DATA_LSB                     UINT8_C(0x2C)
#define BMP581_REG_NVM_DATA_MSB                     UINT8_C(0x2D)
#define BMP581_REG_DSP_CONFIG                       UINT8_C(0x30)
#define BMP581_REG_DSP_IIR                          UINT8_C(0x31)
#define BMP581_REG_OOR_THR_P_LSB                    UINT8_C(0x32)
#define BMP581_REG_OOR_THR_P_MSB                    UINT8_C(0x33)
#define BMP581_REG_OOR_RANGE                        UINT8_C(0x34)
#define BMP581_REG_OOR_CONFIG                       UINT8_C(0x35)
#define BMP581_REG_OSR_CONFIG                       UINT8_C(0x36)
#define BMP581_REG_ODR_CONFIG                       UINT8_C(0x37)
#define BMP581_REG_OSR_EFF                          UINT8_C(0x38)
#define BMP581_REG_CMD                              UINT8_C(0x7E)

#define BMP581_COMMAND_SOFT_RESET                   UINT8_C(0xB6)

#define BMP581_INT_ASSERTED_DRDY                    UINT8_C(0x01)
#define BMP581_INT_ASSERTED_FIFO_FULL               UINT8_C(0x02)
#define BMP581_INT_ASSERTED_FIFO_THRES              UINT8_C(0x04)
#define BMP581_INT_ASSERTED_PRESSURE_OOR            UINT8_C(0x08)
#define BMP581_INT_ASSERTED_POR_SOFTRESET_COMPLETE  UINT8_C(0x10)
#define BMP581_INT_NVM_RDY                          UINT8_C(0x02)
#define BMP581_INT_NVM_ERR                          UINT8_C(0x04)
#define BMP581_INT_NVM_CMD_ERR                      UINT8_C(0x08)
    
#define BMP581_DELAY_US_SOFT_RESET                  UINT16_C(2000)
#define BMP581_DELAY_US_STANDBY                     UINT16_C(2500)
#define BMP581_DELAY_US_NVM_READY_READ              UINT8_C(800)
#define BMP581_DELAY_US_NVM_READY_WRITE             UINT16_C(10000)
    


// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct bmp581_descriptor bmp581_t;
typedef struct bmp581_data_descriptor bmp581_data_t;
typedef struct bmp581_config_descriptor bmp581_config_t;
typedef struct bmp581_status_descriptor bmp581_status_t;

typedef uint8_t (*bmp581_i2c_funcptr_t)(uint16_t client_address, uint8_t *data, uint8_t size);
typedef void (*bmp581_delay_funcptr_t)(uint32_t us);


typedef enum{
    
    BMP581_POWER_MODE_STANDBY      = 0b000, // power-up default
    BMP581_POWER_MODE_NORMAL       = 0b001,
    BMP581_POWER_MODE_FORCED       = 0b010,
    BMP581_POWER_MODE_CONTINOUS    = 0b011,
    BMP581_POWER_MODE_DEEP_STANDBY = 0b100
} BMP581_POWER_MODE_e;

typedef enum{
    BMP581_INT_EN_DISABLE = 0b0, // power-up default
    BMP581_INT_EN_ENABLE  = 0b1
} BMP581_INT_EN_e;

typedef enum{
    BMP581_INT_MODE_PULSED  = 0b0,
    BMP581_INT_MODE_LATCHED = 0b1
} BMP581_INT_MODE_e;

typedef enum{
    BMP581_INT_POLARITY_LOW  = 0b0,
    BMP581_INT_POLARITY_HIGH = 0b1
} BMP581_INT_POLARITY_e;

typedef enum{
    BMP581_INT_DRIVE_PUSHPULL  = 0b0,
    BMP581_INT_DRIVE_OPENDRAIN = 0b1
} BMP581_INT_DRIVE_e;


struct bmp581_config_descriptor{
    BMP581_POWER_MODE_e power_mode;
    
    BMP581_INT_EN_e int_enable;
    BMP581_INT_MODE_e int_mode;
    BMP581_INT_POLARITY_e int_polarity;
    BMP581_INT_DRIVE_e int_drive;
    
    uint8_t oversampling_temperature;
    uint8_t oversampling_pressure;
    
    uint8_t pressure_enable;
    
    uint8_t output_data_rate;
    
    uint8_t iir_filter_temperature;
    uint8_t iir_filter_pressure;
    
    uint8_t iir_shutdown_temperature;
    uint8_t iir_shutdown_pressure;
    
    uint8_t iir_flush_forced_enable;
    
    uint8_t osr_temperature_effective;
    uint8_t osr_pressure_effective;
    
    uint8_t odr_is_valid;
    
    uint8_t drdy_enable;
    uint8_t fifo_full_enable;
    uint8_t fifo_thres_enable;
    uint8_t oor_press_enable;
            
    uint32_t oor_treshold_pressure;
    uint8_t oor_range_pressure;
    uint8_t oor_count_limit;
    
    uint8_t oor_sel_iir_pressure;
    
    uint32_t altitude;
};

struct bmp581_status_descriptor{
    uint8_t int_asserted;
};

struct bmp581_data_descriptor{
    float temperature;
    float pressure;
    float absolute_pressure;
    uint8_t device_id;
};

struct bmp581_descriptor{
    uint16_t client_address;
    bmp581_config_t config;
    bmp581_status_t status;
    bmp581_i2c_funcptr_t i2c_write;
    bmp581_i2c_funcptr_t i2c_read;
    bmp581_delay_funcptr_t delay_us;
    bmp581_data_t data;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

/**
 * \brief Function for initialization bmp581 sensor.
 *
 * \param me pointer to bmp581 sensor type of bmp581_t
 * \param config pointer to bmp581 configuration type of bmp581_config_t
 *
 * \returns status of success / failure
 */
uint8_t bmp581_init (bmp581_t *me, uint16_t client_address, bmp581_config_t *config);

uint8_t bmp581_i2c_write_register (bmp581_t *me, bmp581_i2c_funcptr_t i2c_write_funcptr);

uint8_t bmp581_i2c_read_register (bmp581_t *me, bmp581_i2c_funcptr_t i2c_read_funcptr);

uint8_t bmp581_delay_us_register (bmp581_t *me, bmp581_delay_funcptr_t delay_us_funcptr);

uint8_t bmp581_get_data (bmp581_t *me);

void bmp581_convert_data_to_absolute_pressure (bmp581_t *me);

uint8_t bmp581_i2c_check (bmp581_t *me);

uint8_t bmp581_set_power_mode (bmp581_t *me, BMP581_POWER_MODE_e power_mode);

uint8_t bmp581_get_device_id (bmp581_t *me);

uint8_t bmp581_get_device_status (bmp581_t *me);

void bmp581_get_default_config(bmp581_config_t *config);



#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of BMP581_H
