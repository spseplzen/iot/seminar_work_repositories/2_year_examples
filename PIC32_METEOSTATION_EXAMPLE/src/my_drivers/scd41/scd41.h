/*
 * File:   scd41.h
 * Author: Miroslav Soukup
 * Description: Header file of scd41 CO2 sensor driver.
 * 
 */



#ifndef SCD41_H // Protection against multiple inclusion
#define SCD41_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define SCD41_I2C_CLIENT_ADDRESS                        UINT8_C(0x62)


#define SCD41_CMD_MEASUREMENT_PERIODIC_START            UINT16_C(0x21B1)
#define SCD41_CMD_MEASUREMENT_PERIODIC_LOW_POWER_START  UINT16_C(0x21AC)
#define SCD41_CMD_MEASUREMENT_PERIODIC_STOP             UINT16_C(0x3F86)
#define SCD41_CMD_MEASUREMENT_SINGLE_START              UINT16_C(0x219d)
#define SCD41_CMD_PERSIST_SETTINGS                      UINT16_C(0x3615)
#define SCD41_CMD_PERFORM_FACTORY_RESET                 UINT16_C(0x3632)
#define SCD41_CMD_REINIT                                UINT16_C(0x3646)
    
#define SCD41_GET_MEASUREMENT                           UINT16_C(0xEC05)
#define SCD41_GET_TEMPERATURE_OFFSET                    UINT16_C(0x2318)
#define SCD41_GET_ALTITUDE                              UINT16_C(0x2322)
#define SCD41_GET_DATA_READY_STATUS                     UINT16_C(0xe4b8)
#define SCD41_GET_SERIAL_NUMBER                         UINT16_C(0x3682)

#define SCD41_SET_TEMPERATURE_OFFSET                    UINT16_C(0x241D)
#define SCD41_SET_ALTITUDE                              UINT16_C(0x2427)
#define SCD41_SET_AMBIENT_PRESSURE                      UINT16_C(0xE000)
#define SCD41_SET_AUTOMATIC_SELF_CALIBRATION            UINT16_C(0x2416)



// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct scd41_descriptor scd41_t;
typedef struct scd41_data_descriptor scd41_data_t;
typedef struct scd41_config_descriptor scd41_config_t;
typedef struct scd41_status_descriptor scd41_status_t;

typedef uint8_t (*scd41_i2c_rw_funcptr_t)(uint16_t client_address, uint8_t *data, uint8_t size);
typedef void (*scd41_delay_ms_funcptr_t)(uint32_t ms);


typedef enum{
    SCD41_CONFIG_AUTO_SELF_CALIB_OFF = 0b0,
    SCD41_CONFIG_AUTO_SELF_CALIB_ON  = 0b1
} SCD41_CONFIG_AUTO_SELF_CALIB_e;

typedef enum{
    SCD41_CONFIG_MEAS_MODE_SINGLE_SHOT        = 0b00, // 5 s
    SCD41_CONFIG_MEAS_MODE_PERIODIC           = 0b01, // each 5 s
    SCD41_CONFIG_MEAS_MODE_PERIODIC_LOW_POWER = 0b10  // each 30 s
} SCD41_CONFIG_MEAS_MODE_e;

typedef enum{
    SCD41_CONFIG_PERS_SETTINGS_DO_NOT_SAVE = 0b0,
    SCD41_CONFIG_PERS_SETTINGS_SAVE        = 0b1
} SCD41_CONFIG_PERS_SETTINGS_e;

typedef enum{
    SCD41_STATUS_DATA_NOT_READY = 0b0,
    SCD41_STATUS_DATA_READY     = 0b1
} SCD41_STATUS_DATA_e;



struct scd41_status_descriptor{
    uint8_t data_ready;
};

struct scd41_config_descriptor{
    float temp_offset;
    uint16_t altitude;
    uint32_t ambient_press;
    
    SCD41_CONFIG_AUTO_SELF_CALIB_e auto_self_calib;
    
    SCD41_CONFIG_MEAS_MODE_e meas_mode;
    
    SCD41_CONFIG_PERS_SETTINGS_e pers_settings;
};

struct scd41_data_descriptor{
    uint64_t serial_number;
    
    uint16_t co2;
    int8_t temp;
    uint8_t hum;
};

struct scd41_descriptor{
    uint16_t client_address;
    
    scd41_config_t config;
    
    scd41_status_t status;
    
    scd41_i2c_rw_funcptr_t i2c_write;
    scd41_i2c_rw_funcptr_t i2c_read;
    
    scd41_delay_ms_funcptr_t delay_ms;
    
    scd41_data_t data;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

/**
 * \brief Function for initialization scd41 module.
 *
 * \param me pointer to scd41 sensor type of scd41_t
 * \param config pointer to scd41 configuration type of scd41_config_t
 *
 * \returns status of success / failure
 */
uint8_t scd41_init (scd41_t *me, uint16_t client_address, scd41_config_t *config);

uint8_t scd41_i2c_write_register (scd41_t *me, scd41_i2c_rw_funcptr_t i2c_write_funcptr);

uint8_t scd41_i2c_read_register (scd41_t *me, scd41_i2c_rw_funcptr_t i2c_read_funcptr);

uint8_t scd41_delay_ms_register (scd41_t *me, scd41_delay_ms_funcptr_t delay_ms_funcptr);

uint8_t scd41_get_data (scd41_t *me);

uint8_t scd41_get_serial_number (scd41_t *me);

uint8_t scd41_i2c_check (scd41_t *me);

uint8_t scd41_get_device_status (scd41_t *me);

uint8_t scd41_perform_persist_settings (scd41_t *me);

uint8_t scd41_perform_factory_reset (scd41_t *me);

uint8_t scd41_perform_measure_single_shot (scd41_t *me);

void scd41_get_default_config(scd41_config_t *config);


#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of SCD41_H
