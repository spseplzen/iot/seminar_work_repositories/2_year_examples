/*
 * File:   sgp41.c
 * Author: Miroslav Soukup
 * Description: Source file of sgp41 VOC sensor driver.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "sgp41.h"



// *****************************************************************************
// Section: Functions
// *****************************************************************************

static uint8_t _sgp41_calc_crc(uint8_t data[2])
{
    uint8_t crc = 0xFF;
    for(uint8_t i = 0; i < 2; i++){
        crc ^= data[i];
        for(uint8_t bit = 8; bit > 0; --bit){
            if(crc & 0x80) {
                crc = (crc << 1) ^ 0x31u;
            }
            else{
                crc = (crc << 1);
            }
        }
    }
    return crc;
}

uint8_t sgp41_init (sgp41_t *me, uint16_t client_address)
{
    if(!me) return 0;
    
    me->client_address = client_address;
    
    if(!sgp41_get_serial_number(me)) return 0;
    
    if(!sgp41_execute_conditioning(me)) return 0;
    
    if(!sgp41_get_measurement_data_not_compensated(me)) return 0;
    
    if(!sgp41_turn_heater_off(me)) return 0;
    
    return 1;
}

uint8_t sgp41_i2c_write_register (sgp41_t *me, sgp41_i2c_funcptr_t i2c_write_funcptr)
{
    if(!me || i2c_write_funcptr == NULL) return 0;
    me->i2c_write = i2c_write_funcptr;
    return 1;
}


uint8_t sgp41_i2c_read_register (sgp41_t *me, sgp41_i2c_funcptr_t i2c_read_funcptr)
{
    if(!me || i2c_read_funcptr == NULL) return 0;
    me->i2c_read = i2c_read_funcptr;
    return 1;
}

uint8_t sgp41_delay_us_register (sgp41_t *me, sgp41_delay_us_funcptr_t delay_us_funcptr)
{
    if(!me || delay_us_funcptr == NULL) return 0;
    me->delay_us = delay_us_funcptr;
    return 1;
}

uint8_t sgp41_execute_conditioning (sgp41_t *me)
{
    if(!me) return 0;
    
    uint8_t data[8] =   {
                            SGP41_CMD_EXECUTE_CONDITIONING_MSB,
                            SGP41_CMD_EXECUTE_CONDITIONING_LSB,
                            0x80,
                            0x00,
                            0xA2,
                            0x66,
                            0x66,
                            0x93
                        };
    
    if(!me->i2c_write(me->client_address, data, 8)) return 0;
    me->delay_us(50000);
    if(!me->i2c_read(me->client_address, data, 3)) return 0;
    
    me->data.sraw_voc = (((uint16_t) data[0]) << 8) | data[1];
    
    return 1;
}

uint8_t sgp41_get_measurement_data (sgp41_t *me, float humidity, float temperature)
{
    if(!me) return 0;
    
    uint16_t _humidity = (humidity * 65535.0) / 100.0;
    uint16_t _temperature = ((temperature + 45) * 65535.0) / 175.0;
    
    uint8_t _hum[2] = {(_humidity >> 8) & 0xFF, _humidity & 0xFF};
    uint8_t _temp[2] = {(_temperature >> 8) & 0xFF, _temperature & 0xFF};
    
    uint8_t _hum_crc = _sgp41_calc_crc(_hum);
    uint8_t _temp_crc = _sgp41_calc_crc(_temp);
    
    uint8_t data[8] =   {
                            SGP41_CMD_MEASURE_RAW_SIGNALS_MSB,
                            SGP41_CMD_MEASURE_RAW_SIGNALS_LSB,
                            _hum[0],
                            _hum[1],
                            _hum_crc,
                            _temp[0],
                            _temp[1],
                            _temp_crc
                        };
    
    if(!me->i2c_write(me->client_address, data, 8)) return 0;
    me->delay_us(50000);
    if(!me->i2c_read(me->client_address, data, 6)) return 0;
    
    me->data.sraw_voc = (((uint16_t) data[0]) << 8) | data[1];
    me->data.sraw_nox = (((uint16_t) data[3]) << 8) | data[4];
    
    return 1;
}

uint8_t sgp41_get_measurement_data_not_compensated (sgp41_t *me)
{
    if(!me) return 0;
    if(!sgp41_get_measurement_data(me, 50.0, 25.0)) return 0;
    return 1;
}

uint8_t sgp41_turn_heater_off (sgp41_t *me)
{
    if(!me) return 0;
    
    uint8_t data[2] =   {
                            SGP41_CMD_TURN_HEATER_OFF_MSB,
                            SGP41_CMD_TURN_HEATER_OFF_LSB
                        };
    
    if(!me->i2c_write(me->client_address, data, 2)) return 0;
    me->delay_us(1000);
    
    return 1;
}

uint8_t sgp41_get_serial_number (sgp41_t *me)
{
    if(!me) return 0;
    
    uint8_t data[9];
    
    data[0] = SGP41_CMD_GET_SERIAL_NUMBER_MSB;
    data[1] = SGP41_CMD_GET_SERIAL_NUMBER_LSB;
    if(!me->i2c_write(me->client_address, data, 2)) return 0;
    me->delay_us(1000);
    if(!me->i2c_read(me->client_address, data, 9)) return 0;
    
    me->data.serial_number[0] = data[0];
    me->data.serial_number[1] = data[1];
    me->data.serial_number[2] = data[3];
    me->data.serial_number[3] = data[4];
    me->data.serial_number[4] = data[6];
    me->data.serial_number[5] = data[7];
    
    return 1;
}
