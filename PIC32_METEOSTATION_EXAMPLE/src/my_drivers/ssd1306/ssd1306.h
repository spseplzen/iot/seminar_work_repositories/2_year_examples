/*
 * File:   ssd1306.h
 * Author: Miroslav Soukup
 * Description: Header file of ssd1306 OLED display driver.
 * 
 */

#ifndef SSD1306_H // Protection against multiple inclusion
#define SSD1306_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define SSD1306_I2C_ADDRESS     UINT16_C(0x3C)

#define SSD1306_DISPLAY_WIDTH 128
#define SSD1306_DISPLAY_HEIGHT 64
#define SSD1306_DISPLAY_PAGE_COUNT 8

#define SSD1306_DISPLAY_PIXELS_COUNT (SSD1306_DISPLAY_WIDTH * SSD1306_DISPLAY_HEIGHT)
#define SSD1306_DISPLAY_PIXELS_COUNT_BYTES (SSD1306_DISPLAY_PIXELS_COUNT / 8)

#define SSD1306_I2C_COMMAND_DISPLAY_OFF             UINT8_C(0xAE)
#define SSD1306_I2C_COMMAND_DISPLAY_ON              UINT8_C(0xAF)

#define SSD1306_I2C_COMMAND_DISPLAY_CLOCK_DIVISOR   UINT8_C(0xD5)
#define SSD1306_I2C_COMMAND_DISPLAY_MULTIPLEX       UINT8_C(0xA8)
#define SSD1306_I2C_COMMAND_DISPLAY_OFFSET          UINT8_C(0xD3)
#define SSD1306_I2C_COMMAND_CHARGE_PUMP             UINT8_C(0x8D)
#define SSD1306_I2C_COMMAND_MEMORY_MODE             UINT8_C(0x20)
#define SSD1306_I2C_COMMAND_SEGMENT_REMAP           UINT8_C(0xA1)
#define SSD1306_I2C_COMMAND_COM_SCAN_DIRECTION      UINT8_C(0xC8)
#define SSD1306_I2C_COMMAND_COM_PINS_CONFIG         UINT8_C(0xDA)
#define SSD1306_I2C_COMMAND_CONTRAST                UINT8_C(0x81)
#define SSD1306_I2C_COMMAND_PRECHARGE_PERIOD        UINT8_C(0xD9)
#define SSD1306_I2C_COMMAND_VCOMH_DESELECT_LEVEL    UINT8_C(0xDB)
#define SSD1306_I2C_COMMAND_NORMAL_DISPLAY          UINT8_C(0xA6)
#define SSD1306_I2C_COMMAND_INVERT_DISPLAY          UINT8_C(0xA7)



// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct ssd1306_descriptor ssd1306_t;
typedef struct ssd1306_font_decriptor ssd1306_font_t;
typedef struct ssd1306_position_descriptor ssd1306_position_t;

typedef uint8_t (*ssd1306_i2c_funcptr_t)(uint16_t client_address, uint8_t *data, uint8_t size);

struct ssd1306_font_decriptor{
    void *font_ptr;
    uint8_t font_width;
    uint8_t font_height;
};

struct ssd1306_position_descriptor{
    uint16_t x;
    uint16_t y;
};

struct ssd1306_descriptor{
    uint16_t client_address;
    
    uint8_t buffer[SSD1306_DISPLAY_PIXELS_COUNT_BYTES];
    
    ssd1306_i2c_funcptr_t i2c_write;
    
    ssd1306_position_t position;
    
    ssd1306_font_t font;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

uint8_t ssd1306_init(ssd1306_t *me, uint16_t client_address, ssd1306_font_t *font);

uint8_t ssd1306_i2c_write_register (ssd1306_t *me, ssd1306_i2c_funcptr_t i2c_write_funcptr);

void ssd1306_clear_display(ssd1306_t *me);

void ssd1306_write_pixel(ssd1306_t *me, uint16_t x, uint16_t y, bool value);

void ssd1306_write_char(ssd1306_t *me, uint16_t x, uint16_t y, char c);

void ssd1306_write_string(ssd1306_t *me, const char *str);

void ssd1306_write_string_position(ssd1306_t *me, uint16_t x, uint16_t y, const char *str);

uint8_t ssd1306_update_display_partial(ssd1306_t *me, uint16_t x0, uint16_t x1, uint16_t y0, uint16_t y1);

uint8_t ssd1306_update_display(ssd1306_t *me);

void ssd1306_draw_line(ssd1306_t *me, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);

void ssd1306_draw_square_empty(ssd1306_t *me, uint16_t x, uint16_t y, uint16_t size);

void ssd1306_draw_square_fill(ssd1306_t *me, uint16_t x, uint16_t y, uint16_t size);

void ssd1306_draw_circle_empty(ssd1306_t *me, uint16_t x0, uint16_t y0, uint16_t radius);

void ssd1306_draw_circle_fill(ssd1306_t *me, uint16_t x0, uint16_t y0, uint16_t radius);

void ssd1306_set_font(ssd1306_t *me, void *font_ptr, uint16_t width, uint16_t height);

void ssd1306_display_invert(ssd1306_t *me, bool invert);

void ssd1306_set_contrast(ssd1306_t *me, uint8_t contrast);


#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of SSD1306_H
