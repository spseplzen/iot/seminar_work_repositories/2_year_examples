/*
 * File:   ssd1306.c
 * Author: Miroslav Soukup
 * Description: Source file of SSD1306 OLED display driver.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdbool.h>
#include <stdlib.h>

#include "ssd1306.h"



// *****************************************************************************
// Section: Functions
// *****************************************************************************

inline int32_t _ssd1306_abs(int32_t v){
    return (v < 0) ? ((-1) * v) : v;
}

inline uint8_t _ssd1306_send_command(ssd1306_t *me, uint8_t command) {
    if(!me) return 0;
    uint8_t _buff[2] = {0x00, command};
    return me->i2c_write(me->client_address, _buff, 2);
}

inline uint8_t _ssd1306_send_data(ssd1306_t *me, uint8_t data) {
    if(!me) return 0;
    uint8_t _buff[2] = {0x40, data};
    return me->i2c_write(me->client_address, _buff, 2);
}

uint8_t ssd1306_init(ssd1306_t *me, uint16_t client_address, ssd1306_font_t *font)
{
    if(!me || !font) return 0;
    
    me->client_address = client_address;
    
    me->position.x = 0;
    me->position.y = 0;
    
    me->font = *font;
    
    ssd1306_clear_display(me);
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_DISPLAY_OFF)) return 0; // display off
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_DISPLAY_CLOCK_DIVISOR)) return 0; // clock frequency
    if(!_ssd1306_send_command(me, 0x80)) return 0;
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_DISPLAY_MULTIPLEX)) return 0; // multiplex set
    if(!_ssd1306_send_command(me, SSD1306_DISPLAY_HEIGHT - 1)) return 0; // display height
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_DISPLAY_OFFSET)) return 0; // display offset
    if(!_ssd1306_send_command(me, 0x00)) return 0;
    
    if(!_ssd1306_send_command(me, 0x40)) return 0; // set start column
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_CHARGE_PUMP)) return 0; // charge pump
    if(!_ssd1306_send_command(me, 0x14)) return 0; // enable
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_MEMORY_MODE)) return 0; // memory mode
    if(!_ssd1306_send_command(me, 0x00)) return 0; // horizontal mode
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_SEGMENT_REMAP)) return 0; // segment remap
    if(!_ssd1306_send_command(me, 0xA1)) return 0; // set to remap
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_COM_SCAN_DIRECTION)) return 0; // direction of com input
    if(!_ssd1306_send_command(me, 0xC8)) return 0; // scan in down direction
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_COM_PINS_CONFIG)) return 0; // hardware COM pin configuration
    if(!_ssd1306_send_command(me, 0x12)) return 0; // configuration pins
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_CONTRAST)) return 0; // set contrast
    if(!_ssd1306_send_command(me, 0xFF)) return 0; // contrast value
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_PRECHARGE_PERIOD)) return 0; // set pre-charge period
    if(!_ssd1306_send_command(me, 0xF1)) return 0; // pre-charge period
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_VCOMH_DESELECT_LEVEL)) return 0; // VCOMH deselect level
    if(!_ssd1306_send_command(me, 0x40)) return 0;
    
    if(!_ssd1306_send_command(me, 0xA4)) return 0; // view from RAM content
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_NORMAL_DISPLAY)) return 0; // normal view
    
    if(!_ssd1306_send_command(me, SSD1306_I2C_COMMAND_DISPLAY_ON)) return 0; // display on
    
    if(!ssd1306_update_display(me)) return 0;
    
    return 1;
}

uint8_t ssd1306_i2c_write_register (ssd1306_t *me, ssd1306_i2c_funcptr_t i2c_write_funcptr)
{
    if(!me || i2c_write_funcptr == NULL) return 0;
    me->i2c_write = i2c_write_funcptr;
    return 1;
}

void ssd1306_set_font(ssd1306_t *me, void *font_ptr, uint16_t width, uint16_t height)
{
    if(!me || !font_ptr) return;
    me->font.font_ptr = (void *) font_ptr;
    me->font.font_width = width;
    me->font.font_height = height;
}

void ssd1306_clear_display(ssd1306_t *me)
{
    if(!me) return;
    for(uint16_t i = 0; i < SSD1306_DISPLAY_PIXELS_COUNT_BYTES; ++i) me->buffer[i] = 0;
    me->position.x = 0;
    me->position.y = 0;
}

void ssd1306_write_pixel(ssd1306_t *me, uint16_t x, uint16_t y, bool value)
{
    if(!me) return;
    if (x < 0 || x >= SSD1306_DISPLAY_WIDTH || y < 0 || y >= SSD1306_DISPLAY_HEIGHT) return;
    
    me->position.x = x;
    me->position.y = y;
    
    uint16_t byte_index = (y / 8) * SSD1306_DISPLAY_WIDTH + x;
    uint16_t bit_index = y % 8;

    if (value) me->buffer[byte_index] |= (1 << bit_index);  // set pixel
    else me->buffer[byte_index] &= ~(1 << bit_index);       // clear pixel
}

void ssd1306_write_char(ssd1306_t *me, uint16_t x, uint16_t y, char c)
{
    if (!me || c < 32 || c > 126) return; // out of range

    const uint8_t word_size = (me->font.font_height + 7) / 8; // byte count for column

    const uint8_t *bitmap_char = (((uint8_t *) (me->font.font_ptr)) + (me->font.font_width * (c - 32) * word_size));

    for (uint8_t coll_index = 0; coll_index < me->font.font_width; ++coll_index) {      // columns
        for (uint8_t row_index = 0; row_index < me->font.font_height; ++row_index) {    // rows
            
            uint8_t row_byte_index = row_index >> 3; // index of byte in word
            
            uint8_t row_byte = bitmap_char[(coll_index * word_size) + row_byte_index]; // get byte
            
            uint8_t pixel_state = (row_byte >> (row_index & 0b111)) & 1; // get actual pixel status
            
            ssd1306_write_pixel(me, x + coll_index, y + row_index, pixel_state); // write pixel
        }
    }

    me->position.y = y; // actualize y
}

void ssd1306_write_string(ssd1306_t *me, const char *str)
{
    if(!me) return;
    
    while (*str){
        if(*str == '\r') me->position.x = 0;
        else if(*str == '\n') me->position.y += (me->font.font_height + 1);
        else if(*str == '\t'){
            uint8_t _width = (me->font.font_width + 1) * 2;
            if((me->position.x + _width) < SSD1306_DISPLAY_WIDTH) me->position.x = ((uint16_t) me->position.x / (_width)) * _width + _width;
        }
        else{
            ssd1306_write_char(me, me->position.x, me->position.y, *str);
            me->position.x += 2;
        }
        ++str;
    }
}

void ssd1306_write_string_position(ssd1306_t *me, uint16_t x, uint16_t y, const char *str)
{
    if(!me || !str) return;
    
    me->position.x = x;
    me->position.y = y;
    ssd1306_write_string(me, str);
}

uint8_t ssd1306_update_display_partial(ssd1306_t *me, uint16_t x0, uint16_t x1, uint16_t y0, uint16_t y1)
{
    if (!me || x0 >= SSD1306_DISPLAY_WIDTH || x1 >= SSD1306_DISPLAY_WIDTH || y0 >= SSD1306_DISPLAY_HEIGHT || y1 >= SSD1306_DISPLAY_HEIGHT || x0 > x1 || y0 > y1) return 0;
    
    uint8_t page0 = y0 / SSD1306_DISPLAY_PAGE_COUNT;
    uint8_t page1 = y1 / SSD1306_DISPLAY_PAGE_COUNT;
    
    for (uint8_t page = page0; page <= page1; ++page) {
        if(!_ssd1306_send_command(me, 0xB0 + page)) return 0;        // set page address
        if(!_ssd1306_send_command(me, (x0 & 0x0F))) return 0;        // set lower column address (x_start)
        if(!_ssd1306_send_command(me, 0x10 | (x0 >> 4))) return 0;   // set higher column address (x_start)

        for (uint16_t col = x0; col <= x1; ++col) if(!_ssd1306_send_data(me, me->buffer[page * SSD1306_DISPLAY_WIDTH + col])) return 0; // // send bytes for specified columns
    }
    
    return 1;
}

uint8_t ssd1306_update_display(ssd1306_t *me)
{
    if(!me) return 0;
    return ssd1306_update_display_partial(me, 0, SSD1306_DISPLAY_WIDTH - 1, 0, SSD1306_DISPLAY_HEIGHT - 1);
}

void ssd1306_draw_line(ssd1306_t *me, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{
    if(!me) return;
    
    int16_t dx = _ssd1306_abs(x1 - x0);     // difference in x
    int16_t sx = x0 < x1 ? 1 : -1;          // x direction
    
    int16_t dy = -_ssd1306_abs(y1 - y0);    // difference in y
    int16_t sy = y0 < y1 ? 1 : -1;          // x direction
    
    int16_t err = dx + dy;
    int16_t e2;

    while(1){
        ssd1306_write_pixel(me, x0, y0, true);  // write actual pixel
        
        if ((x0 == x1) && (y0 == y1)) break;  // check for end point of line
        
        e2 = 2 * err;

        if(e2 >= dy){
            err += dy;
            x0 += sx;
        }
        if(e2 <= dx){
            err += dx;
            y0 += sy;
        }
    }
}

void ssd1306_draw_square_empty(ssd1306_t *me, uint16_t x, uint16_t y, uint16_t size)
{
    if(!me) return;
    ssd1306_draw_line(me, x, y, x + size, y);
    ssd1306_draw_line(me, x, y + size, x + size, y + size);
    ssd1306_draw_line(me, x, y, x, y + size);
    ssd1306_draw_line(me, x + size, y, x + size, y + size);
}

void ssd1306_draw_square_fill(ssd1306_t *me, uint16_t x, uint16_t y, uint16_t size)
{
    if(!me) return;
    for (uint16_t i = 0; i <= size; ++i) ssd1306_draw_line(me, x, y + i, x + size, y + i);
}

void ssd1306_draw_circle_empty(ssd1306_t *me, uint16_t x0, uint16_t y0, uint16_t radius)
{
    if(!me) return;
    
    int16_t x = radius;
    int16_t y = 0;
    int16_t err = 1 - x;

    while (x >= y){
        ssd1306_write_pixel(me, x0 + x, y0 + y, true);
        ssd1306_write_pixel(me, x0 - x, y0 + y, true);
        ssd1306_write_pixel(me, x0 + x, y0 - y, true);
        ssd1306_write_pixel(me, x0 - x, y0 - y, true);
        ssd1306_write_pixel(me, x0 + y, y0 + x, true);
        ssd1306_write_pixel(me, x0 - y, y0 + x, true);
        ssd1306_write_pixel(me, x0 + y, y0 - x, true);
        ssd1306_write_pixel(me, x0 - y, y0 - x, true);

        ++y;

        if (err < 0){
            err += 2 * y + 1;
        }
        else{
            x--;
            err += 2 * (y - x) + 1;
        }
    }
}

void ssd1306_draw_circle_fill(ssd1306_t *me, uint16_t x0, uint16_t y0, uint16_t radius)
{
    if(!me) return;
    
    int16_t x = radius;
    int16_t y = 0;
    int16_t err = 1 - x;

    while (x >= y){
        for (int16_t i = x0 - x; i <= x0 + x; i++) {
            ssd1306_write_pixel(me, i, y0 + y, true);
            ssd1306_write_pixel(me, i, y0 - y, true);
        }
        
        for (int16_t i = x0 - y; i <= x0 + y; i++) {
            ssd1306_write_pixel(me, i, y0 + x, true);
            ssd1306_write_pixel(me, i, y0 - x, true);
        }

        ++y;

        if (err < 0){
            err += 2 * y + 1;
        }
        else{
            x--;
            err += 2 * (y - x) + 1;
        }
    }
}

void ssd1306_display_invert(ssd1306_t *me, bool invert)
{
    if(!me) return;
    if(invert == true) _ssd1306_send_command(me, SSD1306_I2C_COMMAND_INVERT_DISPLAY);
    else _ssd1306_send_command(me, SSD1306_I2C_COMMAND_NORMAL_DISPLAY);
}

void ssd1306_set_contrast(ssd1306_t *me, uint8_t contrast)
{
    if(!me) return;
    _ssd1306_send_command(me, SSD1306_I2C_COMMAND_CONTRAST);
    _ssd1306_send_command(me, contrast);
}
