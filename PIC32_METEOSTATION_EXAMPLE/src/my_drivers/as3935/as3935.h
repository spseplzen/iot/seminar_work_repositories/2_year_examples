/*
 * File:   as3935.h
 * Author: Miroslav Soukup
 * Description: Header file of as3935 thunder detector.
 * 
 */



#ifndef AS3935_H // Protection against multiple inclusion
#define AS3935_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define AS3935_REG_POWER                            UINT8_C(0x00)
#define AS3935_REG_NOISE                            UINT8_C(0x01)
#define AS3935_REG_STATISTICS                       UINT8_C(0x02)
#define AS3935_REG_ANTENA_AND_INT                   UINT8_C(0x03)
#define AS3935_REG_LIGHTNING_ENERGY_LSBYTE          UINT8_C(0x04)
#define AS3935_REG_LIGHTNING_ENERGY_MSBYTE          UINT8_C(0x05)
#define AS3935_REG_LIGHTNING_ENERGY_MMSBYTE         UINT8_C(0x06)
#define AS3935_REG_DISTANCE                         UINT8_C(0x07)
#define AS3935_REG_INT_AND_TUNING                   UINT8_C(0x08)
#define AS3935_REG_INT_TRCO_CALIB_STATUS            UINT8_C(0x3A)
#define AS3935_REG_INT_SRCO_CALIB_STATUS            UINT8_C(0x3B)



// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct as3935_descriptor as3935_t;
typedef struct as3935_data_descriptor as3935_data_t;
typedef struct as3935_config_descriptor as3935_config_t;

typedef uint8_t (*as3935_spi_rw_funcptr_t)(uint8_t *data, uint8_t size);
typedef void (*as3935_spi_cs_funcptr_t)(void);


typedef enum{
    AS3935_CONFIG_PWD_POWER_ON  = 0b0,
    AS3935_CONFIG_PWD_POWER_OFF = 0b1
} AS3935_CONFIG_PWD_e;


struct as3935_data_descriptor{
    uint8_t minimum_number_of_lightning;
    uint8_t number_of_spike_rejections;
    uint32_t lightning_energy;
    uint8_t distance;
    
    uint8_t trco_calib;
    uint8_t srco_calib;
};

struct as3935_config_descriptor{
    uint8_t hw_cs_enabled;
    
    // 0x00
    AS3935_CONFIG_PWD_e pwd;    // 0:0
    uint8_t afe_gb;             // 5:1
    
    // 0x01
    uint8_t wdth;               // 3:0
    uint8_t nf_lev;             // 6:4
    
    // 0x03
    uint8_t interr;             // 3:0
    uint8_t mask_dist;          // 5:5
    uint8_t lco_fdiv;           // 7:6
    
    // 0x08
    uint8_t tun_cap;            // 3:0
    uint8_t disp_trco;          // 5:5
    uint8_t disp_srco;          // 6:6
    uint8_t disp_lco;           // 7:7
    
};

struct as3935_descriptor{
    as3935_config_t config;
    
    as3935_data_t data;
    
    as3935_spi_rw_funcptr_t spi_write;
    as3935_spi_rw_funcptr_t spi_read;
    
    as3935_spi_cs_funcptr_t spi_cs_set;
    as3935_spi_cs_funcptr_t spi_cs_clear;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

/**
 * \brief Function for initialization n24c32 module.
 *
 * \param me pointer to as3935 module type of as3935_t
 * \param config pointer to as3935 configuration type of as3935_config_t
 *
 * \returns status of success / failure
 */
uint8_t as3935_init (as3935_t *me, as3935_config_t *config);

uint8_t as3935_spi_write_register (as3935_t *me, as3935_spi_rw_funcptr_t spi_write_funcptr);

uint8_t as3935_spi_read_register (as3935_t *me, as3935_spi_rw_funcptr_t spi_read_funcptr);

uint8_t as3935_spi_cs_set_register (as3935_t *me, as3935_spi_cs_funcptr_t spi_cs_set_funcptr);

uint8_t as3935_spi_cs_clear_register (as3935_t *me, as3935_spi_cs_funcptr_t spi_cs_clear_funcptr);

uint8_t as3935_get_data (as3935_t *me);

void as3935_get_default_config(as3935_config_t *config);



#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of AS3935_H
