/*
 * File:   n24c32.c
 * Author: Miroslav Soukup
 * Description: Source file of as3935 thunder detector.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "as3935.h"


// *****************************************************************************
// Section: Functions
// *****************************************************************************

static uint8_t _as3935_spi_read_reg(as3935_t *me, uint8_t reg, uint8_t *byte)
{
    if(!me) return 0;
    
    if(me->config.hw_cs_enabled)
    {
        if(me->spi_cs_clear) me->spi_cs_clear();
        else return 0;
    }
    
    reg = 0b01000000 | (reg & 0b00111111);
    if(!me->spi_write(&reg, 1)) return 0;
    if(!me->spi_read(byte, 1)) return 0;
    
    if(me->config.hw_cs_enabled)
    {
        if(me->spi_cs_set) me->spi_cs_set();
        else return 0;
    }
    
    return 1;
}

static uint8_t _as3935_spi_write_reg(as3935_t *me, uint8_t reg, uint8_t byte)
{
    if(!me) return 0;
    
    if(me->config.hw_cs_enabled)
    {
        if(me->spi_cs_clear) me->spi_cs_clear();
        else return 0;
    }
    
    uint8_t _data[2] = {reg & 0b00111111, byte};
    if(!me->spi_write(_data, 2)) return 0;
    
    if(me->config.hw_cs_enabled)
    {
        if(me->spi_cs_set) me->spi_cs_set();
        else return 0;
    }
    
    return 1;
}

uint8_t as3935_init (as3935_t *me, as3935_config_t *config)
{
    if(!me || !config) return 0;
    
    me->config = *config;
    
    if(me->config.hw_cs_enabled)
    {
        if(me->spi_cs_set) me->spi_cs_set();
        else return 0;
    }
    
    if(!_as3935_spi_write_reg(me, AS3935_REG_POWER, me->config.afe_gb << 1 | me->config.pwd)) return 0;
    
    uint8_t byte = 0x00;
    if(!_as3935_spi_read_reg(me, AS3935_REG_POWER, &byte)) return 0;
    if(byte != (me->config.afe_gb << 1 | me->config.pwd)) return 0;
    
    if(!_as3935_spi_write_reg(me, AS3935_REG_NOISE, me->config.nf_lev << 4 | me->config.wdth)) return 0;
    
    if(!_as3935_spi_write_reg(me, AS3935_REG_STATISTICS, 0b01000000)) return 0; // clear statistics
    
    if(!_as3935_spi_write_reg(me, AS3935_REG_ANTENA_AND_INT, me->config.lco_fdiv << 6 | me->config.mask_dist << 5 | me->config.interr)) return 0;
    
    if(!_as3935_spi_write_reg(me, AS3935_REG_INT_AND_TUNING, me->config.disp_lco << 7 | me->config.disp_srco << 6 | me->config.disp_trco << 5 | me->config.tun_cap)) return 0;
    
    return 1;
}

uint8_t as3935_spi_write_register (as3935_t *me, as3935_spi_rw_funcptr_t spi_write_funcptr)
{
    if(!me || !spi_write_funcptr) return 0;
    me->spi_write = spi_write_funcptr;
    return 1;
}

uint8_t as3935_spi_read_register (as3935_t *me, as3935_spi_rw_funcptr_t spi_read_funcptr)
{
    if(!me || !spi_read_funcptr) return 0;
    me->spi_read = spi_read_funcptr;
    return 1;
}

uint8_t as3935_spi_cs_set_register (as3935_t *me, as3935_spi_cs_funcptr_t spi_cs_set_funcptr)
{
    if(!me || !spi_cs_set_funcptr) return 0;
    me->spi_cs_set = spi_cs_set_funcptr;
    return 1;
}

uint8_t as3935_spi_cs_clear_register (as3935_t *me, as3935_spi_cs_funcptr_t spi_cs_clear_funcptr)
{
    if(!me || !spi_cs_clear_funcptr) return 0;
    me->spi_cs_clear = spi_cs_clear_funcptr;
    return 1;
}

uint8_t as3935_get_data (as3935_t *me)
{
    if(!me) return 0;
    
    uint8_t _data[3];
    
    _as3935_spi_read_reg(me, AS3935_REG_STATISTICS, &_data[0]);
    me->data.minimum_number_of_lightning = (_data[0] >> 4) & 0b11;
    me->data.number_of_spike_rejections  = _data[0] & 0b1111;
    
    _as3935_spi_read_reg(me, AS3935_REG_LIGHTNING_ENERGY_LSBYTE, &_data[0]);
    _as3935_spi_read_reg(me, AS3935_REG_LIGHTNING_ENERGY_MSBYTE, &_data[1]);
    _as3935_spi_read_reg(me, AS3935_REG_LIGHTNING_ENERGY_MMSBYTE, &_data[2]);
    me->data.lightning_energy = ((uint32_t) _data[2]) << 16 | ((uint32_t) _data[1]) << 8 | _data[0];
    
    _as3935_spi_read_reg(me, AS3935_REG_DISTANCE, &_data[0]);
    me->data.distance = _data[0];
    
    return 1;
}

void as3935_get_default_config(as3935_config_t *config)
{
    if(!config) return;
    
    config->hw_cs_enabled = 1;

    config->pwd    = AS3935_CONFIG_PWD_POWER_ON;
    config->afe_gb = 0b10010;

    config->wdth   = 0b0010;
    config->nf_lev = 0b010;

    config->lco_fdiv  = 0b00;
    config->mask_dist = 0b0;
    config->interr    = 0b0000;

    config->disp_lco  = 0b0;
    config->disp_srco = 0b0;
    config->disp_trco = 0b0;
    config->tun_cap   = 0b0000;
}
