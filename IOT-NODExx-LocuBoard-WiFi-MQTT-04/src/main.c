#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"
#include <string.h>
#include <stdio.h>

#include "my_drivers/esp32c6_driver_uart/esp32c6_driver_uart.h"
#include "my_drivers/ring_buffer/ring_buffer.h"

#include "my_drivers/mcp9844/mcp9844.h"


#define NODE_WIFI_SSID "ssid"          // zmen jmeno WiFi
#define NODE_WIFI_PASSWORD "password"  // zmen heslo k WiFi

#define NODE_HOSTNAME "NODExx"              // zmen jmeno nodu
#define NODE_MQTT_SERVER_IP "10.10.10.10"   // zmen IP adresu MQTT serveru
#define NODE_MQTT_SERVER_PORT "1883"        // PORT MQTT serveru
#define NODE_MQTT_CLIENT_USERNAME ""        // zmen uzivatelske jmeno
#define NODE_MQTT_CLIENT_PASSWORD ""        // zmen heslo k MQTT brokeru


#define NODE_MQTT_BTN_TOPIC_BEGINNING   "home/" NODE_HOSTNAME "/btn"
#define NODE_MQTT_TEMPERATURE_TOPIC     "home/" NODE_HOSTNAME "/measure/temperature01"


volatile uint8_t module_uart_receive_char;
void module_uart_rx_callback(uintptr_t context){
    ring_buffer_write((ring_buffer_t *) context, module_uart_receive_char);
    SERCOM2_USART_Read((void *) &module_uart_receive_char, 1);
}

bool module_uart_tx_write(char *buffer, size_t size){
    return SERCOM2_USART_Write((void *) buffer, size);
}

bool module_uart_tx_is_busy(void){
    return SERCOM2_USART_WriteIsBusy();
}

bool rb_read_char(uintptr_t rbme, char *c)
{
    return ring_buffer_read((ring_buffer_t *) rbme, (uint8_t *) c);
}

bool rb_is_empty(uintptr_t rbme)
{
    return ring_buffer_is_empty((ring_buffer_t *) rbme);
}



void leds_init(void);
void led1_led2_set_lighting_level(uint8_t l1, uint8_t l2);
void led3_set_power_level(bool s);
void led4_set_power_level(bool s);
void led5_rgb_set_color(uint8_t r, uint8_t g, uint8_t b);


void led12_callback(esp32c6_t *me){
    static uint8_t led1_value = 0;
    static uint8_t led2_value = 0;
    
    uint8_t value = (me->data.buffer[0] - '0') * 100 + (me->data.buffer[1] - '0') * 10 + (me->data.buffer[2] - '0');
    
    if(strcmp((char *) me->data.mqtt_topic, (char *) me->config.mqtt_topics[0].topic) == 0) led1_value = value;
    else led2_value = value;
    
    printf("-> LED1 & LED2 = (%1u, %1u)\r\n", led1_value, led2_value);
    
    led1_led2_set_lighting_level(led1_value, led2_value);
}

void led3_callback(esp32c6_t *me){
    printf("-> LED3 = %1u\r\n", me->data.buffer[0] - '0');
    led3_set_power_level(me->data.buffer[0] - '0');
}

void led4_callback(esp32c6_t *me){
    printf("-> LED4 = %1u\r\n", me->data.buffer[0] - '0');
    led4_set_power_level(me->data.buffer[0] - '0');
}

void led5_callback(esp32c6_t *me){
    static uint8_t r = 0;
    static uint8_t g = 0;
    static uint8_t b = 0;

    r = (me->data.buffer[0] - '0') * 100 + (me->data.buffer[1] - '0') * 10 + (me->data.buffer[2] - '0');
    g = (me->data.buffer[4] - '0') * 100 + (me->data.buffer[5] - '0') * 10 + (me->data.buffer[6] - '0');
    b = (me->data.buffer[8] - '0') * 100 + (me->data.buffer[9] - '0') * 10 + (me->data.buffer[10] - '0');

    printf("-> LED5 RGB = (%3u, %3u, %3u)\r\n", r, g, b);

    led5_rgb_set_color(r, g, b);
}

esp32c6_mqtt_config_topic_t mqtt_topics[] =     {
                                                    {"home/" NODE_HOSTNAME "/control/led1/level",     led12_callback},    // led1
                                                    {"home/" NODE_HOSTNAME "/control/led2/level",     led12_callback},    // led2
                                                    {"home/" NODE_HOSTNAME "/control/led3/switch",    led3_callback},     // led3
                                                    {"home/" NODE_HOSTNAME "/control/led4/switch",    led4_callback},     // led4
                                                    {"home/" NODE_HOSTNAME "/control/led5/color",     led5_callback}      // led5
                                                };


uint8_t ESP32C6_MQTT_BTN_Task(uint8_t n, uint8_t *eic_flag);
uint8_t ESP32C6_MQTT_MCP9844_Task(mcp9844_t *me);


volatile uint8_t timer0_flag = 0;
volatile uint8_t timer0_mcp9844_flag = 0;
void timer0_callback(TC_TIMER_STATUS status, uintptr_t context){
    timer0_flag = 1;
    timer0_mcp9844_flag = 1;
}


void btn_callback(uintptr_t context){
    *((uint8_t *) context) = 1;
}

uint8_t i2c_read(uint16_t client_address, uint8_t *data, uint8_t size){
    bool _state = SERCOM5_I2C_Read(client_address, data, size);
    while(SERCOM5_I2C_IsBusy());
    return (_state) ? 1 : 0;
}

uint8_t i2c_write(uint16_t client_address, uint8_t *data, uint8_t size){
    bool _state = SERCOM5_I2C_Write(client_address, data, size);
    while(SERCOM5_I2C_IsBusy());
    return (_state) ? 1 : 0;
}

void nextion_display_init(void){
    char _buff[64];
    size_t len;
    
    strcpy(_buff, "info.ssid.txt=\"" NODE_WIFI_SSID "\"");
    len = strlen(_buff);
    _buff[len++] = 0xFF;
    _buff[len++] = 0xFF;
    _buff[len++] = 0xFF;
    SERCOM1_USART_Write((void *) _buff, len);
    while(SERCOM1_USART_WriteIsBusy());
    
    strcpy(_buff, "info.pswd.txt=\"" NODE_WIFI_PASSWORD "\"");
    len = strlen(_buff);
    _buff[len++] = 0xFF;
    _buff[len++] = 0xFF;
    _buff[len++] = 0xFF;
    SERCOM1_USART_Write((void *) _buff, len);
    while(SERCOM1_USART_WriteIsBusy());
    
    strcpy(_buff, "info.mqtt.txt=\"" NODE_MQTT_SERVER_IP "\"");
    len = strlen(_buff);
    _buff[len++] = 0xFF;
    _buff[len++] = 0xFF;
    _buff[len++] = 0xFF;
    SERCOM1_USART_Write((void *) _buff, len);
    while(SERCOM1_USART_WriteIsBusy());
}


int main(void)
{
    SYS_Initialize(NULL);
    
    
    // Turn on 5V power (display)
    PORT_PinOutputEnable(PORT_PIN_PB02);
    PORT_PinSet(PORT_PIN_PB02);
    
    
    leds_init(); // init leds
    
    
    
    ring_buffer_t rb;
    uint8_t rb_buffer[256];
    ring_buffer_init(&rb, rb_buffer, 256);
    
    SERCOM2_USART_ReadCallbackRegister(module_uart_rx_callback, (uintptr_t) &rb);
    SERCOM2_USART_Read((void *) &module_uart_receive_char, 1);
    
    
    
    esp32c6_t esp32c6 = {};
    
    esp32c6_rb_read_char_register(&esp32c6, rb_read_char, (uintptr_t) &rb); // ring buffer read
    esp32c6_rb_is_empty_register(&esp32c6, rb_is_empty, (uintptr_t) &rb);   // ring buffer is empty
    
    esp32c6_uart_write_register(&esp32c6, module_uart_tx_write);            // uart write
    esp32c6_uart_write_is_busy_register(&esp32c6, module_uart_tx_is_busy);  // uart is busy
    
    esp32c6_configure_init(&esp32c6, NODE_HOSTNAME, ESP32C6_DEBUG_LEVEL_BASIC);  // module config init
    
    esp32c6_configure_wifi_secure(&esp32c6, NODE_WIFI_SSID, NODE_WIFI_PASSWORD); // wifi config

    esp32c6_configure_mqtt_secure(&esp32c6, NODE_MQTT_SERVER_IP, NODE_MQTT_SERVER_PORT, NODE_MQTT_CLIENT_USERNAME, NODE_MQTT_CLIENT_PASSWORD); // mqtt config
    
    esp32c6_configure_mqtt_reconnecting(&esp32c6, true); // mqtt init reconnecting enabled
    
    esp32c6_configure_mqtt_register_subscribe_topics(&esp32c6, mqtt_topics, sizeof(mqtt_topics)/sizeof(*mqtt_topics)); // mqtt subscribe topics config
    
    
    
    mcp9844_t mcp9844;
    
    mcp9844_config_t config;
    config.SHDN = MCP9844_CONFIG_SHDN_CONTINUOS;
    config.Thyst = MCP9844_CONFIG_THYST_0C;
    config.resolution = MCP9844_CONFIG_RESOLUTION_0_25C;
    
    mcp9844_i2c_read_register(&mcp9844, i2c_read);
    mcp9844_i2c_write_register(&mcp9844, i2c_write);
    
    mcp9844_init(&mcp9844, 0x18, &config);
    
    mcp9844_get_temperature(&mcp9844);
    
    
    volatile uint8_t btn1_flag = 0;
    volatile uint8_t btn2_flag = 0;
    volatile uint8_t btn3_flag = 0;
    EIC_CallbackRegister(EIC_PIN_6, btn_callback, (uintptr_t) &btn1_flag);
    EIC_CallbackRegister(EIC_PIN_7, btn_callback, (uintptr_t) &btn2_flag);
    EIC_CallbackRegister(EIC_PIN_15, btn_callback, (uintptr_t) &btn3_flag);
    
    
    TC0_TimerCallbackRegister(timer0_callback, (uintptr_t) NULL);
    TC0_TimerStart();
    
    
    printf("LocuBoard" " " NODE_HOSTNAME " WiFi MQTT example (04) initialization...");
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(3000);
    SYSTICK_TimerStop();
    printf("done.\r\n");
    
    
    nextion_display_init(); // init nextion display
    
    
    while(true)
    {
        SYS_Tasks();
        
        
        ESP32C6_PARSER_Task(&esp32c6); // read and parse received messages

        ESP32C6_Task(&esp32c6); // module initialization

        ESP32C6_WIFI_Task(&esp32c6); // initialize WiFi connection

        ESP32C6_MQTT_Task(&esp32c6); // initialize MQTT

        ESP32C6_MQTT_SUBSCRIBED_TOPICS_EXECUTE_CALLBACK_Task(&esp32c6); // read mqtt topic and call callback function
        
        
        if(esp32c6_get_init_mqtt_state(&esp32c6) == ESP32C6_INIT_MQTT_STATE_DONE)
        {
            if(timer0_flag && !module_uart_tx_is_busy() && !SERCOM1_USART_WriteIsBusy()) // casove omezeni + omezeni odesilani (uart)
            {
                uint8_t clear_timer = 0;
                
                clear_timer |= ESP32C6_MQTT_MCP9844_Task(&mcp9844);

                if(!clear_timer) clear_timer |= ESP32C6_MQTT_BTN_Task(1, (uint8_t *) &btn1_flag);
                if(!clear_timer) clear_timer |= ESP32C6_MQTT_BTN_Task(2, (uint8_t *) &btn2_flag);
                if(!clear_timer) clear_timer |= ESP32C6_MQTT_BTN_Task(3, (uint8_t *) &btn3_flag);

                if(clear_timer) timer0_flag = 0;
            }
        }
        
    }

    return EXIT_FAILURE;
}


uint8_t ESP32C6_MQTT_BTN_Task(uint8_t n, uint8_t *eic_flag){
    uint8_t _state = 0;
    
    if(*eic_flag)
    {
        *eic_flag = 0;
        
        static uint8_t btn_state[3] = {0, 0, 0};
        
        printf("-> BTN%1u = %1u\r\n", n, btn_state[n-1]);
        
        char _topic[64];
        sprintf(_topic, NODE_MQTT_BTN_TOPIC_BEGINNING "%1u/state", n);
        
        char _buff[128];
        uint16_t len = esp32c6_get_mqtt_create_message_uint(_buff, _topic, btn_state[n-1]);
        module_uart_tx_write(_buff, len);
        
        _state = 1;
    }
    
    return _state;
}

uint8_t ESP32C6_MQTT_MCP9844_Task(mcp9844_t *me){
    uint8_t _state = 0;
    
    
    static uint8_t _smc = 0;
    static uint8_t _sms = 0;
    switch(_sms){
        case 0:
            if(timer0_mcp9844_flag){
                timer0_mcp9844_flag = 0;
                _sms = 1;
            }
        break;

        case 1:
            if(_smc >= 10) _sms = 2;
            else{
                ++_smc;
                _sms = 0;
            }
        break;

        case 2:
            mcp9844_get_temperature(me);
            
            static int16_t value_int_last = 0;
            static int16_t value_dec_last = 0;
            int16_t value_int = me->data.temp.integer;
            int16_t value_dec = me->data.temp.decimal;

            if(value_int != value_int_last || value_dec != value_dec_last){
                value_int_last = value_int;
                value_dec_last = value_dec;
                
                
                printf("-> MCP9844 = %6.3f\r\n", value_int + (value_dec / 16.0));

                float temperature = value_int + (value_dec / 16.0);
                
                char _buff[128];
                uint16_t len1 = esp32c6_get_mqtt_create_message_float(_buff, NODE_MQTT_TEMPERATURE_TOPIC, temperature, 3);
                module_uart_tx_write(_buff, len1);
                

                int len = sprintf(_buff, "main.val0.val=%d", value_int);
                _buff[len++] = 0xFF;
                _buff[len++] = 0xFF;
                _buff[len++] = 0xFF;
                SERCOM1_USART_Write((void *) _buff, (size_t) len);
                
                _state = 1;
            }
            
            _smc = 0;
            _sms = 0;
        break;
    }
    
    return _state;
}

void leds_init(void){
    TC4_CompareInitialize();
    TC4_Compare8bitMatch0Set(0);
    TC4_Compare8bitMatch1Set(0);
    TC4_CompareStart();
    
    TCC0_PWM24bitDutySet(TCC0_CHANNEL0, 0);
    TCC0_PWM24bitDutySet(TCC0_CHANNEL1, 0);
    TCC0_PWMStart();
    
    TC3_CompareInitialize();
    TC3_Compare8bitMatch0Set(0);
    TC3_CompareStart();
    
    LED3_Clear();
    LED4_Clear();
}

void led1_led2_set_lighting_level(uint8_t l1, uint8_t l2){
    TCC0_PWMInitialize();
    TCC0_PWM24bitDutySet(TCC0_CHANNEL0, l1); // LED1
    TCC0_PWM24bitDutySet(TCC0_CHANNEL1, l2); // LED2
    TCC0_PWMStart();
}

void led3_set_power_level(bool s){
    (s) ? LED3_Set() : LED3_Clear(); // LED3
}

void led4_set_power_level(bool s){
    (s) ? LED4_Set() : LED4_Clear(); // LED4
}

void led5_rgb_set_color(uint8_t r, uint8_t g, uint8_t b){
    TC4_CompareInitialize();
    TC4_Compare8bitMatch0Set(b); // blue
    TC4_CompareStart();
    TC4_Compare8bitMatch1Set(g); // green
    TC3_CompareInitialize();
    TC3_Compare8bitMatch0Set(r); // red
    TC3_CompareStart();
}
