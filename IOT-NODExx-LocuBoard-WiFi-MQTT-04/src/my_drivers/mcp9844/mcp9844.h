/*
 * File:   mcp9844.h
 * Author: Miroslav Soukup
 * Description: Header file of mcp9844 sensor driver.
 * 
 */

#ifndef MCP9844_H // Protection against multiple inclusion
#define MCP9844_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define MCP9844_REG_CAPABILITY          UINT8_C(0x00)
#define MCP9844_REG_CONFIGURATION       UINT8_C(0x01)
#define MCP9844_REG_UPPER_TEMPERATURE   UINT8_C(0x02)
#define MCP9844_REG_LOWER_TEMPERATURE   UINT8_C(0x03)
#define MCP9844_REG_CRITICALTEMPERATURE UINT8_C(0x04)
#define MCP9844_REG_AMBIENT_TEMPERATURE UINT8_C(0x05)
#define MCP9844_REG_MANUFACTURER_ID     UINT8_C(0x06)
#define MCP9844_REG_DEVICE_ID           UINT8_C(0x07)
#define MCP9844_REG_DEVICE_REVISION     UINT8_C(0x08)
#define MCP9844_REG_RESOLUTION          UINT8_C(0x09)



// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct mcp9844_descriptor mcp9844_t;
typedef struct mcp9844_data_descriptor mcp9844_data_t;
typedef struct mcp9844_config_descriptor mcp9844_config_t;
typedef struct mcp9844_temp_limit_descriptor mcp9844_temp_limit_t;
typedef struct mcp9844_data_temperature_descriptor mcp9844_data_temperature_t;
typedef struct mcp9844_data_device_descriptor mcp9844_data_device_t;
typedef struct mcp9844_data_capability_descriptor mcp9844_data_capability_t;

typedef uint8_t (*mcp9844_i2c_funcptr_t)(uint16_t client_address, uint8_t *data, uint8_t size);

typedef enum{
    MCP9844_CONFIG_THYST_0C   = 0b00, // power-up default
    MCP9844_CONFIG_THYST_1_5C = 0b01,
    MCP9844_CONFIG_THYST_3C   = 0b10,
    MCP9844_CONFIG_THYST_6C   = 0b11
} MCP9844_CONFIG_THYST_e;

typedef enum{
    MCP9844_CONFIG_SHDN_CONTINUOS = 0b0, // power-up default
    MCP9844_CONFIG_SHDN_SHUTDOWN  = 0b1
} MCP9844_CONFIG_SHDN_e;

typedef enum{
    MCP9844_CONFIG_LOCK_UNLOCKED = 0b0, // power-up default
    MCP9844_CONFIG_LOCK_LOCKED   = 0b1
} MCP9844_CONFIG_LOCK_e;

typedef enum{
    MCP9844_CONFIG_INTC_NO_EFFECT = 0b0, // power-up default
    MCP9844_CONFIG_INTC_CLEAR     = 0b1
} MCP9844_CONFIG_INTC_e;

typedef enum{
    MCP9844_CONFIG_EVENT_STATUS_NOT_ASSERTED = 0b0, // power-up default
    MCP9844_CONFIG_EVENT_STATUS_IS_ASSERTED  = 0b1
} MCP9844_CONFIG_EVENT_STATUS_e;

typedef enum{
    MCP9844_CONFIG_EVENT_CONTROL_OUTPUT_DISABLED = 0b0, // power-up default
    MCP9844_CONFIG_EVENT_CONTROL_OUTPUT_ENABLED  = 0b1
} MCP9844_CONFIG_EVENT_CONTROL_e;

typedef enum{
    MCP9844_CONFIG_EVENT_SELECT_TUPP_TLOW_TCRIT = 0b0, // power-up default
    MCP9844_CONFIG_EVENT_SELECT_TCRIT           = 0b1
} MCP9844_CONFIG_EVENT_SELECT_e;

typedef enum{
    MCP9844_CONFIG_EVENT_POLARITY_ACTIVE_LOW  = 0b0, // power-up default (pull-up required)
    MCP9844_CONFIG_EVENT_POLARITY_ACTIVE_HIGH = 0b1
} MCP9844_CONFIG_EVENT_POLARITY_e;

typedef enum{
    MCP9844_CONFIG_EVENT_MODE_COMPARATOR = 0b0, // power-up default
    MCP9844_CONFIG_EVENT_MODE_INTERRUPT  = 0b1
} MCP9844_CONFIG_EVENT_MODE_e;

typedef enum{
    MCP9844_CONFIG_RESOLUTION_0_5C    = 0b00, // (30 ms)
    MCP9844_CONFIG_RESOLUTION_0_25C   = 0b01, // power-up default (65 ms)
    MCP9844_CONFIG_RESOLUTION_0_125C  = 0b10, // (130 ms)
    MCP9844_CONFIG_RESOLUTION_0_0625C = 0b11  // (260 ms)
} MCP9844_CONFIG_RESOLUTION_e;

struct mcp9844_temp_limit_descriptor{
    uint16_t sign    : 1;
    uint16_t integer : 8;
    uint16_t decimal : 2;
};

struct mcp9844_config_descriptor{
    MCP9844_CONFIG_THYST_e Thyst;
    MCP9844_CONFIG_SHDN_e SHDN;
    MCP9844_CONFIG_LOCK_e crit_lock;
    MCP9844_CONFIG_LOCK_e win_lock;
    MCP9844_CONFIG_INTC_e int_clear;
    MCP9844_CONFIG_EVENT_STATUS_e event_stat;
    MCP9844_CONFIG_EVENT_CONTROL_e event_cnt;
    MCP9844_CONFIG_EVENT_SELECT_e event_sel;
    MCP9844_CONFIG_EVENT_POLARITY_e event_pol;
    MCP9844_CONFIG_EVENT_MODE_e event_mod;
    
    MCP9844_CONFIG_RESOLUTION_e resolution;
    
    mcp9844_temp_limit_t temp_upper;
    mcp9844_temp_limit_t temp_lower;
    mcp9844_temp_limit_t temp_critical;
};

struct mcp9844_data_temperature_descriptor{
    int16_t integer;
    uint8_t decimal;
};

struct mcp9844_data_device_descriptor{
    uint16_t man_id;
    uint8_t dev_id;
    uint8_t dev_rev;
};

struct mcp9844_data_capability_descriptor{
    uint8_t shdn_stat       : 1;
    uint8_t tout_range      : 1;
    uint8_t unused          : 1;
    uint8_t resolution      : 2;
    uint8_t meas_resolution : 1;
    uint8_t accuracy        : 1;
    uint8_t temp_alarm      : 1;
};

struct mcp9844_data_descriptor{
    mcp9844_data_temperature_t temp;
    mcp9844_data_device_t device;
    mcp9844_data_capability_t capability;
};

struct mcp9844_descriptor{
    uint16_t client_address;
    mcp9844_config_t config;
    mcp9844_i2c_funcptr_t i2c_write;
    mcp9844_i2c_funcptr_t i2c_read;
    mcp9844_data_t data;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************


/**
 * \brief Function for initialization mcp9844 sensor.
 *
 * \param me pointer to mcp9844 sensor type of mcp9844_t
 * \param config pointer to mpc9844 configuration type of mcp9844_config_t
 *
 * \returns status of success / failure
 */
uint8_t mcp9844_init (mcp9844_t *me, uint16_t client_address, mcp9844_config_t *config);


uint8_t mcp9844_i2c_write_register (mcp9844_t *me, mcp9844_i2c_funcptr_t i2c_write_funcptr);


uint8_t mcp9844_i2c_read_register (mcp9844_t *me, mcp9844_i2c_funcptr_t i2c_read_funcptr);


uint8_t mcp9844_get_temperature (mcp9844_t *me);


uint8_t mcp9844_i2c_check (mcp9844_t *me);

uint8_t mcp9844_get_manufacturer_info (mcp9844_t *me);

uint8_t mcp9844_get_device_info (mcp9844_t *me);

uint8_t mcp9844_get_capability (mcp9844_t *me);
        

#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of MCP9844_H
