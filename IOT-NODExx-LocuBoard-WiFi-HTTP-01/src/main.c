#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"
#include <string.h>
#include <stdio.h>

#include "my_drivers/esp32c6_driver_uart/esp32c6_driver_uart.h"
#include "my_drivers/ring_buffer/ring_buffer.h"


#define NODE_WIFI_SSID      "ssid"      // zmen jmeno WiFi
#define NODE_WIFI_PASSWORD  "password"  // zmen heslo k WiFi

#define NODE_HOSTNAME       "NODExx"    // zmen jmeno nodu
#define NODE_SERVER_URL     "url"       // zmen URL adresu HTTP serveru




uint8_t BTNx_Task(uint8_t n, uint8_t *eic_flag);


volatile uint8_t module_uart_receive_char;
void module_uart_rx_callback(uintptr_t context){
    ring_buffer_write((ring_buffer_t *) context, module_uart_receive_char);
    SERCOM2_USART_Read((void *) &module_uart_receive_char, 1);
}

bool module_uart_tx_write(char *buffer, size_t size){
    return SERCOM2_USART_Write((void *) buffer, size);
}

bool module_uart_tx_is_busy(void){
    return SERCOM2_USART_WriteIsBusy();
}

bool rb_read_char(uintptr_t rbme, char *c)
{
    return ring_buffer_read((ring_buffer_t *) rbme, (uint8_t *) c);
}

bool rb_is_empty(uintptr_t rbme)
{
    return ring_buffer_is_empty((ring_buffer_t *) rbme);
}


volatile uint8_t timer0_flag = 0;
void timer0_callback(TC_TIMER_STATUS status, uintptr_t context){
    timer0_flag = 1;
}


void btn_callback(uintptr_t context){
    *((uint8_t *) context) = 1;
}


int main(void)
{
    SYS_Initialize(NULL);

    
    ring_buffer_t rb;
    uint8_t rb_buffer[256];
    ring_buffer_init(&rb, rb_buffer, 256);
    
    
    SERCOM2_USART_ReadCallbackRegister(module_uart_rx_callback, (uintptr_t) &rb);
    SERCOM2_USART_Read((void *) &module_uart_receive_char, 1);

    
    esp32c6_t esp32c6 = {};
    
    esp32c6_rb_read_char_register(&esp32c6, rb_read_char, (uintptr_t) &rb); // ring buffer read
    esp32c6_rb_is_empty_register(&esp32c6, rb_is_empty, (uintptr_t) &rb);   // ring buffer is empty
    
    esp32c6_uart_write_register(&esp32c6, module_uart_tx_write);            // uart write
    esp32c6_uart_write_is_busy_register(&esp32c6, module_uart_tx_is_busy);  // uart is busy
    
    esp32c6_configure_init(&esp32c6, NODE_HOSTNAME, ESP32C6_DEBUG_LEVEL_BASIC);  // module config init
    
    esp32c6_configure_wifi_secure(&esp32c6, NODE_WIFI_SSID, NODE_WIFI_PASSWORD); // wifi config
    
    
    volatile uint8_t btn1_flag = 0;
    volatile uint8_t btn2_flag = 0;
    volatile uint8_t btn3_flag = 0;
    EIC_CallbackRegister(EIC_PIN_6, btn_callback, (uintptr_t) &btn1_flag);
    EIC_CallbackRegister(EIC_PIN_7, btn_callback, (uintptr_t) &btn2_flag);
    EIC_CallbackRegister(EIC_PIN_15, btn_callback, (uintptr_t) &btn3_flag);
    
    
    TC0_TimerCallbackRegister(timer0_callback, (uintptr_t) NULL);
    TC0_TimerStart();

    
    printf("ESP32C6-MINI " NODE_HOSTNAME " initialization...");
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(3000);
    SYSTICK_TimerStop();
    printf("done.\r\n");
    
    
    while(true)
    {
        SYS_Tasks();
        
        ESP32C6_PARSER_Task(&esp32c6); // read and parse received messages

        ESP32C6_Task(&esp32c6); // module initialization

        ESP32C6_WIFI_Task(&esp32c6); // initialize WiFi connection
        
        
        // pokud je vse dokonceno, muzeme zacit s nasim aplikacnim programem
        if(esp32c6_get_init_wifi_state(&esp32c6) == ESP32C6_INIT_WIFI_STATE_DONE){
            
            // casove omezeni, proti prilis castemu mackani tlacitek
            if(timer0_flag){
                uint8_t clear_timer = 0;

                if(!clear_timer) clear_timer |= BTNx_Task(1, (uint8_t *) &btn1_flag);
                if(!clear_timer) clear_timer |= BTNx_Task(2, (uint8_t *) &btn2_flag);
                if(!clear_timer) clear_timer |= BTNx_Task(3, (uint8_t *) &btn3_flag);
                
                if(clear_timer){
                    timer0_flag = 0;
                }
            }
            
        }
        
        
    }

    return EXIT_FAILURE;
}


uint8_t BTNx_Task(uint8_t n, uint8_t *eic_flag){
    uint8_t _state = 0;
    
    if(*eic_flag){
        *eic_flag = 0;
        
        static uint8_t btn_state[3] = {1, 1, 1};
        
        char _buffer[128];
        sprintf(_buffer, ESP32C6_MINI_COMMAND_HTTP_PUT "=\"" NODE_SERVER_URL "\",6\r\n");
        SERCOM2_USART_Write((void *) _buffer, strlen(_buffer));
        while(SERCOM2_USART_WriteIsBusy());
        
        printf("--> btn%1u: %s\r\n", n, _buffer);
        
        SYSTICK_TimerStart();
        SYSTICK_DelayMs(500);
        SYSTICK_TimerStop();
        
        sprintf(_buffer, "led%1u=%1u",n, btn_state[n-1]);
        btn_state[n-1] = !btn_state[n-1];
        SERCOM2_USART_Write((void *) _buffer, strlen(_buffer));
        while(SERCOM2_USART_WriteIsBusy());
        
        printf("-- > btn%1u: %s\r\n", n, _buffer);
        
        _state = 1;
    }
    
    return _state;
}
