/*
 * File:   mcp9844.h
 * Author: Miroslav Soukup
 * Description: Header file of mcp9844 sensor driver.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "mcp9844.h"



// *****************************************************************************
// Section: Functions
// *****************************************************************************

uint8_t mcp9844_init (mcp9844_t *me, uint16_t client_address, mcp9844_config_t *config) {
    
    me->config = *config;
    
    me->client_address = client_address;
    
    uint8_t data[3];
    
    data[0] = MCP9844_REG_CONFIGURATION;
    data[1] = (me->config.Thyst << 1) | MCP9844_CONFIG_SHDN_SHUTDOWN;
    data[2] = (MCP9844_CONFIG_LOCK_UNLOCKED << 7) | (MCP9844_CONFIG_LOCK_UNLOCKED << 6) | (me->config.int_clear << 5) | (me->config.event_stat << 4) | (me->config.event_cnt << 3) | (me->config.event_sel << 2) | (me->config.event_pol << 1) | (me->config.event_mod);
    me->i2c_write(me->client_address, data, 3);
    
    
    data[0] = MCP9844_REG_UPPER_TEMPERATURE;
    data[1] = (me->config.temp_upper.sign << 4)     | (me->config.temp_upper.integer >> 4);
    data[2] = (me->config.temp_upper.integer << 4)  | (me->config.temp_upper.decimal << 2);
    me->i2c_write(me->client_address, data, 3);
    
    
    data[0] = MCP9844_REG_LOWER_TEMPERATURE;
    data[1] = (me->config.temp_lower.sign << 4)     | (me->config.temp_lower.integer >> 4);
    data[2] = (me->config.temp_lower.integer << 4)  | (me->config.temp_lower.decimal << 2);
    me->i2c_write(me->client_address, data, 3);
    
    
    data[0] = MCP9844_REG_CRITICALTEMPERATURE;
    data[1] = (me->config.temp_critical.sign << 4)     | (me->config.temp_critical.integer >> 4);
    data[2] = (me->config.temp_critical.integer << 4)  | (me->config.temp_critical.decimal << 2);
    me->i2c_write(me->client_address, data, 3);
    
    
    data[0] = MCP9844_REG_RESOLUTION;
    data[1] = 0b00000000;
    data[2] = me->config.resolution & 0b11;
    me->i2c_write(me->client_address, data, 3);
    
    
    data[0] = MCP9844_REG_CONFIGURATION;
    data[1] = (me->config.Thyst << 1) | (me->config.SHDN);
    data[2] = (me->config.crit_lock << 7) | (me->config.win_lock << 6) | (me->config.int_clear << 5) | (me->config.event_stat << 4) | (me->config.event_cnt << 3) | (me->config.event_sel << 2) | (me->config.event_pol << 1) | (me->config.event_mod);
    me->i2c_write(me->client_address, data, 3);
    
    return 1;
}

uint8_t mcp9844_i2c_write_register (mcp9844_t *me, mcp9844_i2c_funcptr_t i2c_write_funcptr){
    if(i2c_write_funcptr == NULL) return 0;
    me->i2c_write = i2c_write_funcptr;
    return 1;
}


uint8_t mcp9844_i2c_read_register (mcp9844_t *me, mcp9844_i2c_funcptr_t i2c_read_funcptr){
    if(i2c_read_funcptr == NULL) return 0;
    me->i2c_read = i2c_read_funcptr;
    return 1;
}

uint8_t mcp9844_get_temperature (mcp9844_t *me){
    
    uint8_t data[2];
    
    data[0] = MCP9844_REG_AMBIENT_TEMPERATURE;
    if(!me->i2c_write(me->client_address, data, 1)) return 0;
    
    if(!me->i2c_read(me->client_address, data, 2)) return 0;

    me->data.temp.decimal = data[1] & 0x0F;
    me->data.temp.integer = ((data[0] << 4) | (data[1] >> 4)) & 0xFF;
    uint8_t sign = (data[0] >> 4) & 1;
    
    me->data.temp.integer = (sign) ? ((~(me->data.temp.integer)) + 1) : me->data.temp.integer;
    
    return 1;
}

uint8_t mcp9844_i2c_check (mcp9844_t *me){
    uint8_t data;
    return me->i2c_read(me->client_address, &data, 1);
}

uint8_t mcp9844_get_manufacturer_info (mcp9844_t *me){
    uint8_t data[2];
    
    data[0] = MCP9844_REG_MANUFACTURER_ID;
    if(!me->i2c_write(me->client_address, data, 1)) return 0;
    if(!me->i2c_read(me->client_address, data, 2)) return 0;

    me->data.device.man_id = (data[0] << 8) | data[1];
    
    return 1;
}

uint8_t mcp9844_get_device_info (mcp9844_t *me){
    uint8_t data[2];
    
    data[0] = MCP9844_REG_DEVICE_ID;
    if(!me->i2c_write(me->client_address, data, 1)) return 0;
    if(!me->i2c_read(me->client_address, data, 2)) return 0;

    me->data.device.dev_id  = data[0];
    me->data.device.dev_rev = data[1];
    
    return 1;
}

uint8_t mcp9844_get_capability (mcp9844_t *me){
    uint8_t data[2];
    
    data[0] = MCP9844_REG_CAPABILITY;
    if(!me->i2c_write(me->client_address, data, 1)) return 0;
    if(!me->i2c_read(me->client_address, data, 2)) return 0;

    me->data.capability.shdn_stat       = (data[1] >> 7) & 0b1;
    me->data.capability.tout_range      = (data[1] >> 6) & 0b1;
    me->data.capability.unused          = (data[1] >> 5) & 0b1;
    me->data.capability.resolution      = (data[1] >> 3) & 0b11;
    me->data.capability.meas_resolution = (data[1] >> 2) & 0b1;
    me->data.capability.accuracy        = (data[1] >> 1) & 0b1;
    me->data.capability.temp_alarm      = data[1] & 0b1;
    
    return 1;
}