/*******************************************************************************
  ESP32C6 Driver UART

  Created by:
    Miroslav Soukup

  File Name:
    esp32c6_driver_uart.c

  Summary:
    ESP32C6 Driver UART Source File
 
  Version:
    2.3

  Description:
    This file provides basic functions for esp32c6 communication module.

*******************************************************************************/



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include <stdio.h>

#include "esp32c6_driver_uart.h"



// *****************************************************************************
// Section: Functions
// *****************************************************************************

bool esp32c6_rb_read_char_register (esp32c6_t *me, esp32c6_rb_read_char_funcptr_t funcptr, uintptr_t rbme)
{
    if(me == NULL || funcptr == NULL) return false;
    me->rb_read_char_funcptr_rbme = rbme;
    me->rb_read_char_funcptr = funcptr;
    return true;
}
bool esp32c6_rb_is_empty_register (esp32c6_t *me, esp32c6_rb_is_empty_funcptr_t funcptr, uintptr_t rbme)
{
    if(me == NULL || funcptr == NULL) return false;
    me->rb_is_empty_funcptr_rbme = rbme;
    me->rb_is_empty_funcptr = funcptr;
    return true;
}

bool esp32c6_uart_write_register (esp32c6_t *me, esp32c6_uart_write_funcptr_t funcptr){
    if(me == NULL || funcptr == NULL) return false;
    me->write_funcptr = funcptr;
    return true;
}

bool esp32c6_uart_write_is_busy_register (esp32c6_t *me, esp32c6_uart_write_is_busy_funcptr_t funcptr){
    if(me == NULL || funcptr == NULL) return false;
    me->write_is_busy_funcptr = funcptr;
    return true;
}

void ESP32C6_PARSER_Task(esp32c6_t *me){
    static ESP32C6_PARSER_STATE_e parser_state = 0;
    static ESP32C6_PARSER_COMMAND_TYPE_e parser_command_type_parsing = ESP32C6_PARSER_COMMAND_TYPE_NONE;
    char rb_char = '\0';
    
    static uint8_t topic_increment = 0;
    static uint8_t data_increment  = 0;
    

    switch(parser_state){
        case ESP32C6_PARSER_STATE_INIT:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == '\r') parser_state = ESP32C6_PARSER_STATE_CHECK_CRLF;
                else if(rb_char == '+') parser_state = ESP32C6_PARSER_STATE_PLUS;
//                else if(rb_char == 'W');
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'M') parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                
                static uint8_t i = 0;
                
                if(rb_char == "QTT"[i++]){
                    if(i >= 3){
                        i = 0;
                        parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_X;
                    }
                }
                else{
                    i = 0;
                    parser_state = ESP32C6_PARSER_STATE_INIT;
                }
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_X:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'S') parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                
                static uint8_t i = 0;
                
                if(rb_char == "UBRECV"[i++]){
                    if(i >= 6){
                        i = 0;
                        parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_DELIM;
                    }
                }
                else{
                    i = 0;
                    parser_state = ESP32C6_PARSER_STATE_INIT;
                }
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_DELIM:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == ':') parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_LINKID;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_LINKID:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char >= '0' && rb_char <= '9'){
                    me->data.mqtt_link_id = rb_char - '0';
                    parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_COMMA1;
                }
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_COMMA1:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == ',') parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_QUOTATION_MARK1;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_QUOTATION_MARK1:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == '\"') parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_TOPIC_READ;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_TOPIC_READ:
            topic_increment = 0;
            parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_TOPIC_READING;
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_TOPIC_READING:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == '\"'){
                    me->data.mqtt_topic[topic_increment] = '\0';
                    parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_COMMA2;
                }
                else if(rb_char == '\r') parser_state = ESP32C6_PARSER_STATE_CHECK_CRLF;
                else me->data.mqtt_topic[topic_increment++] = rb_char;
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_COMMA2:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == ',') parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_SIZE_READ;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_SIZE_READ:
            // size_increment = 0;
            parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_SIZE_READING;
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_SIZE_READING:
           if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == ',') parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_DATA_READ;
                else if(rb_char == '\r') parser_state = ESP32C6_PARSER_STATE_CHECK_CRLF;
                // else size
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_DATA_READ:
            data_increment = 0;
            parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_DATA_READING;
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_DATA_READING:
           if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == '\r') parser_state = ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_CR_DONE;
                else me->data.buffer[data_increment++] = rb_char;
            }
        break;
        
        case ESP32C6_PARSER_STATE_PLUS_MQTT_SUBRECV_CR_DONE:
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("<mqttsubrecv detected>\r\n");
            parser_command_type_parsing = ESP32C6_PARSER_COMMAND_TYPE_MQTT_SUBRECV;
            parser_state = ESP32C6_PARSER_STATE_COMMAND_END_CRLF;
        break;

        case ESP32C6_PARSER_STATE_CHECK_CRLF:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == '\n') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_INIT;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_INIT:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == '\r') parser_state = ESP32C6_PARSER_STATE_CHECK_CRLF;
                else if(rb_char == '+') parser_state = ESP32C6_PARSER_STATE_PLUS;
                else if(rb_char == 'O') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_OK;
                else if(rb_char == 'E') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_ER;
                else if(rb_char == 'r') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_re;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_OK:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'K') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_OK_SET;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_OK_SET:
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("<OK detected>\r\n");
            parser_command_type_parsing = ESP32C6_PARSER_COMMAND_TYPE_OK;
            parser_state = ESP32C6_PARSER_STATE_COMMAND_END_CR;
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_ER:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'R') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_ERR;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_ERR:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'R') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_ERRO;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_ERRO:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'O') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_ERROR;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_ERROR:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'R') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_ERROR_SET;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_ERROR_SET:
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("<ERROR detected>\r\n");
            parser_command_type_parsing = ESP32C6_PARSER_COMMAND_TYPE_ERROR;
            parser_state = ESP32C6_PARSER_STATE_COMMAND_END_CR;
        break;


        case ESP32C6_PARSER_STATE_COMMAND_CRLF_re:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'e') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_rea;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_rea:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'a') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_read;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_read:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'd') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_ready;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_ready:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == 'y') parser_state = ESP32C6_PARSER_STATE_COMMAND_CRLF_ready_SET;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_CRLF_ready_SET:
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("<ready detected>\r\n");
            parser_command_type_parsing = ESP32C6_PARSER_COMMAND_TYPE_READY;
            parser_state = ESP32C6_PARSER_STATE_COMMAND_END_CR;
        break;            

        case ESP32C6_PARSER_STATE_COMMAND_END_CR:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == '\r') parser_state = ESP32C6_PARSER_STATE_COMMAND_END_CRLF;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_END_CRLF:
            if(!me->rb_is_empty_funcptr(me->rb_is_empty_funcptr_rbme)){
                me->rb_read_char_funcptr(me->rb_read_char_funcptr_rbme, &rb_char);
                if(rb_char == '\n') parser_state = ESP32C6_PARSER_STATE_COMMAND_ACCEPT;
                else parser_state = ESP32C6_PARSER_STATE_INIT;
            }
        break;

        case ESP32C6_PARSER_STATE_COMMAND_ACCEPT: // command accepted
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_EXTENDED) printf("<command accepted>\r\n");
            me->tasks.parser.command.type = parser_command_type_parsing;
            parser_state = ESP32C6_PARSER_STATE_INIT;
        break;
    }
    
    if(me->debug_level >= ESP32C6_DEBUG_LEVEL_ALL && rb_char != '\0'){
        if(rb_char == '\r') printf("[CR]");
        else if(rb_char == '\n') printf("[LF]");
        else printf("%c", rb_char);
    }
}


ESP32C6_INIT_STATE_e ESP32C6_Task(esp32c6_t *me)
{        
    switch(me->tasks.state.modul){
        case ESP32C6_INIT_STATE_INIT:
            me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
            
            me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_INIT;
            me->tasks.state.http = ESP32C6_INIT_HTTP_STATE_INIT;
            me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_INIT;
            
            me->tasks.state.modul = ESP32C6_INIT_STATE_RESET;
        break;

        case ESP32C6_INIT_STATE_RESET:{
            char _buff[128];
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_RESET "\r\n");
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            me->tasks.state.modul = ESP32C6_INIT_STATE_RESETING1;
        }break;

        case ESP32C6_INIT_STATE_RESETING1:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                me->tasks.state.modul = ESP32C6_INIT_STATE_RESETING2;
            }
        break;

        case ESP32C6_INIT_STATE_RESETING2:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_READY){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                me->tasks.state.modul = ESP32C6_INIT_STATE_ECHO_DISABLE;
            }
        break;

        case ESP32C6_INIT_STATE_ECHO_DISABLE:{
            char _buff[128];
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_ECHO_DISABLE "\r\n");
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            me->tasks.state.modul = ESP32C6_INIT_STATE_ECHO_DISABLING;
        }break;

        case ESP32C6_INIT_STATE_ECHO_DISABLING:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                if(me->debug_level >= ESP32C6_DEBUG_LEVEL_BASIC) printf("<init completed>\r\n");
                me->tasks.state.modul = ESP32C6_INIT_STATE_DONE;
            }
        break;

        case ESP32C6_INIT_STATE_DONE:
            
        break;
    }
    
    return me->tasks.state.modul;
}


ESP32C6_INIT_WIFI_STATE_e ESP32C6_WIFI_Task(esp32c6_t *me)
{    
    switch(me->tasks.state.wifi){
        case ESP32C6_INIT_WIFI_STATE_INIT:
            if(esp32c6_get_init_module_state(me) == ESP32C6_INIT_STATE_DONE) me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZE;
        break;

        case ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZE:{
            char _buff[128];
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_WIFI_INIT "1" "\r\n");
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZATION;
        }break;
        
        case ESP32C6_INIT_WIFI_STATE_DRIVER_INITIALIZATION:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_MODE_STATION_SET;
            }
        break;
        
        case ESP32C6_INIT_WIFI_STATE_MODE_STATION_SET:{
            char _buff[128];
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_WIFI_MODE_STATION "\r\n");
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_MODE_STATION_SETTING;
        }break;
        
        case ESP32C6_INIT_WIFI_STATE_MODE_STATION_SETTING:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_HOSTNAME_SET;
            }
        break;
        
        case ESP32C6_INIT_WIFI_STATE_HOSTNAME_SET:{
            char _buff[128];
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_WIFI_STATION_HOSTNAME "\"" "%s" "\"" "\r\n", me->config.hostname);
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_HOSTNAME_SETTING;
        }break;
        
        case ESP32C6_INIT_WIFI_STATE_HOSTNAME_SETTING:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_CONNECT;
            }
        break;
        
        case ESP32C6_INIT_WIFI_STATE_CONNECT:{
            char _buff[128];
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_WIFI_STATION_CONNECT "\"" "%s" "\",\"" "%s" "\"" "\r\n", me->config.wifi_ssid, me->config.wifi_password);
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_CONNECTING;
        }break;
        
        case ESP32C6_INIT_WIFI_STATE_CONNECTING:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                if(me->debug_level >= ESP32C6_DEBUG_LEVEL_BASIC) printf("<init wifi completed>\r\n");
                me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_DONE;
            }
            else if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_ERROR){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                if(me->debug_level >= ESP32C6_DEBUG_LEVEL_BASIC) printf("<init wifi error>\r\n");
                me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_DISCONECTED;
            }
        break;
        
        case ESP32C6_INIT_WIFI_STATE_DISCONECTED:
            
        break;
        
        case ESP32C6_INIT_WIFI_STATE_DONE:
            
        break;
    }
    
    return me->tasks.state.wifi;
}

ESP32C6_INIT_MQTT_STATE_e ESP32C6_MQTT_Task(esp32c6_t *me)
{
    static uint8_t increment = 0;
    
    switch(me->tasks.state.mqtt){
        case ESP32C6_INIT_MQTT_STATE_INIT:
            if(esp32c6_get_init_wifi_state(me) == ESP32C6_INIT_WIFI_STATE_DONE) me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_CONFIG;
        break;

        case ESP32C6_INIT_MQTT_STATE_CONFIG:{
            char _buff[256];
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_MQTT_CONFIG "0,1,\"" "%s" "\",\"" "%s" "\",\"" "%s" "\",0,0,\"\"" "\r\n", me->config.hostname, me->config.mqtt_username, me->config.mqtt_password);
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_CONFIGURING;
        }break;
        
        case ESP32C6_INIT_MQTT_STATE_CONFIGURING:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_CONNECT;
            }
        break;
        
        case ESP32C6_INIT_MQTT_STATE_CONNECT:{
            char _buff[128];
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_MQTT_CONNECT "0,\"" "%s" "\"," "%s" ",%d" "\r\n", me->config.mqtt_broker_ip, me->config.mqtt_broker_port, (int) me->config.mqtt_reconnect_enable);
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_CONNECTING;
        }break;
        
        case ESP32C6_INIT_MQTT_STATE_CONNECTING:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_SUBSCRIBE;
            }
            else if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_ERROR){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                if(me->debug_level >= ESP32C6_DEBUG_LEVEL_BASIC) printf("<init mqtt error>\r\n");
                me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_DISCONNECTED;
            }
        break;
        
        case ESP32C6_INIT_MQTT_STATE_DISCONNECTED:
            if(me->config.mqtt_reconnect_enable) me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_CONNECT;
        break;
        
        case ESP32C6_INIT_MQTT_STATE_SUBSCRIBE:{
            char _buff[128];
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_MQTT_SUBSCRIBE "\"" "%s" "\",2" "\r\n", me->config.mqtt_topics[increment].topic);
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_SUBSCRIBING;
        }break;
        
        case ESP32C6_INIT_MQTT_STATE_SUBSCRIBING:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                ++increment;
                if(increment >= me->config.mqtt_topics_count) me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_SUBSCRIBED;
                else me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_SUBSCRIBE;
            }
        break;
        
        case ESP32C6_INIT_MQTT_STATE_SUBSCRIBED:
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_BASIC) printf("<init mqtt completed>\r\n");
            me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_DONE;
        break;
        
        case ESP32C6_INIT_MQTT_STATE_DONE:
            
        break;
    }
    
    return me->tasks.state.mqtt;
}

ESP32C6_INIT_HTTP_STATE_e ESP32C6_HTTP_Task(esp32c6_t *me)
{
    switch(me->tasks.state.http){
        case ESP32C6_INIT_HTTP_STATE_INIT:
            if(esp32c6_get_init_wifi_state(me) == ESP32C6_INIT_WIFI_STATE_DONE){
                if(me->config.http_header_enable) me->tasks.state.http = ESP32C6_INIT_HTTP_STATE_CONFIG;
                else me->tasks.state.http = ESP32C6_INIT_HTTP_STATE_DONE;
            }
        break;
        
        case ESP32C6_INIT_HTTP_STATE_CONFIG:{
            char _buff[256];
            
            size_t header_length = strlen(me->config.http_header);
            
            int len = sprintf((char *) _buff, ESP32C6_MINI_COMMAND_HTTP_HEADER "=" "%d\r\n", header_length);
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", _buff);
            me->write_funcptr(_buff, (size_t) len);
            while(me->write_is_busy_funcptr());
            
            if(me->debug_level >= ESP32C6_DEBUG_LEVEL_INFORM) printf("%s", me->config.http_header);
            me->write_funcptr((void *) me->config.http_header, header_length);
            while(me->write_is_busy_funcptr());
            
            me->tasks.state.http = ESP32C6_INIT_HTTP_STATE_CONFIGURING;
        }break;
        
        case ESP32C6_INIT_HTTP_STATE_CONFIGURING:
            if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_OK){
                me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;
                me->tasks.state.http = ESP32C6_INIT_HTTP_STATE_DONE;
            }
        break;
        
        case ESP32C6_INIT_HTTP_STATE_DONE:
            
        break;
    }
    
    return me->tasks.state.http;
}



void ESP32C6_MQTT_SUBSCRIBED_TOPICS_EXECUTE_CALLBACK_Task(esp32c6_t *me)
{
    if(esp32c6_get_init_mqtt_state(me) == ESP32C6_INIT_MQTT_STATE_DONE){
        if(me->tasks.parser.command.type == ESP32C6_PARSER_COMMAND_TYPE_MQTT_SUBRECV){
            me->tasks.parser.command.type = ESP32C6_PARSER_COMMAND_TYPE_NONE;

            for(uint8_t i = 0; i < me->config.mqtt_topics_count; ++i){                
                if(strcmp((char *) me->data.mqtt_topic, me->config.mqtt_topics[i].topic) == 0){
                    me->config.mqtt_topics[i].callback_funcptr(me);
                    break;
                }
            }
        }
    }
}

void esp32c6_configure_init(esp32c6_t *me, char *hostname, ESP32C6_DEBUG_LEVEL_e dl)
{
    me->tasks.state.modul = ESP32C6_INIT_STATE_INIT;
    me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_INIT;
    
    me->config.http_header_enable = false;
    
    me->debug_level = dl;
    
    strcpy(me->config.hostname, hostname);
}

void esp32c6_configure_wifi_secure(esp32c6_t *me, char *ssid, char *pswd)
{
    me->tasks.state.wifi = ESP32C6_INIT_WIFI_STATE_INIT;
    strcpy(me->config.wifi_ssid, ssid);
    strcpy(me->config.wifi_password, pswd);
}

void esp32c6_configure_mqtt_secure(esp32c6_t *me, char *server_ip, char *server_port, char *client_username, char *client_password)
{
    me->tasks.state.mqtt = ESP32C6_INIT_MQTT_STATE_INIT;
    strcpy(me->config.mqtt_broker_ip, server_ip);
    strcpy(me->config.mqtt_broker_port, server_port);
    strcpy(me->config.mqtt_username, client_username);
    strcpy(me->config.mqtt_password, client_password);
}

void esp32c6_configure_mqtt_reconnecting(esp32c6_t *me, bool enable)
{
    me->config.mqtt_reconnect_enable = enable;
}

void esp32c6_configure_mqtt_register_subscribe_topics(esp32c6_t *me, esp32c6_mqtt_config_topic_t *mqtt_topics_array, const uint8_t count)
{
    me->config.mqtt_topics = mqtt_topics_array;
    me->config.mqtt_topics_count = count;
}

void esp32c6_configure_http_header(esp32c6_t *me, char *http_header)
{
    me->config.http_header_enable = true;
    me->config.http_header = http_header;
}

void esp32c6_configure_http_no_header(esp32c6_t *me)
{
    me->config.http_header_enable = false;
}

ESP32C6_INIT_STATE_e esp32c6_get_init_module_state(esp32c6_t *me)
{
    return me->tasks.state.modul;
}

ESP32C6_INIT_WIFI_STATE_e esp32c6_get_init_wifi_state(esp32c6_t *me)
{
    return me->tasks.state.wifi;
}

ESP32C6_INIT_MQTT_STATE_e esp32c6_get_init_mqtt_state(esp32c6_t *me)
{
    return me->tasks.state.mqtt;
}

ESP32C6_INIT_HTTP_STATE_e esp32c6_get_init_http_state(esp32c6_t *me)
{
    return me->tasks.state.http;
}

uint16_t esp32c6_get_mqtt_create_message_uint(char *_buff, char *topic, unsigned int value)
{
    int len = sprintf(_buff, ESP32C6_MINI_COMMAND_MQTT_PUBLISH "\"" "%s" "\",\"" "%u" "\",0,0" "\r\n", topic, value);
    return (len > 0) ? ((uint16_t) len) : 0;
}

uint16_t esp32c6_get_mqtt_create_message_int(char *_buff, char *topic, signed int value)
{
    int len = sprintf(_buff, ESP32C6_MINI_COMMAND_MQTT_PUBLISH "\"" "%s" "\",\"" "%d" "\",0,0" "\r\n", topic, value);
    return (len > 0) ? ((uint16_t) len) : 0;
}

uint16_t esp32c6_get_mqtt_create_message_float(char *_buff, char *topic, float value, unsigned int decimal_places)
{
    int len = sprintf(_buff, ESP32C6_MINI_COMMAND_MQTT_PUBLISH "\"" "%s" "\",\"" "%.*f" "\",0,0" "\r\n", topic, decimal_places, value);
    return (len > 0) ? ((uint16_t) len) : 0;
}
