/*******************************************************************************
 * RING BUFFER library
 
  Created by:
    Miroslav Soukup

  File Name:
    ring_buffer.h

  Summary:
    RING BUFFER library Header File
 
  Version:
    2.0

  Description:
    This file provides basic functions.

*******************************************************************************/


#ifndef RING_BUFFER_H // Protection against multiple inclusion
#define RING_BUFFER_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 
#include <string.h> // memset() function 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define RING_BUFFER_DATA_TYPE uint8_t
#define RING_BUFFER_INDEX_DATA_TYPE uint16_t


// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct ring_buffer_descriptor ring_buffer_t;

struct ring_buffer_descriptor{
    RING_BUFFER_DATA_TYPE *buffer;
    RING_BUFFER_INDEX_DATA_TYPE capacity;
    volatile RING_BUFFER_INDEX_DATA_TYPE write_index;
    volatile RING_BUFFER_INDEX_DATA_TYPE read_index;
    volatile RING_BUFFER_INDEX_DATA_TYPE count;
};



// *****************************************************************************
// Section: Functions
// *****************************************************************************

/**
 * \brief Function for initialization ring buffer.
 *
 * \param me pointer to a struct of type ring_buffer_t
 * \param capacity the capacity of the ring buffer in bytes (uint8_t)
 *
 * \returns void
 */
inline void ring_buffer_init (ring_buffer_t *me, RING_BUFFER_DATA_TYPE *rb_buffer, RING_BUFFER_INDEX_DATA_TYPE capacity) {
    me->capacity = capacity; // store capacity of ring buffer
    me->buffer = rb_buffer;  // assign address of buffer
    me->write_index = 0;     // clear write index
    me->read_index  = 0;     // clear read index
    me->count       = 0;     // clear count
}

/**
 * \brief Function to get next index in order.
 *
 * \param index actual
 * \param capacity the capacity of the ring buffer in bytes (uint8_t)
 *
 * \returns next index in order
 */
inline RING_BUFFER_INDEX_DATA_TYPE ring_buffer_get_next (RING_BUFFER_INDEX_DATA_TYPE index, RING_BUFFER_INDEX_DATA_TYPE capacity) {
    return ( (index >= (capacity - 1)) ? (0) : (index + 1) );
}

/**
 * \brief Function to get next write index in order.
 *
 * \param me pointer to a struct of type ring_buffer_t
 *
 * \returns next write index in order
 */
inline RING_BUFFER_INDEX_DATA_TYPE ring_buffer_get_next_write (ring_buffer_t *me) {
    return ring_buffer_get_next(me->write_index, me->capacity);
}

/**
 * \brief Function to get next read index in order.
 *
 * \param me pointer to a struct of type ring_buffer_t
 *
 * \returns next read index in order
 */
inline RING_BUFFER_INDEX_DATA_TYPE ring_buffer_get_next_read (ring_buffer_t *me) {
    return ring_buffer_get_next(me->read_index, me->capacity);
}

/**
 * \brief Function to get full status of ring buffer.
 *
 * \param me pointer to a struct of type ring_buffer_t
 *
 * \returns status about full ring buffer
 */
inline uint8_t ring_buffer_is_full (ring_buffer_t *me) {
    return ( me->read_index == ring_buffer_get_next_write(me) );
}

/**
 * \brief Function to write one value to ring buffer.
 *
 * \param me pointer to a struct of type ring_buffer_t
 * \param value to write in to the ring buffer
 *
 * \returns status of success or failure of write operation
 */
inline uint8_t ring_buffer_write (ring_buffer_t *me, uint8_t value) {
    if (ring_buffer_is_full(me)) return 0;                      // if full -> error
    me->buffer[me->write_index] = value;                        // write down value
    me->write_index = ring_buffer_get_next_write(me);           // get and set new next write index
    ++(me->count);
    return 1;
}

/**
 * \brief Function to get empty status of ring buffer.
 *
 * \param me pointer to a struct of type ring_buffer_t
 *
 * \returns status about empty ring buffer
 */
inline uint8_t ring_buffer_is_empty (ring_buffer_t *me) {
    return ( me->read_index == me->write_index );
}

/**
 * \brief Function to read one value from ring buffer.
 *
 * \param me pointer to a struct of type ring_buffer_t
 * \param value pointer to read one value from the ring buffer
 *
 * \returns status of success or failure of read operation
 */
inline uint8_t ring_buffer_read (ring_buffer_t *me, uint8_t *value) {
    if (ring_buffer_is_empty(me)) return 0;                     // if empty -> error
    *value = me->buffer[me->read_index];                        // read out value
    me->read_index = ring_buffer_get_next_read(me);             // get and set new next read index
    --(me->count);
    return 1;
}

/**
 * \brief Function to get char count.
 *
 * \param me pointer to a struct of type ring_buffer_t
 *
 * \returns count of saved chars
 */
inline RING_BUFFER_INDEX_DATA_TYPE ring_buffer_get_count (ring_buffer_t *me) {
    if (!me) return 0; // if empty -> return 
    return me->count;
}


#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of RING_BUFFER_H
