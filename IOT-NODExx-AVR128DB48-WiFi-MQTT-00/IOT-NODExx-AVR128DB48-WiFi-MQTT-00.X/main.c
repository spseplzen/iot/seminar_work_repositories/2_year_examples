 /*
 * MAIN Generated Driver File
 * 
 * @file main.c
 * 
 * @defgroup main MAIN
 * 
 * @brief This is the generated driver implementation file for the MAIN driver.
 *
 * @version MAIN Driver Version 1.0.2
 *
 * @version Package Version: 3.1.2
*/

/*
� [2025] Microchip Technology Inc. and its subsidiaries.

    Subject to your compliance with these terms, you may use Microchip 
    software and any derivatives exclusively with Microchip products. 
    You are responsible for complying with 3rd party license terms  
    applicable to your use of 3rd party software (including open source  
    software) that may accompany Microchip software. SOFTWARE IS ?AS IS.? 
    NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS 
    SOFTWARE, INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT,  
    MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT 
    WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY 
    KIND WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF 
    MICROCHIP HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE 
    FORESEEABLE. TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP?S 
    TOTAL LIABILITY ON ALL CLAIMS RELATED TO THE SOFTWARE WILL NOT 
    EXCEED AMOUNT OF FEES, IF ANY, YOU PAID DIRECTLY TO MICROCHIP FOR 
    THIS SOFTWARE.
*/
#include <stdlib.h>

#include "mcc_generated_files/system/system.h"
#include "mcc_generated_files/timer/delay.h"

#include "my_drivers/ring_buffer/ring_buffer.h"
#include "my_drivers/esp32c6_driver_uart/esp32c6_driver_uart.h"



#define NODE_WIFI_SSID      "ssid"      // name of WiFi
#define NODE_WIFI_PASSWORD  "pswd"      // password of WiFi

#define NODE_HOSTNAME               "NODExx"        // node name
#define NODE_MQTT_SERVER_IP         "172.16.2.1"    // IP address MQTT broker
#define NODE_MQTT_SERVER_PORT       "1883"          // PORT for MQTT broker
#define NODE_MQTT_CLIENT_USERNAME   ""              // user name for MQTT broker
#define NODE_MQTT_CLIENT_PASSWORD   ""              // password for MQTT broker



ring_buffer_t rb;

void module_uart_rx_callback(){
    if(!USART2_ErrorGet())
        ring_buffer_write(&rb, USART2_Read());
}

bool module_uart_tx_write(char *buffer, size_t size){
    for(uint8_t i = 0; i < size; ++i)
    {
        USART2_Write(buffer[i]);
        while(!USART2_IsTxDone());
    }
    return true;
}

bool module_uart_tx_is_busy(void){
    return !USART2_IsTxDone();
}


bool rb_read_char(uintptr_t rbme, char *c)
{
    return ring_buffer_read(&rb, (uint8_t *) c);
}

bool rb_is_empty(uintptr_t rbme)
{
    return ring_buffer_is_empty(&rb);
}



void led_callback(esp32c6_t *me)
{
    printf("LED -> %1u\r\n", me->data.buffer[0] - '0');
    if(me->data.buffer[0] == '1') LED_SetLow();
    else if(me->data.buffer[0] == '0') LED_SetHigh();
}


esp32c6_mqtt_config_topic_t mqtt_topics[] =     {
                                                    {"home/" NODE_HOSTNAME "/led", led_callback} // led
                                                };


volatile uint8_t tca0_owf_flag = 0;
void tca0_owf_callback(){
    tca0_owf_flag = 1;
}


volatile uint8_t btn_flag = 0;
void btn_callback(){
    btn_flag = 1;
}


int main(void)
{
    SYSTEM_Initialize();
    
    
    
    uint8_t rb_buffer[256];
    ring_buffer_init(&rb, rb_buffer, 256);
    
    
    
    USART2_RxCompleteCallbackRegister(module_uart_rx_callback);
    
    
    
    esp32c6_t esp32c6 = {};
    
    esp32c6_rb_read_char_register(&esp32c6, rb_read_char, (uintptr_t) &rb); // ring buffer read
    esp32c6_rb_is_empty_register(&esp32c6, rb_is_empty, (uintptr_t) &rb);   // ring buffer is empty
    
    esp32c6_uart_write_register(&esp32c6, module_uart_tx_write);            // uart write
    esp32c6_uart_write_is_busy_register(&esp32c6, module_uart_tx_is_busy);  // uart is busy
    
    esp32c6_configure_init(&esp32c6, NODE_HOSTNAME, ESP32C6_DEBUG_LEVEL_BASIC);  // module config init
    
    esp32c6_configure_wifi_secure(&esp32c6, NODE_WIFI_SSID, NODE_WIFI_PASSWORD); // wifi config

    esp32c6_configure_mqtt_secure(&esp32c6, NODE_MQTT_SERVER_IP, NODE_MQTT_SERVER_PORT, NODE_MQTT_CLIENT_USERNAME, NODE_MQTT_CLIENT_PASSWORD); // mqtt config
    
    esp32c6_configure_mqtt_reconnecting(&esp32c6, true); // mqtt init reconnecting enabled
    
    esp32c6_configure_mqtt_register_subscribe_topics(&esp32c6, mqtt_topics, sizeof(mqtt_topics)/sizeof(*mqtt_topics)); // mqtt subscribe topics config
    

    
    BTN_SetInterruptHandler(btn_callback);
    
    
    
    TCA0_OverflowCallbackRegister(tca0_owf_callback);
    TCA0_Start();
    
    
    
    printf("AVR128DB48 CNANO" " " NODE_HOSTNAME " WiFi MQTT example (00) initialization...");
    DELAY_milliseconds(3000);
    printf("done.\r\n");
    
    

    while(1)
    {
        
        ESP32C6_PARSER_Task(&esp32c6); // read and parse received messages

        ESP32C6_Task(&esp32c6); // module initialization

        ESP32C6_WIFI_Task(&esp32c6); // initialize WiFi connection

        ESP32C6_MQTT_Task(&esp32c6); // initialize MQTT

        ESP32C6_MQTT_SUBSCRIBED_TOPICS_EXECUTE_CALLBACK_Task(&esp32c6); // read mqtt topic and call callback function


        if(esp32c6_get_init_mqtt_state(&esp32c6) == ESP32C6_INIT_MQTT_STATE_DONE)
        {
            if(tca0_owf_flag)
            {
                tca0_owf_flag = 0;

                if(btn_flag && !module_uart_tx_is_busy())
                {
                    btn_flag = 0;

                    static uint8_t btn_state = 0;
                    btn_state = !btn_state;

                    char _buff[128];
                    uint16_t len = esp32c6_get_mqtt_create_message_uint(_buff, "home/" NODE_HOSTNAME "/btn", btn_state);
                    module_uart_tx_write(_buff, len);

                }

            }
        }
        
        
    }
    
    return EXIT_FAILURE;
}