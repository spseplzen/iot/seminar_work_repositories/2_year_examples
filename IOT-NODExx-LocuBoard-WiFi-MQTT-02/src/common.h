#ifndef COMMON_H // Protection against multiple inclusion
#define COMMON_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdbool.h>
#include <math.h>
#include "../src/config/default/peripheral/port/plib_port.h"
#include "../src/config/default/peripheral/adc/plib_adc.h"
#include "../src/config/default/peripheral/tc/plib_tc3.h"
#include "../src/config/default/peripheral/tc/plib_tc4.h"
#include "../src/config/default/peripheral/tcc/plib_tcc0.h"


#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif


void leds_init(void);
void led1_led2_set_lighting_level(uint8_t l1, uint8_t l2);
void led3_set_power_level(bool s);
void led4_set_power_level(bool s);
void led5_rgb_set_color(uint8_t r, uint8_t g, uint8_t b);

void adc_init(void);
uint16_t adc_read(ADC_POSINPUT input);

uint8_t ESP32C6_MQTT_BTN_Task(uint8_t n, uint8_t *eic_flag);
uint8_t ESP32C6_MQTT_ADC_Channel1_Task();
uint8_t ESP32C6_MQTT_ADC_Channel2_Task();
        

#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End
