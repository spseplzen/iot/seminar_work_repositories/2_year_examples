#include "common.h"


void leds_init(void){
    TC4_CompareInitialize();
    TC4_Compare8bitMatch0Set(0);
    TC4_Compare8bitMatch1Set(0);
    TC4_CompareStart();
    
    TCC0_PWM24bitDutySet(TCC0_CHANNEL0, 0);
    TCC0_PWM24bitDutySet(TCC0_CHANNEL1, 0);
    TCC0_PWMStart();
    
    TC3_CompareInitialize();
    TC3_Compare8bitMatch0Set(0);
    TC3_CompareStart();
    
    LED3_Clear();
    LED4_Clear();
}

void led1_led2_set_lighting_level(uint8_t l1, uint8_t l2){
    TCC0_PWMInitialize();
    TCC0_PWM24bitDutySet(TCC0_CHANNEL0, l1); // LED1
    TCC0_PWM24bitDutySet(TCC0_CHANNEL1, l2); // LED2
    TCC0_PWMStart();
}

void led3_set_power_level(bool s){
    (s) ? LED3_Set() : LED3_Clear(); // LED3
}

void led4_set_power_level(bool s){
    (s) ? LED4_Set() : LED4_Clear(); // LED4
}

void led5_rgb_set_color(uint8_t r, uint8_t g, uint8_t b){
    TC4_CompareInitialize();
    TC4_Compare8bitMatch0Set(100 * pow(b / 100.0, 2.2)); // blue
    TC4_CompareStart();
    TC4_Compare8bitMatch1Set(100 * pow(g / 100.0, 2.2)); // green
    TC3_CompareInitialize();
    TC3_Compare8bitMatch0Set(100 * pow(r / 100.0, 2.2)); // red
    TC3_CompareStart();
}

void adc_init(void){
    ADC_Enable();
}

uint16_t adc_read(ADC_POSINPUT input){
    ADC_ChannelSelect(input, ADC_NEGINPUT_GND);
    ADC_ConversionStart();
    while(!ADC_ConversionStatusGet());
    return ADC_ConversionResultGet();
}